function showAjaxModal(url,title)
{
	// SHOWING AJAX PRELOADER IMAGE
	jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="assets/images/preloader.gif" style="height:25px;" /></div>');
	
	// LOADING THE AJAX MODAL
	jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
	// SHOW AJAX RESPONSE ON REQUEST SUCCESS
	$.ajax({
		url: url,
		success: function(response)
		{  console.log(title);
			jQuery('#modal_ajax .modal-title').html(title);
			jQuery('#modal_ajax .modal-body').html(response);	
		}
	});
	$("#multiple").multipleSelect({
    	filter: true
    });
    $("p").click(function(){
		$('#item-selector').slideUp('fast');
		$('#item-editor').delay(300).slideDown('fast');
    });
}
