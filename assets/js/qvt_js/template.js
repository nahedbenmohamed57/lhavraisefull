/**
 * Created by user on 20/04/2018.
 */
$(document).ready( function () {
	$('#tab').DataTable({
		language: {
			url: "DataTables/media/French.json",
			"paginate": {
			  "previous": "Précédent",
			  "next" : "Suivant"
			},
			"search" : "Recherche",
			"lengthMenu": "Affichage _MENU_ élements par page",
			"info": "Affichage page _PAGE_ sur _PAGES_",
			"zeroRecords": "Rien trouvé",
		}
	});
	$('#pole').on('change',function () {
		  var idPole = $(this).find(":selected").val();
		  
		  $.ajax({url: "/perimetre/get_perimetres_by_pole/"+idPole, success: function(result){
        	var myselect = '';
        	var jsonResult = JSON.parse(result) ;
		    $.each(jsonResult, function(k, v) {
		        myselect = myselect+"<option value='"+v.id_perimetre+"'>"+v.perimetre_nom+"</option>"
		    });
        	$("#perimetre").html(myselect);
        	$("#nombre_unite").html('');
        	if($("#perimetre").val() != undefined) {
        		//charger les unite du l'établissement séléctionné
        		idperimetre = $("#perimetre").val();
        		 $.ajax({url: "/perimetre/get_units_by_pole/"+idperimetre, success: function(result){
		        	var myselect = '';
		        	var jsonResult = JSON.parse(result) ;
				    $.each(jsonResult, function(k, v) {
				        myselect = myselect+"<option value='"+v.id_unite+"'>"+v.unite_nom+"</option>"
				    });
		        	$("#nombre_unite").html(myselect);
		   		 }});
        	}
   		 }});
	});

	$('#perimetre').on('change',function () {
		  var idperimetre = $(this).find(":selected").val();
		  
		  $.ajax({url: "/perimetre/get_units_by_pole/"+idperimetre, success: function(result){
        	var myselect = '';
        	var jsonResult = JSON.parse(result) ;
		    $.each(jsonResult, function(k, v) {
		        myselect = myselect+"<option value='"+v.id_unite+"'>"+v.unite_nom+"</option>"
		    });
        	$("#nombre_unite").html(myselect);
   		 }});
	})
} );
function showListing(idDevice) {
	var url = window.location.origin + ('/family/getListing/' + idDevice); 
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		url: url,
		success: function (resp) {
			$( ".listeDevice" ).empty();
			var div = document.createElement("button");
			div.style.background = "rgba(151, 10, 44)";
			div.style.color = "rgb(237, 235, 234)" ;
			div.innerHTML = "+";
			div.onclick = function() { 
				showAjaxModal(window.location.origin + '/family/device/addListing/false/' + resp["device_id"],'Ajout cotation'); 
				
			};
			$( ".listeDevice" ).append(div);
			if(resp["result"].length == 0){
				var div = document.createElement("p");
				div.style.color = "rgb(236,102,8)";
				div.innerHTML = "Pas de cotation";
				$( ".listeDevice" ).append(div);
			}else {
				var content = "<table>" ; 
				for(i= 0; i< resp["result"].length ; i++){ 
					console.log(resp["result"]); 
					var id_listing = resp["result"][i].listing_id[0].listing_id ; 
					var id_device = resp["device_id"] ; 
					content += '<tr><td><a href="#" data-href="' + window.location.origin + '/family/listing/delete/false/' + id_device+ '/' + id_listing+ '" data-toggle="modal" data-target="#modal_delete"><i class="fas fa-trash fa-deter" title="Supprimer"></i></a>' 
				     + resp["result"][i].listing_id[0].listing_score + '-'
					 + resp["result"][i].listing_id[0].listing_name + 

					 '</td></tr>';	
				}
				content += "</table>" ; 
				$( ".listeDevice" ).append(content); 

			}
		},
	});
}

$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});



