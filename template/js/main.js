$(document).ready(function() {
    setTimeout(function(){
      console.log('nahed');
    $('#overlay').fadeOut();
      $('#content').fadeIn();
       }, 1000);

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $('.btn-toggle .btn').click(function() {
        $('.btn-toggle').find('.btn').removeClass('active');
        $(this).addClass('active');
    });
    $('.show-password').click(function() {
        var temp = document.getElementById("profile_password");
       if (temp.type === "password") {
            temp.type = "text";
            $('.show-pass').find('.fa-eye').addClass('fa-eye-slash').removeClass('fa-eye');
        } else {
            temp.type = "password";
            $('.show-pass').find('.fa-eye-slash').addClass('fa-eye').removeClass('fa-eye-slash');
        }
    })

    if($('#save-news').length > 0) {
        $('#save-news').click(function() {
            var quoiNeuf = $('#quoi-neuf').val()
            //save news
            if(quoiNeuf) {
                $.ajax({
                    type:"POST",
                    url:"ajax/saveNews",
                    data : { news : quoiNeuf },
                    success: function (html) {
                       window.location.href = '/';
                    }
                });
            }
        })
    }
    if($('#save-email').length > 0) {
        $('#save-email').click(function() {
            var email = $('#email-contact').val()
            //save news
            if(email) {
                $.ajax({
                    type:"POST",
                    url:"ajax/saveContact",
                    data : { email : email },
                    success: function (html) {
                       window.location.href = '/';
                    }
                });
            }
        })
    }
    if($('#save-counselor').length > 0) {
        $('#save-counselor').click(function() {
            var first_name = $('#first_name').val()
            var last_name = $('#last_name').val()
            var email = $('#email').val();
            var code_postal = $('#code_postal').val()
            //save counselor
            
            if(email) {
                $.ajax({
                    type:"POST",
                    url:"ajax/saveCounselor",
                    data : { first_name : first_name, last_name:last_name,email:email,code_postal:code_postal },
                    success: function (html) {
                       window.location.href = '/counselors';
                    }
                });
            }
        })
    }

   //wizard Evaluation

    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
    $('#add-link').click(function() {
        var valueLink = $('#new-link').val();
        var nameLink = $('#name-link').val();
        if(valueLink.length > 0) {
            //save the new link
            $.ajax({
                type:"POST",
                url:"../ajax/savelink",
                data : { link : valueLink,nameLink: nameLink },
                success: function (html) {
                   window.location.href = '/resource';
                }
            });
        }
   
    });
})
//add question
$('#save-question').click(function() {
    var question = $('#question').val();
    var newDemention = $("#new-demention :selected").val()
    if(question) {
        $.ajax({
            type:"POST",
            url:"ajax/saveQuestion",
            data : { question : question,newDemention: newDemention },
            success: function (html) {
               window.location.href = '/questions';
            }
        });
    }
})
//update question
$( "a[id^='updateQuestionbtn_']" ).click(function(e) {
    var id = $(this).attr('data-questionId');
    var question = $(this).attr('data-question-text');
    var questionDem = $(this).attr('data-question-dem');
    if(!questionDem) questionDem = 0;
    $('#updateQuestion').modal('show');
    $('#updateQuestion #new-question').text(question);
    $('#updateQuestion #new-question').val(question);
    $('#question-demention').val(questionDem);

    $('#updateQuestion #idQuestion').val(id);
    $('#updateQuestion').modal('show');
})
$('.update-question').click(function() {
    var question = $('#updateQuestion #new-question').val();
    var id       = $('#updateQuestion #idQuestion').val();
    var questionDemention = $("#question-demention :selected").val();
    if(question != '' && id != '') {
        $.ajax({
            type:"POST",
            url:"ajax/updateQuestion",
            data : { question : question, id: id, questionDemention: questionDemention },
            success: function (html) {
               window.location.href = '/questions';
            }
        });
    }
})

//delete question

$( "a[id^='deletequestion_']" ).click(function(e) {
    var id = $(this).attr('data-questionId');
    $('#confirm-delete #id-question').val(id);
    $('#confirm-delete').modal('show');
})

$('.delete-question').click(function() {
    var id = $('#confirm-delete #id-question').val();

    if(id) {
        $.ajax({
            type:"POST",
            url:"ajax/deleteQuestion",
            data : { id: id },
            success: function (html) {
               window.location.href = '/questions';
            }
        });
    }
});
$( "a[id^='deleteCounselor_']" ).click(function(e) {
    var id = $(this).attr('data-counselorid');
    $('#confirm-delete #id-counselor').val(id);
    $('#confirm-delete').modal('show');
})
$('.delete-counselor').click(function() {
    var id = $('#confirm-delete #id-counselor').val();
    if(id) {
        $.ajax({
            type:"POST",
            url:"ajax/deleteCounselor",
            data : { id: id },
            success: function (html) {
               window.location.href = '/counselors';
            }
        });
    }
});

//update question
$( "a[id^='updateCounselor_']" ).click(function(e) {
    var id = $(this).attr('data-counselorid');
    var firstName = $(this).attr('data-fn');
    var lastName  = $(this).attr('data-ln');
    var email  = $(this).attr('data-email');
    var cp  = $(this).attr('data-cp');
    $('#updateCounselor').modal('show');
    $('#updateCounselor #first_name').val(firstName);
    $('#updateCounselor #last_name').val(lastName);
    $('#updateCounselor #email').val(email);
    $('#updateCounselor #code_postal').val(cp);
    $('#updateCounselor #idCounselor').val(id);
})
$('.update-counselor').click(function() {
    var id        = $('#updateCounselor #idCounselor').val();
    var firstName = $('#updateCounselor #first_name').val();
    var lastName  = $('#updateCounselor #last_name').val();
    var email     =  $('#updateCounselor #email').val();
    var cp        = $('#updateCounselor #code_postal').val();
    if(email != '' && id != '') {
        $.ajax({
            type:"POST",
            url:"ajax/updateCounselor",
            data : { id : id, firstName: firstName, lastName: lastName,email:email, cp:cp},
            success: function (html) {
               window.location.href = '/counselors';
            }
        });
    }
});

$( "a[id^='deleteUser_']" ).click(function(e) {
    var id = $(this).attr('data-userId');
    $('#confirm-delete #id-user').val(id);
    $('#confirm-delete').modal('show');
})
$('.delete-user').click(function() {
    var id = $('#confirm-delete #id-user').val();
    if(id) {
        $.ajax({
            type:"POST",
            url:"ajax/deleteUser",
            data : { id: id },
            success: function (html) {
               window.location.href = '/users';
            }
        });
    }
});
//update the user status
$('.update-status').click(function() {
    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');
    if(status == 0) {
        $(this).attr('data-status', 1);
        $(this).find('i').removeClass('fas fa-check');
        $(this).find('i').addClass('far fa-thumbs-down')
    } else {
        $(this).attr('data-status', 0);
        $(this).find('i').removeClass('far fa-thumbs-down');
        $(this).find('i').addClass('fas fa-check')
    }
    if(id) {
        $.ajax({
            type:"POST",
            url:"ajax/updateStatus",
            data : { id: id, status: status },
            success: function (html) {
                console.log(status)

               //window.location.href = '/users';
            }
        });
    }
}) 
$('.affectCounselor').click(function() {
    var id = $(this).attr('data-id');
    var cp = $(this).attr('data-cp');
    var userName = $(this).attr('data-name');
    var estab      = $(this).attr('data-est');
    $('#affect-counselor #id-user').val(id);
    $('#affect-counselor #cp').val(cp);
    $('#affect-counselor #userName').val(userName);
    $('#affect-counselor #establis').val(estab);

    $('#affect-counselor #choose-counselor > option').each(function() {
        var cpCounselor = $(this).attr('data-cp');
        $(this).removeClass('hidden');
        if(cp != cpCounselor) {
            $(this).addClass('hidden');
        }
    });
    $('#affect-counselor').modal('show');
})
$('.affect-counselor-user').click(function() {
    var counselor = $('#affect-counselor #choose-counselor').val();
    var id        = $('#affect-counselor #id-user').val();
    var email     = $('#affect-counselor #choose-counselor').find(":selected").attr('data-email');
    var cp        = $('#affect-counselor #cp').val();
    var username  = $('#affect-counselor #userName').val();
    var establis  = $('#affect-counselor #establis').val();
    if(counselor && id) {
        $.ajax({
            type:"POST",
            url:"ajax/affectCounselor",
            data : { id: id, counselor: counselor,email: email,cp:cp,username:username,establis:establis},
            success: function (html) {
               //window.location.href = '/users';
               $('#affect-counselor').modal('hide');
            }
        });
    }
})
if($('#dataTable').length > 0){
    $('#dataTable').dataTable( {
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/French.json"
            }
        } );
}
console.log('tet')
$( "a[id^='deletePeriod_']" ).click(function(e) {
    var id = $(this).attr('data-periodId');
    console.log(id)
    $('#confirm-delete #periodId').val(id);
    $('#confirm-delete').modal('show');
})

$('.delete-period').click(function() {
    var id = $('#confirm-delete #periodId').val();
console.log(id)
    if(id) {
        $.ajax({
            type:"POST",
            url:"admin/delete_period/"+id,
            success: function (html) {
               window.location.href = '/period';
            }
        });
    }
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

$( "a[id^='deleteemail_']" ).click(function(e) {
    var id = $(this).attr('data-emailId');
    $('#confirm-delete #id-email').val(id);
    $('#confirm-delete').modal('show');
})

//chart radar
if($('#radarChart').length >0) {

      Highcharts.chart('radarChart', {

        chart: {
            polar: true,
            type: 'area'
        },
        title: {
            text: '',
        },

        pane: {
            size: '100%'
        },

        xAxis: {
            categories: [],
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>%{point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            area: {
                dataLabels: {
                    enabled: true
                },
            }
        },

        series: [{
            name: 'Votre résultat',
            data: [],
            pointPlacement: 'on'
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 800
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom'
                    },
                    pane: {
                        size: '90%'
                    }
                }
            }]
        }
    });
    var period = $('#choose-period').val();
    $.ajax({
        url: '/ajax/getResultPeriod/',
        method: 'POST',
        data: {period: period},
        success: function(result){
        
            var tabResult = JSON.parse(result);
            var chartRadar = $('#radarChart').highcharts();
        
            chartRadar.xAxis[0].setCategories(tabResult.dimensions);
            chartRadar.series[0].update({
              data: tabResult.result
            }, false); 
            
            chartRadar.redraw();    
        }
    });
}
$('#choose-period').on('change',function() {
    var period = $('#choose-period option:selected').val();
    $('.period-name').text(  $('#choose-period option:selected').text());
    $.ajax({
        url: '/ajax/getResultPeriod/',
        method: 'POST',
        data: {period: period},
        success: function(result){
        
            var tabResult = JSON.parse(result);
            var chartRadar = $('#radarChart').highcharts();
        
            chartRadar.xAxis[0].setCategories(tabResult.dimensions);
            chartRadar.series[0].update({
              data: tabResult.result
            }, false); 
            
            chartRadar.redraw();    
        }
    });
})
  
if( $('#container-chart-trend').length > 0) {
    //get the list of period
    //get the list of result for the active dimention
    var demensionName   =  $('.dimension-trend .active').attr('data-demension-name');
    $('.demension-name').text(demensionName);
    Highcharts.chart('container-chart-trend', {
        chart: {
            marginBottom: 200,
        },
 
        title: {
            text: ''
        },
     
        yAxis: {
            title: {
                text: 'Résultat'
            },
            min:0,
            max:100
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0
        },
     
        plotOptions: {
            series: {
                zones: [{
                   value: 10,
                    className: 'zone-0'
                }, {
                    value: 50,
                    className: 'zone-1'
                }, {
                    value: 80,
                    className: 'zone-2'
                }, {
                    className: 'zone-3'
                }],
                threshold: 0
            }
        },
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        },
 
        series: [{
            type: 'areaspline',
            data:[10, 20, 30]
        }]
    });
     var activeDimension = $('.dimension-trend li a.active').attr('data-id-demension');
    $.ajax({
        url: '/ajax/getResultTrend/'+activeDimension,
        success: function(result){
            var tabResult = JSON.parse(result);
            var chartTrend = $('#container-chart-trend').highcharts();
            chartTrend.xAxis[0].setCategories(tabResult.period);
            chartTrend.series[0].update({
               data: tabResult.moyenne,
               name : demensionName
            }, false);
            chartTrend.redraw(); 
        }
    });
    $('.dimension-trend li a').click(function() {
        var activeDimension = $(this).attr('data-id-demension');
        var demensionName   =  $(this).attr('data-demension-name');
        $('.demension-name').text(demensionName);
        if(activeDimension) {
            $.ajax({
                url: '/ajax/getResultTrend/'+activeDimension,
                success: function(result){
                    var tabResult = JSON.parse(result);
                    var chartTrend = $('#container-chart-trend').highcharts();
                    chartTrend.series[0].update({
                      data: tabResult.moyenne,
                      name : demensionName
                    }, false); 
                    chartTrend.redraw();  
                }
            });
        }
    })
}

if($('.situations').length > 0) {
    for (let pas = 0; pas <= 5; pas++) {
        Highcharts.chart('radar-situation-'+pas, {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            title: {
                text: ''
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#fff'],
                            [1, '#fff']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '107%'
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#fff',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#fff',
                labels: {
                    step: 3,
                    rotation: 'auto'
                },
                title: {
                    text: 'Score'
                },
                plotBands: [{
                    from: 0,
                    to: 43,
                    color: '#CA6F1E' // orange
                }, {
                from: 43,
                    to: 63,
                    color: '#F8C471' // yellow
                }, {
                    from: 63,
                    to: 80,
                    color: '#52BE80' // pgreen
                }, {
                    from: 80,
                    to: 100,
                    color: '#1D8348' // dgreen
                }]
            },

            series: [{
                name: 'Score',
                data: [],
                tooltip: {
                    valueSuffix: '%'
                }
            }]
        });
    }
    var period = $('#choose-period-situation').val();
    $.ajax({
        url: '/ajax/getResultPeriod/',
        method: 'POST',
        data: {period: period},
        success: function(result){
        
            var tabResult = JSON.parse(result);
            for (let res = 0; res <= 5; res++) {
                var chartsituation = $('#radar-situation-'+res).highcharts();
                
                chartsituation.series[0].update({
                  data: [tabResult.result[res]]
                }, false); 
                
                chartsituation.redraw(); 
            }  
        }
    });
    $('#choose-period-situation').on('change',function() {
        var period = $('#choose-period-situation option:selected').val();
        $('.period-name').text( $('#choose-period-situation option:selected').text());
        $.ajax({
            url: '/ajax/getResultPeriod/',
            method: 'POST',
            data: {period: period},
            success: function(result){
                var tabResult = JSON.parse(result);
                for (let res = 0; res <= 5; res++) {
                    var chartsituation = $('#radar-situation-'+res).highcharts();
                    
                    chartsituation.series[0].update({
                      data: [tabResult.result[res]]
                    }, false); 
                
                    chartsituation.redraw(); 
                }
            }
        });
    });
 
}
 $('#choose-period-deb').on('change',function() {
    var period = $('#choose-period-deb option:selected').val();
    //$('.period-name').text( $('#choose-period-deb option:selected').text());
    console.log(period)
    $.ajax({
        url: '/ajax/getDebriefing/',
        method: 'POST',
        data: {period: period},
        success: function(result){
            
            var tabResult = JSON.parse(result);
    
            for (var i = 0; i < tabResult.result.length; i++) {
                let res = tabResult.result[i];
                $('.faible-'+i).addClass('hidden');
                $('.fort-'+i).addClass('hidden');
                $('.dem-val-'+i).text(res+'%');
                var passing_score = $('#passing_score').attr('data-score')
                if(res > passing_score) {
                    $('.faible-'+i).addClass('hidden');
                    $('.fort-'+i).removeClass('hidden');
                    $('.mention-dem-'+i).text('(Point fort)');
                
                } else {
                    $('.fort-'+i).addClass('hidden');
                    $('.faible-'+i).removeClass('hidden');
                    $('.mention-dem-'+i).text("(Axe d'amélioration)");
             
                }
            }
        }
    });
})

$( "a[id^='deleteResource_']" ).click(function(e) {
    var id = $(this).attr('data-docId');

    $('#confirm-delete #docId').val(id);
    $('#confirm-delete').modal('show');
});
$('.delete-resource').click(function() {
    var id = $('#confirm-delete #docId').val();

    if(id) {
        $.ajax({
            type:"POST",
            url:"../ajax/deleteDoc",
            data : { id: id },
            success: function (html) {
               window.location.href = '/resource';
            }
        });
    }
});

//update question
$(".edit-prec" ).click(function(e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var value = $(this).attr('data-value');

    $('#updateRecommendations').modal('show');
    $('#updateRecommendations #value').val(value);
    $('#updateRecommendations #id-prec').val(id);
})
$('.update-prec').click(function() {
    var id    = $('#updateRecommendations #id-prec').val();
    var value = $('#updateRecommendations #value').val();

    if(id != '') {
        $.ajax({
            type:"POST",
            url:"./ajax/updateRecommendation",
            data : { id : id, value: value},
            success: function (html) {
               window.location.href = '/recommendations';
            }
        });
    }
    $('#updateRecommendations').modal('hide');
});

$('#updateScore').click(function(e) {
    e.preventDefault();
    var socre = $('#score').val();
    $.ajax({
        type:"POST",
        url:"./ajax/updateScore/"+socre, 
        success: function (html) {
          window.location.href = '/recommendations';
        }
    });
})

/** delete contact **/
$( "a[id^='deleteContact_']" ).click(function(e) {
    var id = $(this).attr('data-ContactId');
    $('#confirm-delete-contact #id-contact').val(id);
    $('#confirm-delete-contact').modal('show');
})

$('.delete-contact').click(function() {
    var id = $('#confirm-delete-contact #id-contact').val();

    if(id) {
        $.ajax({
            type:"POST",
            url:"ajax/deleteContact",
            data : { id: id },
            success: function (html) {
               window.location.href = '/';
            }
        });
    }
});
$('#sendMydata').click(function() {
   var period = $('#choose-period').val();
    $.ajax({
        url: '/ajax/savePdfResult/',
        method: 'POST',
        data: {period: period},
        success: function(result){  
            window.location.href = '/result';
        }
    }); 
})

window.addEventListener("load", function(){
    setInterval(function(){ 
        var chartRadar = $('#radarChart').highcharts();
        chartRadar.redraw();
    },1000);
});