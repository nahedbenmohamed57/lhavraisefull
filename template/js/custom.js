$(document).ready(function() {
	if ($('#save-news').length > 0) {
		$('#save-news').click(function () {
			var quoiNeuf = $('#quoi-neuf').val()
			//save news
			if (quoiNeuf) {
				$.ajax({
					type: "POST",
					url: "ajax/saveNews",
					data: {news: quoiNeuf},
					success: function (html) {
						window.location.href = '/dashboard';
					}
				});
			}
		})
	}
	if($('.resultChart').length >0) {
		$('.resultChart').each(function( index ) {
			let id = $(this).attr('id');
			var poleID = $('#'+id).attr('data-pole');
			var family =  $('#'+id).attr('data-family-id');
			var familyName = $('#'+id).attr('data-name');
		
			Highcharts.chart(id, {

				chart: {
					polar: true,
					type: 'area'
				},
				title: {
					text: '',
				},

				pane: {
					size: '100%'
				},

				xAxis: {
					categories: [],
				},

				tooltip: {
					shared: true,
					pointFormat: '<span style="color:{series.color}">{series.name}: <b>%{point.y:,.0f}</b><br/>'
				},

				legend: {
					align: 'right',
					verticalAlign: 'middle'
				},
				plotOptions: {
					area: {
						dataLabels: {
							enabled: true
						},
					}
				},

				series: [{
					name: 'Résultat '+familyName,
					data: [],
					pointPlacement: 'on'
				}],

				responsive: {
					rules: [{
						condition: {
							maxWidth: 900
						},
						chartOptions: {
							legend: {
								align: 'center',
								verticalAlign: 'bottom'
							},
							pane: {
								size: '100%'
							}
						}
					}]
				}
			});
			var period = $('#choose-period').val();
			$.ajax({
				url: '/resultat/getResultPeriod/',
				method: 'POST',
				data: {period: period,poleId:poleID,familyId:family},
				success: function(result){
					var tabResult = JSON.parse(result);
					var chartRadar = $('#'+id).highcharts();

					chartRadar.xAxis[0].setCategories(tabResult.factor);
					chartRadar.series[0].update({
						data: tabResult.result
					}, false);

					chartRadar.redraw();
				}
			});	
			$('#choose-period').on('change',function() {
		    	var period = $('#choose-period option:selected').val();
			    $.ajax({
					url: '/resultat/getResultPeriod/',
					method: 'POST',
					data: {period: period,poleId:poleID,familyId:family},
					success: function(result){
						var tabResult = JSON.parse(result);
						var chartRadar = $('#'+id).highcharts();

						chartRadar.xAxis[0].setCategories(tabResult.factor);
						chartRadar.series[0].update({
							data: tabResult.result
						}, false);

						chartRadar.redraw();
					}
				});	
			});
		});
	}

	/** chart etablissement **/
	if($('.resultChartEtab').length >0) {
		$('#showResult').click(function() {
			$('.resultChartEtab').each(function( index ) {
				let id = $(this).attr('id');
				var poleID      = $('#pole').val();
				var idPerimetre = $('#perimetre').val();
				var family      =  $('#'+id).attr('data-family-id');
				var familyName  = $('#'+id).attr('data-name');
			
				Highcharts.chart(id, {

					chart: {
						polar: true,
						type: 'area'
					},
					title: {
						text: '',
					},

					pane: {
						size: '100%'
					},

					xAxis: {
						categories: [],
					},

					tooltip: {
						shared: true,
						pointFormat: '<span style="color:{series.color}">{series.name}: <b>%{point.y:,.0f}</b><br/>'
					},

					legend: {
						align: 'right',
						verticalAlign: 'middle'
					},
					plotOptions: {
						area: {
							dataLabels: {
								enabled: true
							},
						}
					},

					series: [{
						name: 'Résultat '+familyName,
						data: [],
						pointPlacement: 'on'
					}],

					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom'
								},
								pane: {
									size: '100%'
								}
							}
						}]
					}
				});
				var period = $('#choose-period').val();
				$.ajax({
					url: '/resultat/getResultPeriodEtab/',
					method: 'POST',
					data: {period: period,poleId:poleID,familyId:family,id_perimetre:idPerimetre},
					success: function(result){
						var tabResult = JSON.parse(result);
						var chartRadar = $('#'+id).highcharts();

						chartRadar.xAxis[0].setCategories(tabResult.factor);
						chartRadar.series[0].update({
							data: tabResult.result
						}, false);

						chartRadar.redraw();
					}
				});	
			});
		})	
	}

	/** unit result **/

	/** chart etablissement **/
	if($('.resultChartUnit').length >0) {
		$('#showResultUnit').click(function() {
			$('.resultChartUnit').each(function( index ) {
				let id = $(this).attr('id');
				var poleID      = $('#pole').val();
				var idPerimetre = $('#perimetre').val();
				var family      =  $('#'+id).attr('data-family-id');
				var familyName  = $('#'+id).attr('data-name');
				var unitId      = $('#nombre_unite').val();
			
				Highcharts.chart(id, {

					chart: {
						polar: true,
						type: 'area'
					},
					title: {
						text: '',
					},

					pane: {
						size: '100%'
					},

					xAxis: {
						categories: [],
					},

					tooltip: {
						shared: true,
						pointFormat: '<span style="color:{series.color}">{series.name}: <b>%{point.y:,.0f}</b><br/>'
					},

					legend: {
						align: 'right',
						verticalAlign: 'middle'
					},
					plotOptions: {
						area: {
							dataLabels: {
								enabled: true
							},
						}
					},

					series: [{
						name: 'Résultat '+familyName,
						data: [],
						pointPlacement: 'on'
					}],

					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom'
								},
								pane: {
									size: '100%'
								}
							}
						}]
					}
				});
				var period = $('#choose-period').val();
				$.ajax({
					url: '/resultat/getResultPeriodUnit/',
					method: 'POST',
					data: {period: period,poleId:poleID,familyId:family,id_perimetre:idPerimetre,unitId:unitId},
					success: function(result){
						var tabResult = JSON.parse(result);
						var chartRadar = $('#'+id).highcharts();

						chartRadar.xAxis[0].setCategories(tabResult.factor);
						chartRadar.series[0].update({
							data: tabResult.result
						}, false);

						chartRadar.redraw();
					}
				});	
			});
		})	
	}


	/** action filter **/
	if($('#searchUnitAction').length >0) {
		$('#searchUnitAction').click(function () {
			let tr = $(".table-action tbody tr");
			$.each(tr, function(i) {
				$(tr[i]).removeClass('hidden');
				if(tr[i].cells[2].innerHTML !== $('#nombre_unite :selected').text() ){
					$(tr[i]).addClass('hidden');
				}
			});
		})
		$('#resetUnitAction').click(function () {
			$(".table-action tbody tr").removeClass('hidden');
			$('#pole').prop('selectedIndex', 0);
			$('#perimetre').empty();
			$('#nombre_unite').empty();
		})
	}
	if($('#resetFilter').length >0) {
		$('#resetFilter').click(function () {
			$('#pole').prop('selectedIndex', 0);
			$('#perimetre').empty();
		})
	}
	/* chart global result */
	if($('#resultChartGlobal').length >0) {
		$('#showGlobalResult').click(function() {
			//get the selected family
			let selectedFamily = $('#perimetre :selected').text();
			let idFamily = $('#perimetre :selected').val();
			let title = 'Resultat graphique de périmetre: '+selectedFamily;

			if(selectedFamily.length >0) {
				Highcharts.chart('resultChartGlobal', {
					chart: {
						type: 'area'
					},
					title: {
						text: title
					},
					xAxis: {
						categories: [],
						tickmarkPlacement: 'on',
						title: {
							enabled: false
						}
					},
					yAxis: {
						title: {
							text: 'Valeur'
						},
						labels: {
						}
					},
					tooltip: {
						split: true,
					},
					plotOptions: {
						area: {
							stacking: 'normal',
							lineColor: '#666666',
							lineWidth: 1,
							marker: {
								lineWidth: 1,
								lineColor: '#666666'
							}
						}
					},
					series: []
				});

				// get all result for all period
				$.ajax({
					url: '/resultat/getGlobalResultForAllPeriod/',
					method: 'POST',
					data: {'perimetre':idFamily},
					success: function(result){
						var tabResult = JSON.parse(result);
						console.log(tabResult)
						let periodLabel  = [];
						let currentChart = $('#resultChartGlobal').highcharts();
						$.each(tabResult, function(el,pr) {
							if(el == 'periods') {
								$.each(pr, function(element,period) {
									periodLabel.push(period.libelle)
								})
							} else {
								currentChart.addSeries({
									name: el,
									data: pr
								});
							}
						});
						currentChart.xAxis[0].setCategories(periodLabel);
						currentChart.redraw();
					}
				});
			}
		})
	};
});
