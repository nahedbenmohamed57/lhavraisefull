<div class="container-fluid">
    <h2 class="pb-5" style="color: rgb(38, 96, 133);">
        <strong>
            <?php
                if($task== "update") {
                    echo("Modifier l'");
                } else if($task== "show") {
                    echo("Détailles de l'");
                } else {
                    echo('Ajouter un ');
                }
                
                if(isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "établissement";
            ?>
        </strong>
    </h2>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" class="form-control" name="id_perimetre" value="<?php if(isset($perimetre)) {echo($perimetre->id_perimetre);} ?>"  id="perimetre_id" placeholder="">
            <form name="addperimetre" method="post" class="form-group" id="<?php if(isset($perimetre)) {echo('editperimetre');} else {echo('addperimetre');}?>">
                <div class="form-group" id="name-user-group">
                    <?php if(!$has_pole || $task == "update") { ?>
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="perimetre_nom"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control" name="pole_id">
                                <option></option>
                                <?php foreach ($poles as $key => $p) { ?>
                                    <option value="<?=$p->id_pole?>" <?php if($p->id_pole== $p_id) echo "selected" ?>><?=$p->pole_nom?></option>
                                <?php } ?>
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="perimetre_nom">Nom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="perimetre_nom" value="<?php if(isset($perimetre)) {echo($perimetre->perimetre_nom);} ?>"  id="perimetre_nom" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
      
                <?php if($task != "show"){ ?>
                    <div class="form-group" id="user-submit-group">
                        <button type="submit" class="btn submit btn btn-primary"
                            id="<?php   if(isset($perimetre)) 
                                            {echo('edit-perimetre');}
                                        else 
                                            {echo('add-perimetre');}?>">
                            <div class="glyphicon glyphicon-plus"></div>
                            <?php if(isset($perimetre)){ echo('Modifier'); } else { echo('Ajouter'); }?>
                        </button>

                       <!-- <a href="/pole" class="btn sousMenu submit mr-2">
                            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
                            <span style="color:#3e91b2ff;">Retour</span>
                        </a>-->
                    </div>
                <?php } ?>
                <div id="the-message-add-perimetre"></div>
            </form>
        </div>
    </div>
</div>
