<div class="container-fluid" style="margin:0px">
		<a href="/pole" class="btn sousMenu submit mr-2" style=''>
			<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
			<span style="color:#3e91b2ff;">Retour</span>
		</a>
			
		<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Ajouter un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "établissement"?> </strong></h2>
		
		<div class="row">
           
        <div class="col-md-12" style="margin-top: 20px;">
        	<?php $id_p_add=0; ?>
        	<span style='color: rgb(38, 96, 133); display: inline-block; font-size: 32px; margin-bottom: 32px;'><?php echo $pole->pole_nom; ?></span>
        	<a href="<?php echo base_url() ?>perimetre/crud_perimetre/add/<?php echo $id_p_add; ?>/<?=$p_id;?>" style="cursor: pointer;">
			<button class="sousMenu submit btn btn-primary float-right p-1" >
				<i class="fas fa-plus fa-titre" title="Ajouter"></i>
				
					<span style="color:#fff;">Ajouter une <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "établissement"?></span>
				
			</button>
			</a>
			<table id="tab" class='table display table-bordered'>
				<thead>
					<tr>
						<!-- <th>N°</th> -->
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($perimetres as $perimetre) {  ?>
					<tr>
						<!-- <td><?=$perimetre->id_perimetre?></td> -->
						<td><?=$perimetre->perimetre_nom?></td> 
						<td>
							<a href="<?php echo base_url();?>perimetre/crud_perimetre/delete/<?=$perimetre->id_perimetre;?>/<?=$p_id;?>">
								<i class="fas fa-trash-alt " title="Supprimer"></i>
							</a>
							<a href="<?php echo base_url();?>perimetre/crud_perimetre/update/<?=$perimetre->id_perimetre;?>/<?=$p_id;?>">
								<i class="fas fa-edit" title="Modifier"></i>
							</a>
							<!-- <a href="<?php echo base_url();?>perimetre/crud_perimetre/show/<?=$perimetre->id_perimetre;?>/<?=$p_id;?>">
								<i class="fas fa-eye" title="Vue"></i>
							</a> -->
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
        </div>
    </div>
</div>
