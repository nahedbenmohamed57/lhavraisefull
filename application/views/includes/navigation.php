<nav class="navigation-nav">
	<div class="navigation-nav-right">

		<a href="<?=base_url() . 'profil'?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mon profil" >
			<img src="<?=base_url() . 'assets/images/icons/man-user.png'?>" title="Mon profil">
		</a>
		<a href="<?=base_url() . 'support'?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Support">
			<img src="<?=base_url() . 'assets/images/icons/b-support.png'?>" title="Support">
		</a>

		<a href="<?=base_url() . 'login/logout'?> " title="Déconnxion">
			<img src="<?=base_url() . 'assets/images/icons/b-logout.png'?>">
		</a>
	</div>
</nav>



<script>
	
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky");
   navbar.setAttribute('style', 'display inline-blocks !important');
  } else {
    navbar.classList.remove("sticky");
    navbar.setAttribute('style', 'background-color: #f2ede1  !important;');

  }

}

</script>