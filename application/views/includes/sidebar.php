
<div class="wrapper student">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion shadow-lg sidebar-student <?php if(isset($closeMenu)) echo "toggled";?>"
        id="accordionSidebar">
		<?php
		if(isset($_SESSION['config'])) {?>
		<li class="nav-item"><a class="sidebar-brand d-flex align-items-center justify-content-center" href="#"><img width="150px" height="150px" alt="logo" src="/template/img/<?=$_SESSION['config']->image_platforme?>"></a></li>
        <?php }?>
		<?php foreach ($modules as $module) : ?>
            <?php if ($module->link != 'login/logout') : ?>
                <?php if ($module->module_name == "Accueil") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "1") echo "active"?> "><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-home"></i>Accueil</span></a></li>
                <?php elseif ($module->module_name == "Gérer les utilisateurs"): ?>
                    <li class="nav-item <?php if(isset($active) && $active == "2") echo "active"?> "><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-users"></i>Utilisateurs</span></a></li>
                <?php elseif ($module->module_name == "Définir les périmètres"): ?>
                    <li class="nav-item <?php if(isset($active) && $active == "3") echo "active"?> "><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-map-marker-alt"></i>Périmètres</span></a></li>
                <?php elseif ($module->module_name == "Paramétrer la méthode d'évaluation") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "4") echo "active"?> ">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseResult" aria-expanded="false" aria-controls="collapseResult">
                            <span><i class="fas fa-cog"></i>Méthode</span></a>
                        <div id="collapseResult" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <a class="collapse-item" href="/family">Méthode indicateurs</a>
                                <a class="collapse-item" href="/facteurs">Méthode facteurs RPS</a>
                                <a class="collapse-item" href="/facteurs/correlation">Méthode corrélations</a>
                            </div>
                        </div>
                    </li>
                <?php elseif ($module->module_name == "Support utilisateur"): ?>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-home"></i>Support</span></a></li>
                <?php elseif ($module->module_name == "Paramétrer la période d'évaluation") : ?>
                    <li class="nav-item <?php if(isset($activeP) && $activeP == "5") echo "active"?> "><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-calendar-alt"></i>Période</span></a></li>
                <?php elseif ($module->module_name == "Saisir les indicateurs") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "6") echo "active"?>"><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-calculator"></i>Saisir les indicateurs</span></a></li>
                <?php elseif ($module->module_name == "Evaluer les RPS") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "7") echo "active"?>"><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-tachometer-alt"></i>Evaluer les RPS</span></a></li>
				<?php elseif ($module->module_name == "Résultats") : ?>
					<li class="nav-item <?php if(isset($active) && $active == "8") echo "active"?> ">
						<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseResult2" aria-expanded="false" aria-controls="collapseResult2">
							<span><i class="fas fa-poll"></i>Résultats</span></a>
						<div id="collapseResult2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
							<div class="bg-white py-2 collapse-inner rounded">
								<a class="collapse-item" href="/resultat/generalResult">Vue d'ensemble</a>
								<a class="collapse-item" href="/resultat/etabResult">Résultats par <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "Etablissement"?></a>
								<a class="collapse-item" href="/resultat/unitResult">Résultats par <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "unité de travail"?></a>
								<a class="collapse-item" href="/resultat/globalResultAllPeriod">Résultats globale</a>
							</div>
						</div>
					</li>
                <?php elseif ($module->module_name == "Plan d'actions") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "9") echo "active"?>"><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-lightbulb"></i>Plan d'actions</span></a></li>
                <?php elseif ($module->module_name == "Ressources : uploader") : ?>
                    <li class="nav-item <?php if(isset($active) && $active == "10") echo "active"?>"><a class="nav-link" href="<?= base_url() . $module->link ?>"><span><i
                                        class="fas fa-file-download"></i>Ressources : uploader</span></a></li>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
		<li>	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
				<img width="150px" height="150px" alt="logo" src="/template/img/p3-CONSEIL.png"></a></li>
    </ul>
</div>
