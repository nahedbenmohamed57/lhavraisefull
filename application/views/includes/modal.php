<!-- (Ajax Modal)-->
<div class="modal fade" id="modal_ajax">
	<div class="modal-dialog" >
		<div class="modal-content" style="margin-top:100px;"> 
			<div class="modal-header">
				<h4 class="modal-title"><?php echo($titre); ?></h4>
			</div>
			<div class="modal-body" style="height:255px; overflow:auto;">
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<span id="preloader-delete"></span>
				</br>
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>

<!-- (Normal Modal)-->
<div class="modal fade" id="modal_delete">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<h4 class="modal-title" >Êtes vous sûr de vouloir supprimer ?</h4>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<span id="preloader-delete"></span>
				</br>
					<a class="btn btn-danger btn-ok">Effacer</a>
				  <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>	
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                ...
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <a class="btn btn-danger btn-ok">Supprimer</a>
            </div>
        </div>
    </div>
</div>

<script>

$('#modal_delete').on('show.bs.modal', function(e) {
  	$( ".btn-ok" ).click(function() {
		var url = $(e.relatedTarget).data('href'); 
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: url,
			success: function (resp) {
				 /*resp = resp.split(',');*/
				 console.log(resp); 
				//var url = '<?php echo base_url();?>' + 'family/determinant/update/'+ resp[0] + '/' + resp[1]  ;
				var url = '<?php echo base_url();?>' + resp ;
				location.replace(url);
			},
		});
	});
});

</script>
