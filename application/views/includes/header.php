<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="duerp">
<meta name="author" content="3Clcik Solutions">
<meta name="google-signin-client_id" content="https://3click-solutions.com/">
<title>DUERP</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link href="<?=base_url()?>assets/css/style_template.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/style_home.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/users.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/modal.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!--<link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<link href="http://fr.allfont.net/allfont.css?fonts=open-sans-extrabold" rel="stylesheet" type="text/css" />

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="<?=base_url()?>assets/js/jquery-1.11.2.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/template.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/home.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/users.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/login.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/modal.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/jquery.dataTables.min.js"></script>
<!--<script src="<?=base_url()?>assets/js/qvt_js/chosen.jquery.min.js"></script>
-->

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
