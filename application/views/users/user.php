<style type="text/css">
    .bloc-choice {
    max-height: 200px;overflow: auto;background: #fff;border: 2px solid #cccccc36;padding: 15px;box-shadow: -16px 21px 15px -6px rgba(102,170,136,0.32);border-radius: 7px;
    }
  .bloc-choice ul {
    padding: 0;list-style: none;
  }
  .bloc-choice ul li {
    display: flex;width: 100%;
  }
</style>
<?php
    if (!isset($profil))
        if ($task == "update" && $user->user_id == $_SESSION['user_id']) $titre = ("Modifier mon profil");
        else if ($task == "update") $titre = ("Modifier le profil");
        else if($task == "show") $titre = ("Détails de l'utilisateur");
        else $titre = ('Ajouter un utilisateur');
        
        // $titre .= ("mon profil");
    
    if ($monprofil) {
        $titre = ("Modifier mon profil");
    }
?>

<div class="container-fluid" style="margin:0px">
    <div class="row">
        <input type="hidden" class="form-control" name="user_id" value="<?php if(isset($user)) {echo($user->user_id);} ?>"  id="user_id" placeholder="">
        
        <div class="col-md-8 ">
            <?php if ($updated) : ?>
            <div class="alert alert-success text-center"><?= $monprofil == TRUE ? 'Votre' : 'Le' ?> profil a été mis à jour avec succès</div>
            <?php endif; ?>
            
            <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);"><?=$titre?> </strong></h2>

            <div class="col-md-4"></div><div class="col-md-2"></div>
            <form name="addUsers" method="post" class="form-group" 
                id="<?php   if(isset($user)) {echo('editUser');}
                            else {echo('addUser');}?>">
                <div class="form-group" id="name-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_user">Prénom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_firstname" value="<?php if(isset($user)) {echo($user->user_firstname);} ?>"  id="name_user" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="lastname-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="lastname_user">Nom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_lastname" id="lastname_user" value="<?php if(isset($user)) {echo($user->user_lastname);} ?>" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="email-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="email_user">Email</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="email" class="form-control" name="user_email" id="email_user" value="<?php if(isset($user)) {echo($user->user_email);} ?>" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="user-id-access-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="user_role">Rôle</label>
                        </div>
                        <?php if(!isset($profil)){?>
                            <div class="col-sm-9 col-8">
                                <select name="user_role" class="form-control" id="user_role">
                                    <?php if($task != "show"){ ?>
                                        <option value="">Choisir le rôle</option>
                                    <?php }  foreach($roles as $role) { 
                                            if($role->role_id == $user->user_role){ ?>
                                                <option value="<?=$role->role_id;?>" selected><?=$role->role_name;?></option>
                                            <?php } else if($task != "show"){ ?>
                                                <option value="<?=$role->role_id;?>"><?=$role->role_name;?></option>
                                            <?php } 
                                    } ?>
                                </select>
                                <small class="help-block"></small>

                            </div>
<?php if($user->user_role == 3){
	$user_perm = json_decode($user->user_perm);
    $user_pole = json_decode($user->user_pole);
    $user_unit = json_decode($user->user_unit);?>
	<div class="col-sm-3 col-4 mt-3">
		<label for="user_pole">Pôle</label>
	</div>
	<div class="col-sm-9 col-8 mt-3 bloc-choice">
		<ul>
		<?php foreach ($poles as $pol) {?>
			<li>
				<div class="custom-control custom-checkbox small">
				<input type="checkbox" class="custom-control-input" id="pole_<?=$pol->id_pole?>" name="user_pole[]" value="<?=$pol->id_pole?>" <?php if(in_array($pol->id_pole, $user_pole)) echo "checked";?>>
				<label class="custom-control-label" for="pole_<?=$pol->id_pole?>"><?=$pol->pole_nom?></label>
			</div>
				</li>
		<?php }?>
		</ul>
	</div>
    <div class="col-sm-3 col-4 mt-3">
        <label for="user_pole">Etablissement</label>
    </div>
    <div class="col-sm-9 col-8 mt-3 bloc-choice">
        <ul class="list-perim">
        <?php foreach ($perimetres as $per) {?>
            <li>
                <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input permimetre-el" id="perimetre_<?=$per->id_perimetre?>" name="user_perm[]" value="<?=$per->id_perimetre?>" <?php if(in_array($per->id_perimetre,$user_perm)) echo "checked";?> data-pole="<?=$per->pole_id?>">
                <label class="custom-control-label" for="perimetre_<?=$per->id_perimetre?>"><?=$per->perimetre_nom?></label>
            </div>
                </li>
        <?php }?>
        </ul>
    </div>
     <div class="col-sm-3 col-4 mt-3">
        <label for="user_pole">Unité</label>
    </div>
    <div class="col-sm-9 col-8 mt-3 bloc-choice">
        <ul>
        <?php foreach ($units as $unit) {?>
            <li>
                <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input unit-el" id="unit_<?=$unit->id_unite?>" name="user_unit[]" value="<?=$unit->id_unite?>" <?php if(in_array($unit->id_unite,$user_unit)) echo "checked";?> data-pole="<?=$unit->pole_id?>" data-perimetre="<?=$unit->perimetre_id?>">
                <label class="custom-control-label" for="unit_<?=$unit->id_unite?>"><?=$unit->unite_nom?></label>
            </div>
                </li>
        <?php }?>
        </ul>
    </div>
							</div>
							<?php }?>
                        <?php } else {?>
                            <div class="col-sm-9 col-8">
                                <input type="user_role" class="form-control" name="user_role" id="user_role" 
                                value="<?php if(isset($user)) {echo($user->user_role->role_name);} ?>" disabled>
                                <small class="help-block"></small>
                            </div>
                        <?php } ?>
                    </div>
                    <small class="help-block"></small>
                </div>

                <?php if(isset($profil)) {?>
                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password">Mot de passe</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="password" class="form-control" name="user_password" id="password_user" placeholder="******">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">Confirmer mot de passe</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="password" class="form-control" name="password_user_confirm" id="password_user_confirm" placeholder="******">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                <div class="form-group" id="password-user-group">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <label for="user_password">Mot de passe</label>
                        </div>
                        <div class="col-sm-8 col-8">
                            <input type="password" class="form-control" name="user_password" id="password_user" placeholder="******">
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <?php } ?>
	</div>
                <?php if($task != "show"){ ?>
                    <div class="form-group" id="user-submit-group">
						<a href="/users" class="btn sousMenu submit mr-2">
							<i class="fas fa-arrow-left fa-titre position-relative" title="Retour"></i>
							<span style="color:#3e91b2ff;">Retour</span>
						</a>
                        <button type="submit" class="submit btn btn-primary" id="<?php if(isset($user))  {echo('edit-user');} else  {echo('add-user');}?>">
                            <div class="glyphicon glyphicon-plus"></div>
                            <?php if(isset($user)){ echo('Modifier'); } else { echo('Ajouter'); }?>
                        </button>

                    </div>
                <?php } ?>
                <div id="the-message-add-user"></div>
            </form>
        </div>

        <br>
		
    </div>
    
</div>
<script type="text/javascript">
    var selectedPole = [];
    var selectedPerimetre = [];
    $('[id^="pole_"]').on('change', function () {
        var pole = $(this).val();
		if($(this).is(':checked')) {
           selectedPole.push(pole);
        
		} else {
            const index = selectedPole.indexOf(pole);
            if (index > -1) {
              selectedPole.splice(index, 1);
            }
        }
        //show only permimetre and unit for the selected pole
        $('.permimetre-el').each(function( index ) {
            if(selectedPole.includes($( this ).attr('data-pole'))){
                $(this ).parent().css('display','initial') ;
            } else {
                $( this ).parent().css('display','none'); 
            }
        });
        $('.unit-el').each(function( index ) {
            if(selectedPole.includes($( this ).attr('data-pole'))){
                $(this ).parent().css('display','initial') ;
            } else {
                $( this ).parent().css('display','none'); 
            }
        });

        if(selectedPole.length == 0) {
            $('.permimetre-el').each(function( index ) {
                $(this ).parent().css('display','initial') ;
            });
        }
        if(selectedPerimetre.length == 0 && selectedPole.length == 0) {
            $('.unit-el').each(function( index ) {
                $(this ).parent().css('display','initial') ;
            });
        }
    });

    $('[id^="perimetre_"]').on('change', function () {
        var perim = $(this).val();
        if($(this).is(':checked')) {
           selectedPerimetre.push(perim);
        } else {
            const index = selectedPerimetre.indexOf(perim);
            if (index > -1) {
              selectedPerimetre.splice(index, 1);
            }
        }
        //show only permimetre and unit for the selected pole
        $('.unit-el').each(function( index ) {
            if(selectedPerimetre.includes($( this ).attr('data-perimetre'))){
                $(this ).parent().css('display','initial') ;
            } else {
                $( this ).parent().css('display','none'); 
            }
        });
        if(selectedPerimetre.length == 0 ) {
            $('.unit-el').each(function( index ) {
                if(selectedPole.includes($( this ).attr('data-pole'))){
                    $(this ).parent().css('display','initial') ;
                } else {
                    $( this ).parent().css('display','none'); 
                }
            });
        }

        if(selectedPerimetre.length == 0 && selectedPole.length == 0) {
            //show all unit if no pole or perim selected
            $('.unit-el').each(function( index ) {
                $(this ).parent().css('display','initial') ;
            });
        }
    });
</script>
