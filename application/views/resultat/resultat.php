<h1 style="color: rgb(38, 96, 133);">Résultat de l'évaluation</h1>

<h2 style="color: rgb(38, 96, 133);"><?php
	switch ($focus) {
		case 1: echo "Résultats généraux"; break;
		case 2: echo "Résultats par";if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"; break;
		case 3: echo "Résultats par"; if (isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "établissement"; break;
		case 4: echo "Résultats par "; if (isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "unité de travail"; break;
		case 5: echo "Tendances RPS"; break;
		case 6: echo "Tendances indicateurs"; break;
	}
	?></h2>
<div class="row" style="margin-bottom:25px">
	<div class="col-md-4">
		<select class="form-control" name="period" id="period" onchange="init()">
			<option value="0">--Choisir période--</option>
			<?php foreach ($periodes as $key => $periode) { ?>
				<option value="<?=$periode->id?>"><?=$periode->libelle?></option>
			<?php  }?>
		</select>
	</div>
	<div class="col-md-8">
		<span id="focus" style="display: none"><?=$focus?></span>
		<?php if($focus == 2){?>
			<div class="row">
				<select class="col-md-4 form-control" id="pole">
					<option value="0">--Choisir <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?>--</option>
					<?php foreach ($poles as $key => $p) { ?>
						<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
					<?php } ?>
				</select>
				<div class="col-md-4">
					<button class="sousMenu submit w-100 btn btn-primary p-2" style="width: 100%" onclick="init()">Charger</button>
				</div>
			</div>
		<?php } ?>
		<?php if($focus == 3){?>
			<div class="row">
				<select class="col-md-4 form-control" id="perimetre">
					<option value="0">--Choisir <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>--</option>
					<?php foreach ($perimetres as $key => $pr) { ?>
						<option value="<?=$pr->id_perimetre?>"><?=$pr->perimetre_nom?></option>
					<?php } ?>
				</select>
				<div class="col-md-4">
					<button class="sousMenu submit w-100 btn btn-primary p-2" style="width: 100%" onclick="init()">Charger</button>
				</div>
			</div>
		<?php } ?>

		<?php if($focus == 4){?>
			<div class="row">
				<select class="col-md-4 form-control" id="unit">
					<option value="0">--Choisir <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "Unité"?> --</option>
					<?php foreach ($units as $key => $u) { ?>
						<option value="<?=$u->id_unite?>"><?=$u->unite_nom?></option>
					<?php } ?>
				</select>
				<div class="col-md-4">
					<button class="sousMenu submit w-100 btn btn-primary p-2" style="width: 100%" onclick="init()">Charger</button>
				</div>
			</div>
		<?php } ?>
		<?php if($focus == 5){?>
			<div class="row">
				<select class="col-md-4 form-control" id="unit">
					<option value="0">--Choisir <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "Unité"?> --</option>
					<?php foreach ($units as $key => $u) { ?>
						<option value="<?=$u->id_unite?>"><?=$u->unite_nom?></option>
					<?php } ?>
				</select>
				<div class="col-md-4">
					<button class="sousMenu submit w-100 btn btn-primary p-2" style="width: 100%" onclick="init()">Charger</button>
				</div>
			</div>
		<?php } ?>
		<?php if($focus == 6){?>
			<div class="row">
				<select class="col-md-4 form-control" id="unit">
					<option value="0">--Choisir <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "Unité"?>--</option>
					<?php foreach ($units as $key => $u) { ?>
						<option value="<?=$u->id_unite?>"><?=$u->unite_nom?></option>
					<?php } ?>
				</select>
				<div class="col-md-4">
					<button class="sousMenu submit w-100 btn btn-primary p-2" style="width: 100%" onclick="init()">Charger</button>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<div class="row">

		<table border=0 cellspacing=0 cellpadding=0 >
			<tbody>
			<tr>
				<td valign="top"  width="20%" align="right">
					<div>
						<table class="table table-bordered">
							<thead>
							<tr >
								<th rowspan="2" valign="middles" style="vertical-align:middle">Familles de RPS</th>
								<th >Facteurs de RPS</th>
							</tr>
							<tr>
								<th>&nbsp;</th>
							</tr>
							</thead>

							<tbody>
							<?php foreach ($family_rps as $family) : ?>
								<tr>
									<td rowspan="<?=sizeof($family->facteur) + 1?>" style="word-break:break-word">
										<p ><?=$family->family_rps_name?></p></td>
								</tr>

								<?php foreach ($family->facteur as $index => $facteur) : ?>
									<tr>
										<td><?=$index + 1?>. <?=$facteur->facteur_name?></td>
									</tr>
								<?php endforeach; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</td>
				<td width="80%">

						<div id="load-table" style="overflow-x: auto; white-space: nowrap;"></div>

				</td>
			</tr>
			</tbody>
		</table>

</div>

<button style="background: #67AA87; color: white;" class="btn mt-3 mb-3">Exporter .xls</button>

<script>
    function init() {
        var dataTable = document.getElementById('load-table')
        dataTable.innerHTML = "<p class='m-5 p-5 text-center bg-light'><i class='fa fa-circle-notch fa-spin'></i> Loading</p>"

        load()
    }

    function load() {
        $('#load-btn').text('Chargement en cours...');
        var focus = $('#focus').text();
        var pole  = $('#pole').val();
        var etab  = $('#perimetre').val();
        var unit  = $('#unit').val();
        var period = $('#period').val();
        focus = +focus+'/'+pole+'/'+etab+'/'+unit+'/'+period;
        console.log(period);
        console.log(focus);
        $.get(`/resultat/data/`+focus, function(response) {
            var dataTable = document.getElementById('load-table')
            dataTable.innerHTML = response
        })
    }

    init()
</script>

<style>
	table tbody td tr {
		white-space: nowrap;
		overflow: auto;
		text-overflow: ellipsis;
	}

	#load-table {
		white-space: nowrap;
		overflow: auto;
		color: #323e41;
		background-color:#abdde5;
		text-overflow: ellipsis;


	}
</style>
