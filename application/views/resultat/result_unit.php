<div class="container-fluid">
    <h1>
        <strong style="color: rgb(38, 96, 133)">
		Résultat de l'évaluation
        </strong><br>
         
    </h1>
    <h2>
    	<strong style="color: rgb(38, 96, 133)">
		Résultats par unités de travail : 
		<?php foreach ($units as $key => $u) { 
			if($u->id_unite == $unit_id ) { ?>
				<span><?=$u->unite_nom?></span>
		<?php }
			} ?>

        </strong>
    </h2>
	<div id="load-table" class='mt-5'></div>
	<div class="row">
		<form name="form" role="form"  class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
			<?php foreach($families_rps as $family){ ?>

			<table class="table-bordered" width="100%" style="margin-bottom: 58px">	
			 	<tr>
			  		<th >Famille</th>
			   		<th>Facteur</th>
			    	<th >Description</th> 
			    	<th colspan="3">Eposition (EX)</th>
			    	<th colspan="2">Impact</th>  
			    	<th>Niveau de risque</th>
				</tr>
				<tr> 
			 		<th></th> 
			 		<th></th> 
			 		<th></th>
			 	 	<th>Intensité(I)</th>
			 	  	<th>Priorité (P)</th> 
			 	  	<th>Eposition (EX)</th>
			 	  	<th>Impact (IM)</th>
			 	  	<th>Fiabilité</th>
			 	  	<th><span id="nv-risque"></span></th>
			 	</tr> 
			 	<?php $i= 1;
			 	    foreach($facteurs as $facteur){ 
					    if($facteur->family_rps_id == $family->family_id){ $i++;}
					}?>
			 	<tr> 
					<th rowspan="<?=$i?>"><?=$family->family_rps_name;?></th> 
                  <?php $i= 0;
			 	    foreach($facteurs as $facteur){ 
					    if($facteur->family_rps_id == $family->family_id){
					    	$current_elemnt = [];
					    	foreach ($evaluation_qantitive as $key => $eval) {
					    	 	if($eval->facteur_id == $facteur->facteur_id) {
					    	 		$current_elemnt = $eval;
					    	 	}
					    	}?>
                    <tr>
                    	<th colspan=""><?=$facteur->facteur_name?></th>
                    	<td><?php if(!empty($current_elemnt)) echo $current_elemnt->description?></td>
                    	<td class="text-center intensity" id ="intensity_<?=$family->family_id?>_<?=$facteur->facteur_id?>" data-info="<?=$family->family_id?>_<?=$facteur->facteur_id?>"><?php if(!empty($current_elemnt)) echo $current_elemnt->intensity?>
                    	</td> 
					    <td class="text-center priority" id ="priority_<?=$family->family_id?>_<?=$facteur->facteur_id?>" data-info="<?=$family->family_id?>_<?=$facteur->facteur_id?>">
					    	<?php if(!empty($current_elemnt)) echo $current_elemnt->priority?>
                    	</td> 
					    <td id="ex_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center"></td>
					   		 <?php $color_impact = '#fff';
					    		if($facteur->impact > 0 && $facteur->impact < 2) {
									$color_impact = '#66aa88';
								}else if($facteur->impact >= 2 && $facteur->impact < 3) {
									$color_impact = '#ffc000';
								}
								else if($facteur->impact>= 3 && $facteur->impact < 4) {
									$color_impact = '#f79646';
								}
								else if($facteur->impact >= 4 && $facteur->impact < 5) {
									$color_impact = '#c0504d';
								}else if($facteur->impact >= 5) {
									$color_impact = '#000000';
								}?>

					    <td id="impact_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center" style="background: <?=$color_impact?>">

					    	<?=$facteur->impact?>
					    		
					    	</td>
					    <?php $color_fiabilite = '#fff';
					    		if($facteur->fiabilite > 0 && $facteur->fiabilite < 2) {
									$color_fiabilite = '#66aa88';
								}else if($facteur->fiabilite >= 2 && $facteur->fiabilite < 3) {
									$color_fiabilite = '#ffc000';
								}
								else if($facteur->fiabilite>= 3 && $facteur->fiabilite < 4) {
									$color_fiabilite = '#f79646';
								}
								else if($facteur->fiabilite >= 4 && $facteur->fiabilite < 5) {
									$color_fiabilite = '#c0504d';
								}else if($facteur->fiabilite >= 5) {
									$color_fiabilite = '#000000';
								}?>
					    <td class="text-center" style="background: <?=$color_fiabilite?>">
					    	<?=$facteur->fiabilite?>
					    </td>
					    <td id="NvRisq_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center">0</td>	
                	</tr>
                	<?php }
                }?>

				</tr> 

			</table>
	  	<?php }
	  	$rps_evaluation_id = null;
	  	if(!empty($current_elemnt)) $rps_evaluation_id = $current_elemnt->rps_evaluation_id ?>
	  	<!--<div class="form-group" style="margin-top: 20px;">
            <button type="submit" class="submit  btn btn-primary">
                Sauvegarder
            </button>
           -->
        </div>
	 </form>

	 </div>
<br>
</div>
<script type="text/javascript">
	$( ".intensity" ).each(function( index ) {
		var intensity = $(this).text();
		var info      = $(this).data('info');
		var priority  = $('#priority_'+info).text() ;
		var ex = parseInt(intensity)+parseInt(priority);
		if (isNaN(ex)) {
		    ex = 0;
		}
		var color = '#fff';
		if(ex > 0 && ex < 2) {
			color = '#66aa88';
		}else if(ex >= 2 && ex < 3) {
			color = '#ffc000';
		}
		else if(ex >= 3 && ex < 4) {
			color = '#f79646';
		}
		else if(ex >= 4 && ex < 5) {
			color = '#c0504d';
		}else if(ex >= 5) {
			color = '#000000';
		}
		$('#ex_'+info).css('background', color);
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();

		

		var nvRisq = (ex+parseFloat(impact))/2;

		var color_NvRisq = '#fff';
		if(nvRisq > 0 && nvRisq < 2) {
			color_NvRisq = '#66aa88';
		}else if(nvRisq >= 2 && nvRisq < 3) {
			color_NvRisq = '#ffc000';
		}
		else if(nvRisq >= 3 && nvRisq < 4) {
			color_NvRisq = '#f79646';
		}
		else if(nvRisq >= 4 && nvRisq < 5) {
			color = '#c0504d';
		}else if(nvRisq >= 5) {
			color_NvRisq = '#000000';
		}
		$('#NvRisq_'+info).css('background', color_NvRisq);
		$('#NvRisq_'+info).text(nvRisq);

	});
</script>
