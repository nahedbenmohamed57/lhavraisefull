<div class="container-fluid result-page">

	<div class="row">
		<ul>
			<li>
				<a href="/resultat/show/1">Résultats généraux <?php if(isset($_SESSION['config'])) echo ucwords($_SESSION['config']->name_platforme); else echo "Ligue Havraise"?></a>
			</li>

			<li>
				<a href="/resultat/show/2">Résultats par <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?></a>
			</li>

			<li>
				<a href="/resultat/show/3">Résultats par <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "établissement"?></a>
			</li>

			<!--<li>
				<a href="/resultat/show/4">Résultats par unités de travail</a>
			</li>-->
		</ul>
	</div>
</div>
