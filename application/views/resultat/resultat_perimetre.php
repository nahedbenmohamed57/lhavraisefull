<a  href="<?php echo base_url();?>evaluation/export_evaluation/<?=$id_perimetre;?>">
    <button class="sousMenu submit" style="margin-right: 27px;cursor: pointer;">
        <i class="fas fa-titre" title="Ajouter"></i>
        <span style="color:#fff;">Exporter résultat</span>
    </button>
</a>

<button onclick="window.history.back()" class="btn sousMenu submit mr-2">
    <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
    <span style="color:#3e91b2ff;">Retour</span>
</button>

<?php
    $nb=0;
    for($j=0; $j<sizeof($families); $j++)
    {   
        for($i=0; $i< sizeof($deters); $i++) 
        {
            if($deters[$i]->family_id == $families[$j]->family_id )
            {
             $dataPoints[$nb] = array( "label"=>"".$deters[$i]->determinant_name , "y"=>$deters[$i]->determinant_importance*25);
             $nb++;
            }
        }
    }
    for ($j=0; $j <9 ; $j++) 
    { 
        $dataPoints1[$j]=$dataPoints[$j]['y'];
        $dap[$j]=$dataPoints[$j]['label'];
    }   
    $nb=1;
    for ($k=0; $k < sizeof($t_avan); $k++) 
    { 
        $tab_avan[$k]=$t_avan[$nb];
        $nb++;
    }
    for ($k=0; $k <sizeof($dataPoints1) ; $k++) 
    { 
        $t_pro[$k]=round((($dataPoints1[$k]+100)-$tab_avan[$k])/2);
    }
    $dataPoints1[sizeof($dataPoints1)]=0;
    $tab_avan[sizeof( $tab_avan)]=0;
    $t_pro[sizeof( $t_pro)]=0;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<div class="container-fluid-fluid">
    <div class="row">
        <div class="col-md-10 col-sm-12 mb-5">
            <canvas class="pro_canva" id="bar-chart3"></canvas>
        </div>

        <div class="col-md-10 col-sm-12 mb-5">
            <canvas class="canva" id="bar-chart"></canvas>
        </div>

        <div class="col-md-10 col-sm-12 mb-5">
            <canvas class="canva" id="bar-chart2" ></canvas>
        </div>
    </div>
</div>

<script>
window.onload = function () {
    new Chart(document.getElementById("bar-chart3"), {
    type: 'horizontalBar',
    data: {
      labels: <?php echo json_encode($dap); ?>,
      datasets: [
        { 
        backgroundColor: "#266085",
        data: <?php echo json_encode($t_pro); ?>
        }
      ]
    },
    options: {
      scales: { xAxes: [{ display: true }] },
      legend: { display: false },
      title: {
        fontSize: 18,
        fontStyle: 'bold',
        fontColor: '#970A2C',
        display: true,
        fontsize: 18,
        text: 'Priorite des déterminants',
      }

    }
});
new Chart(document.getElementById("bar-chart"), {
    type: 'horizontalBar',
    data: {
      labels: <?php echo json_encode($dap); ?>,
      datasets: [
        { 
        backgroundColor: "#266085",
        data: <?php echo json_encode($dataPoints1); ?>
        }
      ]
    },
    options: {
      scales: { xAxes: [{ display: true }] },
      legend: { display: false },
      title: {
        fontSize: 18,
        fontStyle: 'bold',
        fontColor: '#970A2C',
        display: true,
        fontsize: 18,
        text: 'Importance des déterminants',
      }

    }
});
new Chart(document.getElementById("bar-chart2"), {
    type: 'horizontalBar',
    data: {
      labels: <?php echo json_encode($dap); ?>,
      datasets: [
        { 
        backgroundColor: "#266085",
        data: <?php echo json_encode($tab_avan); ?>
        }
      ]
    },
    options: {
      scales: { xAxes: [{ display: true }] },
      legend: { display: false },
      title: {
        fontSize: 18,
        fontStyle: 'bold',
        fontColor: '#970A2C',
        display: true,
        fontsize: 18,
        text: 'Avancement global des déterminants',
      },
    }
});
}
</script>
<script type="text/javascript">
$( document ).ready(function() {
    var deters = <?php echo json_encode($deters); ?>;
    var families = <?php echo json_encode($families); ?>;
    var family_id_0 = families[0]["family_id"]; 
    var id_per=<?php echo json_encode($id_perimetre); ?>;
    var ids = []; 
    for(var j=0; j<families.length; j++){
        for(var i=0; i< deters.length; i++) {   
            if(deters[i]["family_id"] == families[j]["family_id"] ){
                var heigthDeter = 0; 
                var idDeter = deters[i]["determinant_id"] ; 
                var height = 0; 
                var avancement =0; 
                var nbrDevice= 0; 
                //get Liste Device
                $.ajax({
                    type: 'POST',
                    url: window.location.href+"/getListDevice/" +  idDeter,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    async:false,
                    success: function (res) {
                        var deterId = res["idDeter"] ; 
                        var lengthDevice = res["device"].length; 
                        nbrDevice = lengthDevice ; 

                        for(var i=0; i< lengthDevice; i++){
                            //insert device 
                            $( "#device"+ res["idDeter"] ).append( "<p id=dev"+ res["device"][i]['device_id'] +">" + res["device"][i]["device_name"] + "</p>" );             
                            
                          /*  if(i != res["device"].length -1){
                                $( "#dev"+  res["device"][i]['device_id'] ).append( "<hr id=hrdev"+ res["device"][i]['device_id']+">");        
                            }*/
                            
                            //insert cotation 
                            $.ajax({
                                type: 'POST',
                                url: window.location.href+"/getEvaluationCotation2/" +  res["device"][i]['device_id'] + "/" + deterId + "/" + id_per,
                                dataType: 'json',
                                contentType: false,
                                processData: false, 
                                async:false,
                                success: function (res) {
                                    //cotation  
                                   $( "#cotation"+ res["idDeter"] ).append( "<p id=cot"+res["idDevice"] + "-"+ res["cotation"]['listing_id'] +">" + res["cotation"]["listing_name"] + "</p>" )
                                   //avancement 
                                    $( "#avancement"+ res["idDeter"] ).append( "<p id=avc"+res["idDevice"] + "-" + res["cotation"]['listing_id'] +">" + res["cotation"]["avancement"] + "%</p>" );      
                                    avancement = avancement + parseInt(res["cotation"]["avancement"]) ;        
                                    var heigthDevice =  $( "#dev"+ res["idDevice"] ).height();
                                    var heigthCotation =  $( "#cot" +res["idDevice"] + "-"+ res["cotation"]['listing_id']).height();
                                     
                                   /* if(i != lengthDevice -1){
                                        $( "#cot" +res["idDevice"] + "-"+ res["cotation"]['listing_id']).append( "<hr id=hrcot"+ res["idDevice"] + "-"+ res["cotation"]['listing_id']+">");        
                                    }*/
                                    if(heigthDevice > heigthCotation){
                                        //change heigth cotation
                                        $("#cot" +res["idDevice"] + "-"+ res["cotation"]['listing_id']).css("height",heigthDevice);
                                        $("#avc" +res["idDevice"] + "-" + res["cotation"]['listing_id']).css("height",heigthDevice);
                                        heigthDeter = heigthDeter + heigthDevice; 
                                       
                                        //change heigth avancement
                                     }else {
                                        //change heigth device 
                                        $("#dev"+ res["idDevice"] ).css("height",heigthCotation);
                                        $("#avc" +res["idDevice"] + "-" +res["cotation"]['listing_id']).css("height",heigthCotation);
                                        heigthDeter = heigthDeter + heigthCotation; 
                                        
                                    }   
                                }
                            });
                        }
                    } 
                });  
                //height deter 
                $("#"+ idDeter ).css("height",heigthDeter);
                //global avancement
                $( "#globalavc"+ idDeter).append( "<p id=globalavc"+idDeter + ">" + Math.round(avancement/nbrDevice) + "%</p>" ); 
                                   

            } 
        }
    }
});
</script>
<br>
<br>
<!--
<table id="tab2"  border="1" class='table display'>
    <thead>
        <tr class="bor" style="font-size: :15px;color: white;text-align: center;background-color: #970a2c;">
            
            <th >Famille</th>
            
            <th >Déterminant</th>
           
            <th >Importance</th>

            <th >
                <div class="row">
                    <div class="col-sm-6">Dispositif</div>
                    <div class="col-sm-6">Cotation</div>
                </div>
            </th>
            <th>Cotation</th>
            <th style="border-color: black">Avancement</th>
            <th style="border-color: black">Global avancement </th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $col="";
    foreach($families as $family) { $col=$col."a" ?>
            <tr id="<?=$family->family_id?>" class="<?php echo $col ?>" style="border-bottom: 2px solid black">
            <td valign="middle" style="width:15px;"><?=$family->family_name?></td>
                <td class="parent" id="deter<?=$family->family_id?>" style="width:10px;background-color: #206086;color:white">
                    <table>
                        <?php foreach($deters as $key=>$deter) {  
                            if($deter->family_id == $family->family_id){?>
                            <tr style="height: 554px;text-align: center ">
                                <td class="child" id="<?php echo($deter->determinant_id);?>" ">
                                    <?php echo($deter->determinant_name); ?>
                                </td>
                            </tr>   
                        <?php }  }?>
                    </table>
                </td>
                <td style="width:5px;">
                    <table>
                        <?php foreach($deters as $deter) {  
                            if($deter->family_id == $family->family_id){?>
                            <tr style="height: 554px;"> 
                                <td class="child" id="Impor<?php echo($deter->determinant_id);?>" >
                                    <<?php echo(($deter->determinant_importance*25)."%");?>
                                </td>
                            <tr>
                        <?php } }?>
                    </table>
                </td>
                <td style="width:100%;background-color: #206086;color:white">
                    <table>
                        <?php foreach($deters as $deter) {  
                            if($deter->family_id == $family->family_id){?>
                            <tr style="height: 554px;" id="td<?php echo($deter->determinant_id);?>">
                                <td class="child">
                                    <div class="row">
                                        <div class="col-sm-6"  id="device<?php echo($deter->determinant_id);?>"></div>
                                        <div class="col-sm-6"  id="cotation<?php echo($deter->determinant_id);?>"></div>
                                    </div>
                                </td>
                            <tr>
                        <?php } }?>
                    </table>
                </td>
                <td style="width: 5px;">
                    <table>
                        <?php foreach($deters as $deter) {  
                            if($deter->family_id == $family->family_id){?>
                            <tr style="height: 554px;">
                                <td class="child" id="avancement<?php echo($deter->determinant_id);?>">
                                    
                                </td>
                            <tr>
                        <?php } }?>
                    </table>
                </td>
                <td style="width: 5px;background-color: #206086;color:white">
                    <table>
                        <?php foreach($deters as $deter) {  
                            if($deter->family_id == $family->family_id){?>
                            <tr style="height: 554px;">
                                <td class="child" id="globalavc<?php echo($deter->determinant_id);?>">
                                    
                                </td>
                            <tr>
                        <?php } }?>
                    </table>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
-->

