<div class="row mb-5">
	<div class="col-md-3">
		<span style="color: rgb(38, 96, 133)"><b>Séléctionnner un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?> :</b></span>
		<select id="pole" name="forma" class="pole form-control">
			<option></option>
			<?php foreach ($poles as $key => $p) {
				?>
				<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-3">
		<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
		<select class="form-control" id="perimetre" name="forma" class="pole"></select>
	</div>
	<div class="col-md-2 hidden">
		<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?>:</b></span>
		<select class="form-control" id="nombre_unite" name="forma" class="pole"></select>
	</div>
	<div class="col-md-1">
		<span style="color: transparent; display: block">Charger</span>
		<button class="btn btn-primary p-2" id="showGlobalResult">Charger</button>
	</div>
	<div class="col-md-1">
		<span style="color: transparent;">Réinitialiser</span>
		<button class="btn btn-primary p-2" id="resetFilter">Réinitialiser</button>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="resultChartGlobal" id="resultChartGlobal"></div>
	</div>
</div>
