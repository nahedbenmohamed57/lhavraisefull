<div class="row mb-5">
    <div class="col-md-4">
        <select class="form-control" name="period" id="choose-period" onchange="init()">
            <option value="0">--Choisir période--</option>
            <?php foreach ($periodes as $key => $periode) { ?>
                <option value="<?=$periode->id?>" <?php if($periode->id == $active_periode_id) echo "selected";?>><?=$periode->libelle?></option>
            <?php  }?>
        </select>
    </div>
</div>

    <?php foreach ($poles as $pole) : ?>
       <div class="row">
           <p class="pole p-2" style="background: #66aa88ff;width: 100%;color: #fff; cursor: pointer" data-id="<?=$pole->id_pole?>">
			   <a data-toggle="collapse" data-target="#p<?=$pole->id_pole?>" aria-expanded="false" aria-controls="collapseExample">
				   <?=$pole->pole_nom?>
			   </a>
		   </p>
       </div>


        <div class="row collapse" id="p<?=$pole->id_pole?>">
           <?php foreach ($family_rps as $family) : ?>
               <div class="col-6">
                   <div class="resultChart" id="radarChart_<?=$family->family_id?>_<?=$pole->id_pole?>" data-pole="<?=$pole->id_pole?>" data-family-id="<?=$family->family_id?>" data-name="<?=$family->family_rps_name?>"></div>
               </div>
           <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
