<div class="row mb-5">
    <div class="col-md-3">
      <span style="color: rgb(38, 96, 133)"><b>Période</b></span>
        <select class="form-control" name="period" id="choose-period" onchange="init()">
            <option value="0">--Choisir période--</option>
            <?php foreach ($periodes as $key => $periode) { ?>
                <option value="<?=$periode->id?>" <?php if($periode->id == $active_periode_id) echo "selected";?>><?=$periode->libelle?></option>
            <?php  }?>
        </select>
    </div>
    <div class="col-md-3">
     <span style="color: rgb(38, 96, 133)"><b>Séléctionnner un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?> :</b></span>
      <select id="pole" name="forma" class="pole form-control">
        <option></option>
        <?php foreach ($poles as $key => $p) {
          ?>
          <option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
        <?php } ?>
      </select>
    </div>
    <div class="col-md-3">
     <span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
      <select class="form-control" id="perimetre" name="forma" class="pole"></select>
    </div>
    <div class="col-md-2">
      <span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?>:</b></span>
        <select class="form-control" id="nombre_unite" name="forma" class="pole"></select>
    </div>
    <div class="col-md-1">
      <span style="color: transparent;">charger</span>
      <button class="btn btn-primary p-2" id="showResultUnit">Charger</button>
    </div>
</div>
<div class="row">
   <?php foreach ($family_rps as $family) : ?>
       <div class="col-6">
           <div class="resultChartUnit" id="radarChart_<?=$family->family_id?>" data-family-id="<?=$family->family_id?>" data-name="<?=$family->family_rps_name?>"></div>
       </div>
   <?php endforeach; ?>
</div>
  
