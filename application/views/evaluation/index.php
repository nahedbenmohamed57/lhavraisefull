


<style type="text/css">
	.pole {
		width: 100%;
		text-align: center;
		height: 50%;
		border-radius: 8px;
	}
	body{
    *background: #eee;
	}
	span{
		font-size:15px;
	}
	.box{
		padding:60px 0px;
	}

	.box-part{
		border-radius:0;
		padding:60px 10px;
		margin:30px 0px;
	}
	.text{
		margin:20px 0px;
	}

	.icons-evu {
		width: 100px;
	}
	.title {
		margin-top: 32px;
		color:#5e7178;
		font-weight: 700;
	}
</style>
<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		Paramétrer la méthode d'évaluation
        </strong>
	</h2>
	
	<br>
	<br>
</div>
<?php if (!$active) : ?>
<!-- <div class="alert alert-danger">
	Il faut d'abord ajouter une nouvelle période
</div> -->
<?php endif; ?>

<!-- <div class="container-fluid">
	<h5 style="color: rgb(38, 96, 133)">Séléctionnner un Pôle :</h5>
	<select id="pole" class="form-control w-50" onchange="update(this.value)"></select>

	<br>

	<h5 style="color: rgb(38, 96, 133)">Sélectionner le périmètre de votre évaluation :</h5>
	<select id="perimetre" class="form-control w-50" onchange="window.location = this.value"></select>
	
	<br>

	<button class="btn btn-outline-dark mt-3" onclick="load()" <?php if (!$active) echo "disabled"; ?>>évaluer perimetre</button>
</div> -->
<div class="box middle">
    <div class="container">
     	<div class="row">
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">     
					  <div class="box-part text-center">           
                			<a href="<?=base_url() . 'family'?>"><img class="icons-evu" src="<?=base_url() . 'assets/images/icons/calculatorB.png'?>"></a>           
						<div class="title">
							<h4>Définir les <br>indicateurs</h4>
						</div>                  
					 </div>
				</div>
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">     
					  <div class="box-part text-center">           
              				 <a href="<?=base_url() . 'facteurs'?>"><img class="icons-evu" src="<?=base_url() . 'assets/images/icons/speedmonterB.png'?>"></a>     
						<div class="title">
							<h4>Définir les <br>facteurs de RPS</h4>
						</div>                  
					 </div>
				</div>
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">     
					  <div class="box-part text-center">           
               				<a href="/facteurs/correlation"><img class="icons-evu" src="<?=base_url() . 'assets/images/icons/analytics.png'?>"></a>       
						<div class="title">
							<h4>Définir les <br>corrélations</h4>
						</div>                  
					 </div>
				</div>
				</div>
</div>
</div>
<script>
	var poles = <?=json_encode($poles)?>

	var perimetres = <?=json_encode($perimetres)?>
	
	console.log(poles)
	console.log(perimetres)

	_currentPole = 0

	function init() {
		var perimetre = document.getElementById('pole')
		
		perimetre.options.add(new Option('-- Séléction --', ''))
		poles.map(p => {
			perimetre.options.add(new Option(p.pole_nom, p.id_pole))
		})
	}

	function update(pole) {
		clear()

		var data = perimetres.filter(p => p.pole_id == pole)
		var perimetre = document.getElementById('perimetre')

		perimetre.options.add(new Option('-- Séléction --', ''))
		data.map(p => {
			perimetre.options.add(new Option(p.perimetre_nom, `<?=base_url()?>evaluation/evaluate_family2/<?=$families[0]->family_id;?>/${pole}/${p.id_perimetre}`))
		})

		_currentPole = pole
	}

	function clear() {
		var perimetre = document.getElementById('perimetre')
		perimetre.innerHTML = ''
	}

	function load() {
        if (_currentPole) {
		    location = `<?=base_url()?>evaluation/evaluate_family2/<?=$families[0]->family_id;?>/${_currentPole}/all`
        }
	}

	init()
</script>
<div class="text-right">
<br>
<br>
</div>