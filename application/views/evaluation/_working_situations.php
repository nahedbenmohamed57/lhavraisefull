<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($working_situations)){
         echo form_open_multipart('/rps_evaluation/crud_working_situations/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/update/'.$working_situations->id_working_situations, $attributes);
    }else {
        echo form_open_multipart('/rps_evaluation/crud_working_situations/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/add', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($working_situations)){?>
                   <input type="hidden" name="id_working_situations" class="form-control" id="id_working_situations" value="<?=$working_situations->id_working_situations;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                       
                        <div class="form-group">
                            <label for="field-1" class="col-sm-12 control-label"> Risques priortaires</label>
                            <div class="col-sm-12">
                               <select class="form-control" name="id_priority_risks">
                                   <?php if(!empty($priority_risks)){ 
                                   foreach ($priority_risks as $key => $risks) {?>
                                     <option value="<?=$risks->id_priority_risks?>"
                                      <?php if(!empty($working_situations)) if($working_situations->id_priority_risks == $risks->id_priority_risks) echo "selected"?>><?=$risks->name_priority_risks?></option>
                                  <?php  }
                                    } ?>
                               </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="name_working_situations" class="form-control" id="name_working_situations" value="<?php
                                if(!empty($working_situations)) echo $working_situations->name_working_situations?>">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit">
                                    <?php  if(isset($working_situations)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>