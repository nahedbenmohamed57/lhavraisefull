<div class="container-fluid" style="margin:0px">
		<h3 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Choix du périmètre de l’évaluation</strong></h3>
		<div class="row">
           
        <div class="col-md-12" style="margin-top: 20px;">
        
			<form method="post" name="unite" id="unite">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_pole"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control w-50" name="pole_id">
                            	<option></option>
                            	<?php foreach ($poles as $key => $p) { ?>
                            		<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
                            	<?php } ?>
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_etablissement"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="perimetre" class="form-control w-50" name="establishment_id"></select>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                        	<select id="nombre_unite" name="unit_id" class="form-control w-50"> </select>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite">Date de l’évaluation</label>
                        </div>
                        <div class="col-sm-9 col-8">
                             <select class="form-control w-50" name="   evaluation_periode" style="display: none">
                                <option value="<?=$periodes->id?>" selected><?=$periodes->libelle?></option>
                            </select>
                            <input type="date" name="evaluation_date" class="form-control w-50">
                        </div>
                    </div>
					<div class="row">
						<div class="col-sm-3 col-4">
							<label for="lieux">Evaluateurs (Prénoms, noms et fonctions)</label>
						</div>
						<div class="col-sm-9 col-8">
                            <textarea type="text" class="form-control w-50" name="Evaluator" id="evaluateurs" placeholder=""></textarea>
							<small class="help-block"></small>
						</div>
					</div>
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_personne">Salariés associés à l’évaluation (Prénoms, noms et fonctions)</label>
                        </div>
                        <div class="col-sm-9  col-8">
                            <textarea type="text" class="form-control w-50" name="salaried" id="salaries" placeholder=""></textarea>
                            <small class="help-block"></small>
                        </div>
                    </div>

					<div class="row">
						<div class="col-sm-8 col-8">
							<button type="submit" class="submit btn btn-primary"> Démarrer l’évaluation des RPS</button>
						</div>
					</div>
				</div>
            </form>
        </div>
    </div>
</div>
