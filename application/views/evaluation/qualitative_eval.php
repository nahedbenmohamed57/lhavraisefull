<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		EVALUER LES FACTEURS DE RPS
        </strong><br>
         <strong style="color: rgb(38, 96, 133)">
		Evaluation qualitative
        </strong>
    </h2>

        

	<div class="row">
		
		<div class="col-md-3 pt-5">
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?> :</b></span>
			<select id="pole" name="forma" class="form-control">
				<option></option>
				<?php foreach ($poles as $key => $p) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($p->id_pole == $pole_id ) echo "selected"?>><?=$p->pole_nom?></option>
            	<?php } ?>
			</select>
		</div>

		<div class="col-md-3 pt-5">
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
			<select id="perimetre" name="forma" class="form-control">
				<?php foreach ($perimetres as $key => $pr) { ?>
            		<option value="<?=$pr->id_perimetre?>" <?php if($pr->id_perimetre == $perimetre_id ) echo "selected"?>><?=$pr->perimetre_nom?></option>
            	<?php } ?>
			</select>
		</div>
		<div class="col-md-3 pt-5">
			<span style="color: rgb(38, 96, 133)"><b>UT :</b></span>
			<select id="nombre_unite" name="nombre_unite" class="form-control">
				<?php foreach ($units as $key => $u) { ?>
            		<option value="<?=$u->id_unite?>" <?php if($u->id_unite == $unit_id ) echo "selected"?>><?=$u->unite_nom?></option>
            	<?php } ?>

			</select>
		</div>
		
		<div class="col-md-3">
			<button id="load-btn" class="btn sousMenu submit mt-3 w-100">Charger</button>
		</div>
	</div>
    <div class="row perimeter-header" style="margin-top: 20px">
    	<div class="col-md-4"><span>Risques priortaires</span><a href="#" onclick="showAjaxModal('/rps_evaluation/crud_priority_risks/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/add','<?php echo('Ajouter Risques priortaires');?>');"><i class="fas fa-plus" title="Ajouter"></i></a></div> 
    	<div class="col-md-4"><span>Situations de travail associées</span><a onclick="showAjaxModal('/rps_evaluation/crud_working_situations/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/add','<?php echo('Ajouter Situations de travail ');?>');"><i class="fas fa-plus" title="Ajouter"></i></a></div> 
    	<div class="col-md-4"><span>Pistes de travail identifiées</span><a onclick="showAjaxModal('/rps_evaluation/crud_work_areas/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/add','<?php echo('Ajouter Une Pistes de travail identifiées');?>');"><i class="fas fa-plus" title="Ajouter"></i></a></div>
    </div>

     <div class="row perimeter">  
        	<?php
        	foreach ($priority_risks as $key => $priority) {
        		?>
        		<div class="row pole">
        			<div class="col-md-4">
        				<span><?=$priority->name_priority_risks?><a href="#" onclick="showAjaxModal('/rps_evaluation/crud_priority_risks/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/update/<?=$priority->id_priority_risks?>','<?php echo('Modifer Risques priortaires');?>');"><i class="fas fa-pencil-alt" title="Ajouter"></i></a></span>
        				
        			</div>
        			<div class="col-md-8 list-etap">
        				
        				<?php if(!empty($priority->working_situations)) {
        					 foreach ($priority->working_situations as $key => $situations) { ?>
								<div class="row etab">
	        					  <div class="col-md-6 my-etab">
	        					  	<span><?=$situations->name_working_situations?></span>
	        					  	<a onclick="showAjaxModal('/rps_evaluation/crud_working_situations/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/update/<?=$situations->id_working_situations?>','<?php echo('Modifier Situations de travail ');?>');"><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
	        					  </div>
	        					  <div class="col-md-6 units">
	        					  <?php if(!empty($situations->pistes)) {
	        					  	?>
	        					  	<table>
	        					  	<?php foreach ($situations->pistes as $ke => $piste) { ?>
		        					 
		        					 	<tr><td><span><?=$piste->work_areas_name?></span>
		        					 		<a onclick="showAjaxModal('/rps_evaluation/crud_work_areas/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>/update/<?=$piste->id_work_areas?>','<?php echo('Modifier Une Pistes de travail identifiées');?>');"><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
		        					 	</td></tr>
		        					
	        					   <?php  }?>
	        					   	</table>
	        					 <?php } ?>
 								   </div>
 							    </div>
        					 
        					  <?php } 
        				 }?>
        		
        			</div>
        		</div>
        	
        	<?php }
        	?>
    </div>
	<br>
	<div class="row">
		<div class="col-md-3">
			<button class="btn submit btn btn-primary">Sauvegarder</button>
		</div>
		<div class="col-md-3">
			<button class="btn submit mr-1">Retour vers l'évaluation quantitative</button>
			<span style="color:#3e91b2ff;">Retour</span>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#load-btn').on('click', function() {
		var pole = $('#pole').val();
		var perimetre = $('#perimetre').val();
		var nombre_unite = $('#nombre_unite').val();
		if(pole != null && perimetre != null && nombre_unite != null) {
			console.log('redirection');
			window.location = '/rps_evaluation/qualitative_evaluation/'+pole+'/'+perimetre+'/'+nombre_unite;

		} else {
			console.log('il y des champs qui manque')
		}
	})
</script>
