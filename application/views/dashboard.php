<div class="container-fluid">

	<h1 class="text-primary text-center"> Bienvenue  <?=$user->user_firstname.' '.$user->user_lastname?></h1>
	<p class="text-center"> <span>Votre statut :  <?=$user->user_role->role_name?></span></p>
	<?php if($user->user_role->role_val == 2 ||  $user->user_role->role_val == 3 ) {?>
	<div class="row">
		<!-- Earnings (Monthly) Card Example -->
		<div class="col-xl-12 col-md-12 mb-4">
			<div class="card shadow h-100 py-2">
				<div class="card-body pt-0">
					<div class="col-auto text-center"> <i class="fas fa-calendar-alt fa-2x text-gray-300"></i></div>
					<?= $config->msg_user?>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
	<div class="row">
			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-primary  shadow h-100 py-2">
					<div class="card-body pt-0">
						<div class="col-auto text-center"> <i class="fas fa-calendar-alt fa-2x text-gray-300"></i></div>
						<div class="row no-gutters align-items-center">
							<div class="col mr-2 text-center">
								<div class="text-xs font-weight-bold text-primary  text-uppercase mt-3">Période en cours :
								</div>
								<?php if ($activePeriod) { ?>
									<div class="text-xs font-weight-bold text-primary  text-uppercase mt-1"><?=$activePeriod->libelle?></div>
								<?php	} ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body  pt-0">
						<div class="col-auto text-center"> <i class="fas fa-map-marker-alt fa-2x text-gray-300"></i> </div>
						<div class="row no-gutters align-items-center">
							<div class="col mr-2 text-center">
								<div class="text-xs font-weight-bold text-info text-uppercase mt-3">
									<span class="d-block text-primary "><?=$nbPole?> <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôles"?>s</span>
									<span class="d-block text-primary "><?=$nbPerimetre?> <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "établissement"?></span>
									<span class="d-block text-primary "><?=$nbUnit?> <?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="row no-gutters align-items-center">

									<div class="col">
										<div class="progress progress-sm mr-2">
											<div class="progress-bar bg-info" role="progressbar" style="width: <?=$result?>%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
									<div class="col-auto">
										<div class="h5 mb-0 mr-3 text-primary"><?=$result?>%</div>
									</div></div>
								<div class="text-xs font-weight-bold text-primary  text-uppercase mb-1 mt-3"><?=$nbEval?> évaluations sur <?=$nbUnit?> complétées</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php if($user->user_role->role_val == 1 || $user->user_role->role_val == 2) {?>
		<div class="row mb-4">
			<div class="col-xl-6 col-lg-6">
				<div class="card shadow h-100 mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Envoyer un mail aux admin</h6>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<form method="post" action="dashboard/sendMail">
							<div class="form-group">
								<label>Sujet</label>

								<input type="text" name="subject" class="form-control" value="" required>
								<input type="hidden" name="role" value="<?=$user->user_role->role_val?>">
							</div>
							<div class="form-group">
								<label>Contenu</label>
								<textarea name="content" class="form-control" cols="100"> </textarea>
							</div>
							<input type="submit" class="btn btn-primary mt-3 float-right" name="" value="Envoyer" required>
						</form>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6">
				<div class="card shadow h-100 mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Afficher une actu aux admin</h6>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<h5>Message affiché </h5>
						<p> <?=$config->msg_user?></p>
						<div class="form-group">
							<label>Nouveau message </label>
							<textarea id="quoi-neuf" class="form-control" cols="100"> </textarea>
							<button class="btn btn-primary mt-3 float-right" id="save-news" name="">Valider </button>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php }?>
	<?php if($user->user_role->role_val == 1){ ?>
		<div class="row mb-4">
			<div class="col-xl-6 col-lg-6">
				<div class="card shadow h-100 mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Changer le logo client</h6>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="col-12 text-center">
							<img src="/template/img/<?=$config->image_platforme?>" width="100px">
						</div>
						<div class="form-group">
							<form action="dashboard/updatePhoto" method="post" enctype="multipart/form-data">
								<input type="file" name="fileToUpload" id="fileToUpload">
								<input type="submit" value="Modifier Image" name="submit" class="btn btn-primary mt-3 float-right">
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6">
				<div class="card shadow h-100 mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Nom des périmètres</h6>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<form method="post" action="dashboard/updateConfig">
							<div class="form-group">
								<label>Nom de plateforme</label>
								<input type="text" name="name_platforme" class="form-control" value="<?=$config->name_platforme?>" required>
							</div>
							<div class="form-group">
								<label>Nom de <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?></label>
								<input type="text" name="pole_name" class="form-control" value="<?=$config->pole_name?>" required>
							</div>
							<div class="form-group">
								<label>Nom de l'<?php if(isset($_SESSION['config'])) echo $_SESSION['config']->etab_name; else echo "etablissement"?></label>
								<input type="text" name="etab_name" class="form-control" value="<?=$config->etab_name?>" required>
							</div>
							<div class="form-group">
								<label>Nom de l'unité</label>
								<input type="text" name="unit_name" class="form-control" value="<?=$config->unit_name?>" required>
							</div>
							<input type="submit" class="btn btn-primary mt-3 float-right" name="submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
