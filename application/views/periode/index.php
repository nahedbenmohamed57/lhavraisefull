<div class="container-fluid" style="margin:0px">
	<div class="row mb-4">
		<div class="col-xl-4 col-lg-4">

			<div class="card shadow h-100 mb-4" ALIGN="CENTER">
				<!-- Card Header - Dropdown -->
				<span class="text-center mt-4"><i class="fas fa-calendar-alt" style="font-size:40px"></i></span>
				<div class="card-header py-3" ALIGN="CENTER">
					<strong style="color: rgb(38, 96, 133);">Période en cours : <br> </strong>

				</div>
				<div class="card-header py-3" ALIGN="CENTER">
					<strong style="color: rgb(38, 96, 133);"> <?= $active ? $active : 'Aucune' ?><br>&emsp; </strong>
				</div>
				<div class="card-header py-3" ALIGN="CENTER">
					<button id="fill-indicator-btn" class="btn sousMenu submit mt-3 w-100 btn btn-primary p-2 ml-1" onclick=location.href="/periode/cloturer">Clôturer</button>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-lg-4" >

			<div class="card shadow h-100 mb-4" ALIGN="CENTER" >
				<!-- Card Header - Dropdown -->
				<span class="text-center mt-4"><i class="fas fa-hourglass-end" style="font-size:40px"></i></span>
				<div class="card-header py-3" ALIGN="CENTER">
					<strong style="color: rgb(38, 96, 133);">Historique des périodes: <br> </strong>
				</div>
				<div class="card-header py-3" ALIGN="CENTER"   style= "max-height: 260px;
					 overflow: auto;">

					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Période</th>
							<th>Clôturer</th>
							<th></th>
						</tr>
						</thead>

						<tbody>
						<?php foreach ($periodes as $periode) : ?>
							<tr>
								<td> <?=$periode->libelle?></td>

								<td>
									<?=$periode->active ? '<span class="badge badge-danger">NON</span>' : '<span class="badge badge-success">OUI</span>'?>
								</td>
								<td>
									<a href="<?php echo base_url();?>Periode/suprimer/<?=$periode->id;?>">
										<p><i class="fas fa-trash-alt " title="Supprimer"></i></p>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<div class="col-xl-4 col-lg-4" >

			<div class="card shadow h-100 mb-4">
				<!-- Card Header - Dropdown -->
				<span class="text-center mt-4"><i class="fas fa-plus" style="font-size:40px"></i></span>
				<div class="card-header py-3" ALIGN="CENTER">
					<strong style="color: rgb(38, 96, 133);">Nouvelle période</strong>

					<?php if ($active) : ?>
						<div class="alert alert-danger">
							Il faut d'abord clôturer la dérniere période "<?=$active?>"
						</div>
					<?php endif; ?>
					<form action = "<?php echo base_url() ?>periode/nouvelle"
						  name="addperiode" method="post" class="form-group" id="addperiode">
						<div class="card-header py-3" >
							<label for="libelle">Nom de la période</label>
							<input type="text" class="form-control" name="libelle" value="" id="libelle" placeholder="">
						</div>


						<button type="submit" class="btn sousMenu submit mt-3 w-100 btn btn-primary p-2 ml-1" <?php if ($active) echo "disabled"; ?>>
							Ajouter
						</button>


					</form>

				</div>

			</div>
		</div>


	</div>

</div>
