<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($facteur)){
        echo form_open_multipart('facteurs/facteur/update/false/', $attributes);
    }else {
        echo form_open_multipart('facteurs/facteur/add/', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($facteur)){?>
                   <input type="hidden" name="facteur_id" class="form-control" id="facteur_id" value="<?=$facteur->facteur_id;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="enregpatient" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                             <label for="field-1" class="col-sm-3 control-label">Famille</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                               <select name="family_rps_id" class="form-control">
                                   <option></option>
                                   <?php foreach ($families as $key => $familie): ?>
                                       <option value="<?=$familie->family_id?>"
                                        <?php if(isset($facteur)) { if($familie->family_id == $facteur->family_rps_id) echo "selected";}?>> <?=$familie->family_rps_name?></option>
                                   <?php endforeach ?>
                               </select>
                            </div>
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="facteur_name" class="form-control" id="facteur_name" value="<?php if(isset($facteur)){ echo($facteur->facteur_name);} ?>">
                            </div>
                            <label for="field-1" class="col-sm-3 control-label">Définition</label>
                            <div class="col-sm-12" style="margin-bottom: 20px; height: 150px">
                            <textarea class="form-control" rows="3" id="family_description" name ="family_description" value="<?php if(isset($facteur)){ echo($facteur->family_description);} ?>">
                                <?php if(isset($facteur)) {
                                      if(!empty($facteur->family_description))
                                      echo $facteur->family_description;
                                      else echo "   Définition...";
                                   } else {
                                    echo "   Définition...";
                                   }?>
                         
                        </textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit btn btn-primary">
                                    <?php  if(isset($facteur)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
