
<div class="container-fluid">
	  <div class="row" >
	  	<table class="table perimeter-header">
	  		<thead>
	  			<tr>
	  				<td width="20%" class="custom-tr">Familles</td>
	  				<td width="5%"  class="custom-tr"><a onclick="showAjaxModal('<?php echo base_url();?>family_rps/crud_family_rps/add','<?php echo($titleAddFamilyRps);?>');"><i class="fas fa-plus" title="Ajouter" style="color:#fff"></i></a></td>
	  				<td width="70%"  class="custom-tr">Indicateurs</td>
	  				<td width="5%"  class="custom-tr" style="border-color: #858796"><a onclick="showAjaxModal('<?php echo base_url();?>facteurs/facteur/add/','<?php echo($titleindic);?>');" ><i class="fas fa-plus" title="Ajouter" style="color:#fff"></i></a></td>
	  			</tr>
	  		</thead>
	  	</table>

	  	<?php foreach($families_rps as $family){ ?>
	  		<table class="table table-bordered table-family">
	  		<tbody>
	  			<tr>
	  				<td width="20%">
	  					<span><?=$family->family_rps_name;?></span></td>
	  				<td width="5%"><a onclick="showAjaxModal('<?php echo base_url();?>family_rps/crud_family_rps/update/<?=$family->family_id;?>','<?php echo($titleUpdateFamily);?>');" ><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
	  				<a href="#" data-href="<?php echo base_url();?>family_rps/crud_family_rps/delete/<?=$family->family_id;?>" data-toggle="modal" data-target="#modal_delete">
							<i class="fas fa-trash" title="Supprimer"></i>
						</a></td>
	  				<td width="75%">
	  					<?php foreach($facteurs as $facteur){ 
							if($facteur->family_rps_id == $family->family_id){ ?>
								
								<span><?=$facteur->facteur_name?></span>
								<a href="#" data-href="<?php echo base_url();?>facteurs/facteur/delete/false/<?=$facteur->facteur_id;?>" data-toggle="modal" data-target="#modal_delete">
											<i class="fas fa-trash fa-deter" title="Supprimer"></i>
								</a>

								<a onclick="showAjaxModal('<?php echo base_url();?>facteurs/facteur/update/false/<?=$facteur->facteur_id;?>','<?php echo($titleUpdatfacteur);?>');">
									<i class="fas fa-pencil-alt" title="Modifier"></i>
								</a>

								
								<hr>
							<?php }
						} ?>
	  				</td>
	  				
	  			</tr>
	  		</tbody>
	  	</table>
	  	<?php }?>
	  </div>
</div>



