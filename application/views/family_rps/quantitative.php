<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		EVALUER LES FACTEURS DE RPS
        </strong><br>
         <strong style="color: rgb(38, 96, 133)">
		Evaluation quantitative
        </strong>
    </h2>
	<div class="row">
		
		<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?> :</b></span>
			<br>
			<select id="pole" name="forma" class="form-control">
				<option></option>
				<?php foreach ($poles as $key => $p) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($p->id_pole == $pole_id ) echo "selected"?>><?=$p->pole_nom?></option>
            	<?php } ?>
			</select>
		</div>

		<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
			<br>
			<select id="perimetre" name="perimetre" class="form-control">
				<?php foreach ($perimetres as $key => $pr) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($pr->id_perimetre == $perimetre_id ) echo "selected"?>><?=$pr->perimetre_nom?></option>
            	<?php } ?>

			</select>
		</div>
        	<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>UT:</b></span>
			<br>
			<select id="nombre_unite" name="nombre_unite" class="form-control">
				<?php foreach ($units as $key => $u) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($u->id_unite == $unit_id ) echo "selected"?>><?=$u->unite_nom?></option>
            	<?php } ?>

			</select>
		</div>
		
		<div class="col-md-3 pt-5 mt-2">
			<br>
			<button id="load-btn" class="btn sousMenu submit mt-3 w-100 btn btn-primary p-2" >Charger les résultats</button>
		</div>
	</div>

	<div id="load-table" class='mt-5'></div>
	<div class="row">
		<form name="form" role="form"  class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data" style="width: 100%">
			<?php foreach($families_rps as $family){ ?>
				<div class="bs-calltoaction bs-calltoaction-primary">
					<div class="card-full shadow mb-4">
						<!-- Card Header - Accordion -->
						<a href="#collapse-<?=$family->family_id?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapse-<?=$family->family_id?>">
							<h6 class="m-0 font-weight-bold text-primary"><?=$family->family_rps_name;?></h6>
						</a>
						<!-- Card Content - Collapse -->
						<div class="collapse" id="collapse-<?=$family->family_id?>" style="">
							<div class="card-body">
								<table class="table-bordered" width="100%" style="margin-bottom: 58px">
									<thead>
									<tr>
										<th>Facteur</th>
										<th >Description</th>
										<th>Eposition (EX)</th>
										<th>Impact</th>
										<th>Niveau de risque</th>
										<th>Mesures de prévention existantes</th>
										<th>Mesures de prévention recommandées</th>
									</tr>
									</thead>
									<?php $i= 1;
									foreach($facteurs as $facteur){
										if($facteur->family_rps_id == $family->family_id){ $i++;}
									}?>
									<tr>
										<?php $i= 0;
										foreach($facteurs as $facteur){
										if($facteur->family_rps_id == $family->family_id){
										$current_elemnt = [];
										foreach ($evaluation_qantitive as $key => $eval) {
											if($eval->facteur_id == $facteur->facteur_id) {
												$current_elemnt = $eval;
											}
										}?>
									<tr>
										<th colspan=""><?=$facteur->facteur_name?></th>
										<td>
											<textarea rows="3" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_description" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->description?>" class="form-control p-0 mt-2 mb-2"> <?php if(!empty($current_elemnt)) echo $current_elemnt->description?></textarea>
										</td>
										<td>
											<select class="form-control intensity" id ="intensity_<?=$family->family_id?>_<?=$facteur->facteur_id?>" data-info="<?=$family->family_id?>_<?=$facteur->facteur_id?>" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_intensity">
												<option value="1" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 1) echo "selected"?>>1</option>
												<option value="2" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 2) echo "selected"?>>2</option>
												<option value="3" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 3) echo "selected"?>>3</option>
												<option value="4" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 4) echo "selected"?>>4</option>
												<option value="5" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 5) echo "selected"?>>5</option>
											</select>
										</td>

										<td id="impact_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center"><?=round($facteur->impact,1)?></td>
										<?php $color_fiabilite = '#fff';
										$colorText= '#000';
										if($facteur->fiabilite > 0 && $facteur->fiabilite < 2) {
											$color_fiabilite = '#66aa88';
										}else if($facteur->fiabilite >= 2 && $facteur->fiabilite < 3) {
											$color_fiabilite = '#ffc000';
										}
										else if($facteur->fiabilite>= 3 && $facteur->fiabilite < 4) {
											$color_fiabilite = '#f79646';
										}
										else if($facteur->fiabilite >= 4 && $facteur->fiabilite < 5) {
											$color_fiabilite = '#c0504d';
										}else if($facteur->fiabilite >= 5) {
											$color_fiabilite = '#000000';
											$colorText = '#fff';
										}?>

										<td id="NvRisq_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center">0</td>
									<td>
										<textarea rows="3" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_measures_concerned" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->measures_concerned?>" class="form-control p-0 mt-2 mb-2"> <?php if(!empty($current_elemnt)) echo $current_elemnt->measures_concerned?></textarea>
									</td>
									<td style="display: flex">
										<textarea rows="3" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_measures_recommended" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->measures_recommended?>" class="form-control p-0 mt-2 mb-2"> <?php if(!empty($current_elemnt)) echo $current_elemnt->measures_recommended?></textarea>
										<input type="hidden" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_contributors" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->contributors?>">
										<input type="hidden" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_steps" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->steps?>">
										<input type="hidden" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_human_resources" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->human_resources?>">
										<input type="hidden" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_indicator" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->indicator?>">
										<a href="" data-toggle="modal" data-target="#edit-description" class="edit-action" data-family="<?=$family->family_id?>" data-factor="<?=$facteur->facteur_id?>"
										data-rps_evaluation_id="<?php 	if(!empty($current_elemnt)) echo $current_elemnt->rps_evaluation_id ?>"><i class="fas fa-info-circle"></i></a>
									</td>
								</tr>
								<?php }
								}?>

									</tr>

								</table>

							</div>
						</div>
					</div>
				</div>

	  	<?php }
	  	$rps_evaluation_id = null;
	  	if(!empty($current_elemnt)) $rps_evaluation_id = $current_elemnt->rps_evaluation_id ?>
	  	<div class="form-group" style="margin-top: 20px; float: right">
            <button type="submit" class="submit btn btn-primary">
                Sauvegarder
            </button>
           
        </div>
	 </form>

	 </div>
<br>
</div>
<div class="modal fade" id="edit-description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				Ajouter déscription de mesure
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Pilotes et contributeurs</label>
						<input type="text" id="contributors" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label>Etapes</label>
						<textarea id="steps" class="form-control"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label>Moyens humains, techniques et financiers</label>
						<textarea id="human_resources" class="form-control"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label>Indicateurs de suivi</label>
						<textarea id="indicator" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<a class="btn btn-primary p-2 btn-add-description">Enregistrer</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('[id^=intensity]').on('change',function() {
		var intensity = $(this).val();
		var info      = $(this).data('info');
		var ex = parseInt(intensity);
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();
		var nvRisq = (ex+parseFloat(impact))/2;
		console.log(nvRisq);
		$('#NvRisq_'+info).text(Math.round(nvRisq,1));
	})

	$( ".intensity" ).each(function( index ) {
		var intensity = $(this).val();
		var info      = $(this).data('info');
		var ex = parseInt(intensity);

		var color = '#fff';
		if(ex > 0 && ex < 2) {
			color = '#66aa88';
		}else if(ex >= 2 && ex < 3) {
			color = '#ffc000';
		}
		else if(ex >= 3 && ex < 4) {
			color = '#f79646';
		}
		else if(ex >= 4 && ex < 5) {
			color = '#c0504d';
		}else if(ex >= 5) {
			color = '#000000';
		}
		$('#ex_'+info).css('background', color);
		if(color == "#000000")$('#ex_'+info).css('color', '#fff');
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();

		var color_impact = '#fff';
		if(impact > 0 && impact < 2) {
			color_impact = '#66aa88';
		}else if(impact >= 2 && impact < 3) {
			color_impact = '#ffc000';
		}
		else if(impact >= 3 && impact < 4) {
			color_impact = '#f79646';
		}
		else if(impact >= 4 && impact < 5) {
			color = '#c0504d';
		}else if(impact >= 5) {
			color_impact = '#000000';
		}

		$('#impact_'+info).css('background', color_impact);

		var nvRisq = (ex+parseFloat(impact))/2;

		var color_NvRisq = '#fff';
		if(nvRisq > 0 && nvRisq < 2) {
			color_NvRisq = '#66aa88';
		}else if(nvRisq >= 2 && nvRisq < 3) {
			color_NvRisq = '#ffc000';
		}
		else if(nvRisq >= 3 && nvRisq < 4) {
			color_NvRisq = '#f79646';
		}
		else if(nvRisq >= 4 && nvRisq < 5) {
			color = '#c0504d';
		}else if(nvRisq >= 5) {
			color_NvRisq = '#000000';
		}
		$('#NvRisq_'+info).css('background', color_NvRisq);
		if(color_NvRisq == '#00000')	$('#NvRisq_'+info).css('color', '#fff');
		$('#NvRisq_'+info).text(Math.round(nvRisq,1));

	});

	$('.edit-action').on('click',function() {
		var family = $(this).attr('data-family');
		var factor = $(this).attr('data-factor');

		//set data on the modal
		console.log($('[name ="'+family+'_'+factor+'_contributors"]').val())
		$('#edit-description #contributors').val($('[name ="'+family+'_'+factor+'_contributors"]').val());
		$('#edit-description #steps').val($('[name ="'+family+'_'+factor+'_steps"]').val());
		$('#edit-description #human_resources').val($('[name ="'+family+'_'+factor+'_human_resources"]').val());
		$('#edit-description #indicator').val($('[name ="'+family+'_'+factor+'_indicator"]').val());

		let rps_evaluation_id = $(this).attr('data-rps_evaluation_id');
		$('#edit-description .btn-add-description').attr('data-family', family);
		$('#edit-description .btn-add-description').attr('data-factor', factor);
		$('#edit-description .btn-add-description').attr('data-rps_evaluation_id', rps_evaluation_id);
	});
	$('.btn-add-description').on('click',function(e) {
		e.preventDefault();
		var family = $('.btn-add-description').attr('data-family');
		let factor = $('.btn-add-description').attr('data-factor');

		let contributors =$('#edit-description #contributors').val();
		let steps =$('#edit-description #steps').val();
		let human_resources =$('#edit-description #human_resources').val();
		let indicator =$('#edit-description #indicator').val();
		let rps_evaluation_id =  $('.btn-add-description').attr('data-rps_evaluation_id');
		//set filed by new data given on the modal
		$('[name ="'+family+'_'+factor+'_contributors"]').val(contributors);
		$('[name ="'+family+'_'+factor+'_steps"]').val(steps);
		$('[name ="'+family+'_'+factor+'_human_resources"]').val(human_resources);
		$('[name ="'+family+'_'+factor+'_indicator"]').val(indicator);
		$('#edit-description').removeData();
		$('#edit-description').hide();
		$('.modal-backdrop').hide();
		$('#page-top').removeClass('modal-open');
	});
</script>
