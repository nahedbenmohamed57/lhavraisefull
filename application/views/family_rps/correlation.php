
<div class="container-fluid">
<div class="row">

	<form name="form" role="form"  class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data" style="overflow: auto;">
		<table class="table table-bordered tab-correlation">
			<thead>
				<tr class="top-ligne">
					<td></td>
					<?php foreach($facteurs as $facteur){ ?>
						<td><span><?=$facteur->facteur_name;?></span></td>
					<?php } ?>
				
				</tr>
				<?php foreach ($determinant as $key => $det) {
					$correlation = [];

					foreach ($det->corrolation as $k => $cor) {
						
						if($cor->value == 1) {
							array_push($correlation, $cor->id_facteur);
						}
					}
					
					?>
					<tr class="content-info">
						<td><span><?=$det->determinant_name;?></span></td>
						<?php foreach($facteurs as $facteur){ 
							?>
						<td  class="<?php if(in_array($facteur->facteur_id, $correlation)) echo 'active_checkbox'?>" style="text-align: center" for="<?=$det->determinant_id?>[]">
							<input type="checkbox" name="<?=$det->determinant_id?>[]" value="<?=$facteur->facteur_id?>" <?php if(in_array($facteur->facteur_id, $correlation)) echo "checked"; ?>></td>
					<?php }?>
					</tr>
				<?php } ?>
				
			</thead>
		</table>
		<div class="form-group" style="margin-top: 20px">
            <button type="submit" class="submit btn btn-primary">
                Terminer et Sauvegarder
            </button>
        </div>
	</form>
	
</div>
</div>

<style type="text/css">
	.active_checkbox {
		background: #4a87a6;
	}
	.tab-correlation tr.content-info:nth-child(2n+1) {
		background: #2660858f;
	}
</style>

