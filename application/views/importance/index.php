<style type="text/css">
	.pole {
		width: 100%;
		text-align: center;
		height: 50%;
		border-radius: 8px;
	}
</style>

<div class="container-fluid">
	<h5 style="color: rgb(38, 96, 133)">Séléctionnner un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?> :</h5>
	<select id="pole" class="form-control w-50" onchange="_currentPole = this.value"></select>

	<br>

	<button class="btn btn-outline-dark mt-3" onclick="load()">Evaluer le <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?></button>
</div>

<script>
	var poles = <?=json_encode($poles)?>

    var _currentPole = null

	function init() {
		var perimetre = document.getElementById('pole')
		
		perimetre.options.add(new Option('-- Séléction --', ''))
		poles.map(p => {
			perimetre.options.add(new Option(p.pole_nom, p.id_pole))
		})
	}

	function load() {
        if (_currentPole) {
		    location = `<?=base_url()?>importance/config/${_currentPole}`
        }
	}

	init()
</script>
<div class="text-right">

</div>
<br><br>
<div class="col-md-3 offset-7">
<br>
<br>
<br>
</div>
