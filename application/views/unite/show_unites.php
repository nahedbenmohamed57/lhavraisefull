
<div class="container-fluid" style="margin:0px">
        <a href="/pole" class="btn sousMenu submit mr-2" style=''>
            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
            <span style="color:#3e91b2ff;">Retour</span>
        </a>
            
        <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Définir les unités de travail</strong></h2>
        <h3 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Modifier l'<?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "unité de travail"?></strong></h3>
        <div class="row">
           
        <div class="col-md-12" style="margin-top: 20px;">

            <form method="post" name="unite" id="unite">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_pole"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control w-50" name="pole_id">
                                <option></option>
                                <?php if(!empty($pole)) {
                                    foreach ($pole as $key => $p) {
                                        ?>
                                        <option value="<?=$p->id_pole?>" <?php if($p->id_pole == $unites->pole_id) echo "selected"?>><?=$p->pole_nom?></option>
                                        <?php 
                                    }
                                }?>
                                
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_etablissement"><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="perimetre" class="form-control w-50" name="perimetre_id">
                                <?php foreach ($perimetres as $key => $p) {?>
                                   <option value="<?=$p->id_perimetre?>" <?php if($p->id_perimetre == $unites->perimetre_id) echo "selected" ?>><?=$p->perimetre_nom?></option>
                               <?php }?>
                                
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="unite_nom">Nom de l'<?php if(isset($_SESSION['config'])) echo $_SESSION['config']->unit_name; else echo "unité de travail"?></label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="unite_nom" id="unite_nom" placeholder="" value="<?php if($unites->unite_nom) echo $unites->unite_nom?>">
                            <small class="help-block"></small>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="lieux">Lieux de travail concernées</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="lieux" id="lieux"placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_personne">Nombre de personne physique concernées</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="nombre_personne" id="nombre_personne" placeholder="">
                            <small class="help-block"></small>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="fonction">Fonction des personnes concernées</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="fonction" id="fonction" placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="commenatires">Commentaires/remarques</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <textarea type="text" class="form-control" name="commantaire" id="commenatires" placeholder=""></textarea>
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="submit btn btn-primary"> Termier et sauvegarder</button>
                    </div>
            </form>
        </div>
    </div>
</div>
