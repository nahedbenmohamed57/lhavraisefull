<?php   
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($listing)){ 
        echo form_open_multipart("family/crud_listing/update/$listing->listing_id", $attributes); 
    }else {
        echo form_open_multipart('family/crud_listing/add/', $attributes); 
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-body"> 
                    <form name="form" role="form" id="" class="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="hidden" name="listing_id" class="form-control" id="listing_id" 
                            value="<?php if(isset($listing)) { echo($listing->listing_id); } ?>">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 15px;">
                                <input type="text" name="listing_name" class="form-control" id="listing_name"
                                value="<?php if(isset($listing)) { echo($listing->listing_name); } ?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Score</label>
                            <div class="col-sm-12" style="margin-bottom: 15px;">
                                <input type="number" min ="0" name="listing_score" class="form-control" id="listing_score"
                                value="<?php if(isset($listing)) { echo($listing->listing_score); } ?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="submit">
                            <?php if(isset($listing)){ 
                                echo ("Modifier"); 
                            }else {
                                echo ("Ajouter"); 
                            }?>
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>