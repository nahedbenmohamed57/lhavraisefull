<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($family)){
        echo form_open_multipart('family/crud_family/update/<?=$family->family_id;?>', $attributes);
    }else {
        echo form_open_multipart('family/crud_family/add/', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($family)){?>
                   <input type="hidden" name="family_id" class="form-control" id="family_id" value="<?=$family->family_id;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="family_name" class="form-control" id="family_name" value="<?php if(isset($family)){ echo($family->family_name);} ?>">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit btn btn-primary">
                                    <?php  if(isset($family)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
