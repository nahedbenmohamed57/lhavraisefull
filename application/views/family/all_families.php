
<div class="container-fluid" >
	  <div class="row">
	  	<table class="table perimeter-header">
	  		<thead>
	  			<tr>
	  				<td width="20%" class="custom-tr">Familles</td>
	  				<td width="5%" class="custom-tr"><a onclick="showAjaxModal('<?php echo base_url();?>family/crud_family/add','<?php echo($titleAddFamily);?>');"><i class="fas fa-plus" title="Ajouter" style="color: #fff"></i></a></td>
	  				<td width="70%" class="custom-tr">Indicateurs</td>
	  				<td width="5%" class="custom-tr" style="border-color:#266085 "><a onclick="showAjaxModal('<?php echo base_url();?>family/determinant/add/','<?php echo($titledeter);?>');"><i class="fas fa-plus" title="Ajouter" style="color: #fff"></i></a></td>
	  			</tr>
	  		</thead>
	  	</table>

	  	<?php foreach($families as $family){ ?>
	  		<table class="table table-bordered table-family">
	  		<tbody>
	  			<tr>
	  				<td width="20%">
	  					<span><?=$family->family_name;?></span></td>
	  				<td width="5%"><a onclick="showAjaxModal('<?php echo base_url();?>family/crud_family/update/<?=$family->family_id;?>','<?php echo($titleUpdateFamily);?>');" ><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
	  				<a href="#" data-href="<?php echo base_url();?>family/crud_family/delete/<?=$family->family_id;?>" data-toggle="modal" data-target="#modal_delete">
							<i class="fas fa-trash" title="Supprimer"></i>
						</a></td>
	  				<td width="75%">
	  					<?php foreach($determinants as $determinant){ 
							if($determinant->family_id == $family->family_id){ 
								$status = ($determinant->family_nature == 'positif') ? "green" : "red";
								?>
								<span><?=$determinant->determinant_name?><small style="color:<?=$status?>"> (<?=$determinant->family_nature?>)</small></span>
								<a href="#" data-href="<?php echo base_url();?>family/determinant/delete/false/<?=$determinant->determinant_id;?>" data-toggle="modal" data-target="#modal_delete">
											<i class="fas fa-trash fa-deter" title="Supprimer"></i>
										</a>

								<a onclick="showAjaxModal('<?php echo base_url();?>family/determinant/update/false/<?=$determinant->determinant_id;?>','<?php echo($titleUpdatDeterminant);?>');"><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
								<hr>
							<?php }
						} ?>
	  				</td>
	  				
	  			</tr>
	  		</tbody>
	  	</table>
	  	<?php }?>

	  </div>
</div>

