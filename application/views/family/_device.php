<?php   
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($device)){
        echo form_open_multipart('family/device/update/<?=$determinant_id;?>/<?=$device->device_id;?>', $attributes); 
    }else {
        echo form_open_multipart('family/device/add/<?=$determinant_id;?>', $attributes); 
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                </div>
                <input type="hidden" name="determinant_id" class="form-control" id="determinant_id" value="<?=$determinant_id;?>">
                <input type="hidden" name="device_id" class="form-control" id="determinant_id"  value="<?php if (isset($device)){ echo($device->device_id); } ?>">
                <div class="panel-body"> 
                    <form name="form" role="form" id="enregpatient" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="device_name" class="form-control" id="device_name"  value="<?php if (isset($device)){ echo($device->device_name); } ?>">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit">
                                    <?php if(isset($device)) { 
                                        echo("Modifier");
                                    }else {
                                        echo("Ajouter") ;
                                    }?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>