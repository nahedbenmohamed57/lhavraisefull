<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($determinant)){
        echo form_open_multipart('family/determinant/update/false/<?=$determinant->determinant_id;?>', $attributes);
    }else {
        echo form_open_multipart('family/determinant/add/<?=$family_id;?>', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($determinant)){?>
                   <input type="hidden" name="determinant_id" class="form-control" id="determinant_id" value="<?=$determinant->determinant_id;?>">
                <?php } else { ?>
                    <input type="hidden" name="family_id" class="form-control" id="family_id" value="<?=$family_id;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="enregpatient" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group">

                            <label for="field-1" class="col-sm-3 control-label">Famille</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <select class="form-control" name="family_id">
                                    <option></option>
                                        <?php foreach($families as $family){ ?>
                                           <option value="<?=$family->family_id;?>" <?php if(isset($determinant) && $determinant->family_id == $family->family_id) echo "selected"?>><?=$family->family_name;?></option>
                                        <?php }?>
                                </select>
                            </div>


                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="determinant_name" class="form-control" id="determinant_name" value="<?php if(isset($determinant)){ echo($determinant->determinant_name);} ?>">
                            </div>
                            <label for="field-1" class="col-sm-3 control-label">Nature</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                             <select name="family_nature" class="form-control" id="family_nature" value="<?=$family_nature;?>">
                                        <option value="">Choisir le rôle</option>
                                        <option value="positif" <?php if(isset($determinant) && $determinant->family_nature == "positif") echo "selected"?>>Positif</option>
                                        <option value="negatif" <?php if(isset($determinant) && $determinant->family_nature == "negatif") echo "selected"?>>Négatif</option>
                            </select>
                            </div>

							<label for="Description" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
								<textarea rows="5" name="description_indicateur" class="form-control" value="<?php if(isset($determinant)){ echo($determinant->description_indicateur);} ?>"><?php if(isset($determinant)){ echo($determinant->description_indicateur);} ?></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="submit btn btn-primary">
                                    <?php  if(isset($determinant)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
