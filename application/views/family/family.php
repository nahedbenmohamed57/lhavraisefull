<div class="container-fluid">
    <?php   
		$attributes = array('class' => '', 'id' => 'user_form');
		echo form_open_multipart('family/crud_family/add', $attributes); 
	?>
    <div class="row">
	 <h2><?php 	if($task== "update") 
					{echo('Modifier');}
				else if($task== "show") 
					{echo("Détailles d'");}
				else 
					{echo('Ajouter');}?> une famille</h2>
         <div class="col-md-12">
         </div>
        <div class="col-md-6">
            <form method="post"  >
                <div class="form-group" >
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="family_name">Nom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="family_name" value="<?php if(isset($user)) {echo($user->user_firstname);} ?>"  id="name_user" placeholder="3Click"
							<?php if($task == "show"){ echo("disabled"); } ?>>
                        </div>
                    </div>
                </div>
				<?php if($task != "show"){ ?>
					<div class="form-group">
						<button type="submit" class="submit btn btn-primary" >
							<div class="glyphicon glyphicon-plus"></div>
							<?php if(isset($user)){ echo('Modifier'); } else { echo('Ajouter'); }?>
						</button>
					</div>
				<?php } ?>
            </form>
        </div>
    </div>
</div>
