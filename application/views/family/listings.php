<div class="container-fluid text-center">
    <h1 class="display-4">
        <strong style="color: rgb(38, 96, 133);">
          Liste des cotations
        </strong>
    </h1>
    <br>
   
</div>
<style>
#tab_wrapper{
    margin-top: 90px;
}
footer{
  position: relative !important;
  margin-top: 20px;
}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" >
		<h4 style="font-weight: bold;font-size: 30px;">Liste Cotation</h4>
		<button class="sousMenu submit" style="right:0px !important;position: absolute; z-index: auto;">
			<i class="fas fa-plus fa-titre" title="Ajouter"></i>
			<a onclick="showAjaxModal('<?php echo base_url();?>family/crud_listing/add/false','<?php echo($titleCotation);?>');">
				<span style="color:#fff;">Ajouter Cotation</span>
			</a>
		</button>
			<table id="tab" class='table display'>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Score</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($listings as $listing) {  ?>
					<tr>
						<td><?=$listing->listing_name?></td>
						<td><?=$listing->listing_score?></td> 
						<td>
							<a href="#" data-href="<?php echo base_url();?>family/crud_listing/delete/<?=$listing->listing_id;?>" data-toggle="modal" data-target="#modal_delete">
								<i class="fas fa-trash" title="Supprimer"></i>
							</a>
							<a onclick="showAjaxModal('<?php echo base_url();?>family/crud_listing/update/<?=$listing->listing_id;?>','<?php echo($titleUpdateListing);?>');">
								<span style="color:#fff;"></span>
								<i class="fas fa-edit" title="Modifier"></i>
							</a>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
        </div>
    </div>
</div>
<div class="text-right">
<br>
<br>
</div>