<!DOCTYPE html>
<html lang="en">
	<head>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>template/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>template/css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>template/css/style.css" rel="stylesheet">
		<script src="<?=base_url()?>assets/js/jquery-1.11.2.min.js"></script>
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/template.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/home.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/users.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/login.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/modal.js"></script>
		<script src="<?=base_url()?>assets/js/qvt_js/jquery.dataTables.min.js"></script>

		<?php
		if(isset($modules)) {
			$GLOBALS["modules"] = $modules;
		} else {
			$GLOBALS["modules"] = array();
		}

		?>
	</head>

    <body id="page-top">
    <!-- Page Wrapper -->
    <div class="wrapper student">
        <!-- Sidebar Holder -->

        <!-- Sidebar -->
        <?php include('application/views/includes/sidebar.php'); ?>
        <!-- End of Sidebar -->


        <!-- Page Content Holder -->
        <div id="content">
            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3"> <i class="fa fa-bars"></i> </button>

                <!-- Topbar Search -->
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800 pl-5 text-uppercase">

                    </h1>
                </div>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto custom-navbar">
                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1"> <a class="nav-link dropdown-toggle" href="/login/logout" title="Déconnexion"> <i class="fas fa-sign-out-alt"></i> </a> </li>
                    <!-- Nav Item - User Information -->
                </ul>
            </nav>
            <!-- End of Topbar -->
            <div class="main-content pl-5 pr-5">
                <?php include('application/views/' . $template_content . '.php'); ?>
            </div>
        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include('application/views/includes/modal.php'); ?>

    <!-- Bootstrap core JavaScript-->

    <script src="<?php echo base_url() ?>template/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url() ?>template/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url() ?>template/js/sb-admin-2.min.js"></script>
    <script src="<?php echo base_url() ?>template/js/jquery.dataTables.min.js"></script>

    <!-- add highcharts js -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="<?=base_url()?>template/js/custom.js"></script>
    </body>
</html>

<script type="text/javascript">
	$(window).load(function(){
   		$('#overlay').fadeOut();
   		$('.content').fadeIn();
	});
</script>
