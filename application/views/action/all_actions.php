
<div class="container-fluid-fluid">
	<div class="row mb-5">
		<div class="col-md-3">
			<span style="color: rgb(38, 96, 133)"><b>Séléctionnner un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?> :</b></span>
			<select id="pole" name="forma" class="pole form-control">
				<option></option>
				<?php foreach ($poles as $key => $p) {
					?>
					<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-3">
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
			<select class="form-control" id="perimetre" name="forma" class="pole"></select>
		</div>
		<div class="col-md-2">
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?>:</b></span>
			<select class="form-control" id="nombre_unite" name="forma" class="pole"></select>
		</div>
		<div class="col-md-2 text-right">
			<span style="color: transparent;" class="d-block">Recherche</span>
			<button class="btn btn-primary p-2" id="searchUnitAction">Recherche</button>
		</div>
		<div class="col-md-1">
			<span style="color: transparent;">Réinitialiser</span>
			<button class="btn btn-primary p-2" id="resetUnitAction">Réinitialiser</button>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
			<table id="tab" class='table display table-bordered table-action'>
				<thead>
				<tr>
					<th><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?></th>
					<th><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?></th>
					<th><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?></th>
					<th>Mesures de prévention recommandées</th>
					<th>Mesures de prévention éxistantes</th>
					<th>Statut</th>
					<th>Actions</th>
				</tr>
				</thead>

				<tbody>

				<?php foreach($mesures as $mesure) {?>
					<tr>
						<td><?=$mesure->pole_nom?></td>
						<td><?=$mesure->perimetre_nom?></td>
						<td><?=$mesure->unite_nom?></td>
						<td><?=$mesure->measures_recommended?></td>
						<td><?=$mesure->measures_concerned?></td>
						<td class="text-center"><?php if($mesure->status == 1) echo "<i class='fas fa-tasks'></i>";
						else if($mesure->status == 2) echo "<i class='fas fa-check-circle'></i>";
						else echo "<i class='fas fa-hourglass-half'></i>";
							?></td>
						<td class="text-center">
							<a href="<?php echo base_url();?>action/deleteMesure/<?=$mesure->id_quantitative_eval;?>" data-toggle="modal" data-target="#modal_delete" class="delete">
								<i class="fas fa-trash-alt " title="Supprimer"></i>
							</a>
							<a href="#"  data-mesureId="<?=$mesure->id_quantitative_eval?>" data-status="<?=$mesure->status ?>" data-toggle="modal" data-target="#edit-action-status" class="edit-action">
								<i class="fas fa-edit " title="Modifer"></i>
							</a>
							<input type="hidden" id="<?=$mesure->id_quantitative_eval?>_contributors" value="<?php  echo $mesure->contributors?>">
							<input type="hidden" id="<?=$mesure->id_quantitative_eval?>_steps" value="<?php echo $mesure->steps?>">
							<input type="hidden" id="<?=$mesure->id_quantitative_eval?>_human_resources" value="<?php echo $mesure->human_resources?>">
							<input type="hidden" id="<?=$mesure->id_quantitative_eval?>_indicator" value="<?php echo $mesure->indicator?>">

							<a href="" data-id_quantitative_eval="<?=$mesure->id_quantitative_eval?>"  data-toggle="modal" data-target="#info-action" class="info-action"><i class="fas fa-info-circle"></i></a>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
    </div>
</div>
<div class="modal fade" id="edit-action-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				Modifier le statut d'action
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-8">
						<select id="status" name="status" class=" form-control">
							<option value="0">En attente</option>
							<option value="1">En cours de mise en oeuvre</option>
							<option value="2">Réalisée</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<a class="btn btn-primary p-2 btn-edit-action">Modifier</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="info-action" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				Détailles de mesure
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label class="d-block"><strong>Pilotes et contributeurs</strong></label>
						<span id="contributors"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="d-block"><strong>Etapes</strong></label>
						<span id="steps"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="d-block"><strong>Moyens humains, techniques et financiers</strong></label>
						<span id="human_resources"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="d-block"><strong>Indicateurs de suivi</strong></label>
						<span id="indicator"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>
<script>
	$('.delete').on('click',function() {
		var link = $(this).attr('href');
		$('#modal_delete .btn-ok').attr('href', link);
	})
	$('.edit-action').on('click',function() {
		var mesureId = $(this).attr('data-mesureId');
		var status = $(this).attr('data-status');
		$('#edit-action-status #status').val(status);
		$('#edit-action-status .btn-edit-action').attr('data-mesureId', mesureId);
	});
	$('.btn-edit-action').on('click',function(e) {
		e.preventDefault();
		var mesureId = $('.btn-edit-action').attr('data-mesureid');
		let status = $('#edit-action-status #status :selected').val();
		//update the status of the selected mesure(mesureId) to the selected status
		$.ajax({
			url: window.location.href+"/updateMesureStatus/" +  mesureId+"/" + status,
			success: function (resp) {
				$('.modal').hide();
				location.reload()
			},
		});
	});

	$('.info-action').on('click',function() {
		let idQuantitativeEval = $(this).attr('data-id_quantitative_eval');
		console.log(idQuantitativeEval)
		$('#info-action #contributors').text($('#'+idQuantitativeEval+'_contributors').val());
		$('#info-action #steps').text($('#'+idQuantitativeEval+'_steps').val());
		$('#info-action #human_resources').text($('#'+idQuantitativeEval+'_human_resources').val());
		$('#info-action #indicator').text($('#'+idQuantitativeEval+'_indicator').val());
	});
</script>
