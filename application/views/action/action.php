<?php
        if ($task == "update") $titre = ("Modifier l'action");
        else if ($task == "update") $titre = ("Modifier l'action");
        else $titre = ('Ajouter une action');
        
        // $titre .= ("mon profil");
   
?>

<div class="container-fluid" style="margin:0px">
    <div class="row">
        <input type="hidden" class="form-control" name="user_id" value="<?php if(isset($user)) {echo($user->user_id);} ?>"  id="user_id" placeholder="">
        
        <div class="col-md-6 ">
            <?php if ($updated) : ?>
            <!-- div class="alert alert-success text-center"><?= $monprofil == TRUE ? 'Votre' : 'Le' ?> profil a été mis à jour avec succès</div -->
            <?php endif; ?>
            
            <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);"><?=$titre?> </strong></h2>

            <div class="col-md-4"></div><div class="col-md-2"></div>
            <form name="addUsers" method="post" class="form-group" 
                id="<?php   if(isset($user)) {echo('editUser');}
                            else {echo('addUser');}?>">
                <div class="form-group" id="name-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_user">Action</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_firstname" value="<?php if(isset($user)) {echo($user->user_firstname);} ?>"  id="name_user" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="lastname-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="lastname_user">Périmètre concerné</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_lastname" id="lastname_user" value="" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="email-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="email_user">Population ciblée</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="email" class="form-control" name="user_email" id="email_user" value="" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="user-id-access-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="user_role">Détails</label>
                        </div>
						 <div class="col-sm-9 col-8">
                            <input type="textarea" class="form-control"  id="name_user" placeholder="" 
                            <?php if($task == "show"){ echo("disabled"); } ?> >
                            <small class="help-block"></small>
                        </div>
                    
                    </div>
                    
                </div>

                
                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password">échéance de la mise en oeuvre</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control"  id="name_user" placeholder="" >
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">Pilotes</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="pilotes" id="pilotes" placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                
                 <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">Acteurs associés</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="acteurs" id="acteurs" placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
					
					
					 <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">état d'avancement</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="etat_avancement" id="etat_avancement" placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div> 
					
					<div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">modalités et indicateurs de suivi</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="text" class="form-control" name="etat_avancement" id="etat_avancement" placeholder="">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
					
					<?php if($task != "update"){ ?>
                    <div class="form-group" id="user-submit-group">
                        <button type="submit" class="submit btn btn-primary" id="<?php if(isset($action))  {echo('edit-action');} else  {echo('add-action');}?>">
                            <div class="glyphicon glyphicon-plus"></div>
                            <?php if(isset($action)){ echo('Modifier'); } else { echo('Ajouter'); }?>
                        </button>

                        <a href="/users" class="btn sousMenu submit mr-2">
                            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
                            <span style="color:#3e91b2ff;">Retour</span>
                        </a>
                    </div>
                <?php } ?>
                <div id="the-message-add-user"></div>
				
            </form>
        </div>

        <br>
		
    </div>
    
</div>
