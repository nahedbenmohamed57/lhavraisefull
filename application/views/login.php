<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url() ?>template/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url() ?>template/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<body>
<header class="login">

    <div class="wrapper wrapper-login wrapper-login-full p-0">
        <div class="login-aside w-50 d-flex align-items-center justify-content-center">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-md-2">
                        <div class="p-4">
                            <div class="container-fluid vertical-middle">
                                <div class="text-center">
                                    <h1 class="login-title" style="font-size: 46px; color: #3e91b2ff"><strong><b>Evaluation interne des risques
                                            psychosociaux</b></strong></h1>
                                </div>

                            </div>
						</div>
						<div class="" style="padding: 0rem 8rem;">
                            <?php if($message) { ?>
                                <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
                            <?php } ?>
                            <form method="post" name="login" id="login" class="user text-center" accept-charset="utf-8">
                                <div class="form-group" id="user-group">
                                    <input type="text" class="form-control form-control-user"
                                           id="user" name="user" placeholder="IDENTIFIANT" value="">
                                </div>
                                <div class="form-group" id="password-group">
                                    <div class="position-relative show-pass" id="password-group">
                                        <input type="password" class="form-control form-control-user" id="password" placeholder="MOT DE PASSE" name="password" value="" >
                                        <div class="show-password"> <i class="fas fa-eye"></i> </div>
                                    </div>
                                </div>
                                <div id="the-message"></div>
                                <div class="row">
                                    <div class="col-6 form-group text-right">
                                        <a href="<?php echo site_url('/login/forgetPwd') ?>" class="text-gray-700 forgot-password" id="forgot-password">Mot de passe oublié? </a>
                                    </div>
                                </div>
                                <input type="submit" id="submit-login" name="submit" class="btn btn-primary p-2" value="CONNEXION">
                                <div id="the-message"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>

<!-- Custom scripts for all pages-->
<script src="<?=base_url()?>assets/js/jquery-1.11.2.min.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/template.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/home.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/login.js"></script>
<script src="<?=base_url()?>assets/js/qvt_js/jquery.dataTables.min.js"></script>

</body>
</html>
