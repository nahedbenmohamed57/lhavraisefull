
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <?php if (isset($_GET['error'])) : ?>
            <div class="alert alert-danger">
                Erreur lors du téléchargement du fichier
            </div>
            <?php endif; ?>

        	<button class="sousMenu submit btn btn-primary float-right" data-toggle="modal" data-target="#modal-upload">
                <i class="fas fa-plus fa-titre" title="Ajouter"></i>
                <span style="color:#fff;">Ajouter fichier</span>
            </button>

			<table id="tab" class='table display table-bordered'>
				<thead>
					<tr>
						<th>Fichier</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($files as $file) { ?>
					<tr>
						<td><?=$file?></td>
						<td class="text-right">
                            <a href="<?php echo base_url();?>outil/download/<?=$file;?>">
								<i class="fas fa-download" title="Télécharger"></i>
							</a>

                            &nbsp;

							<a href="<?php echo base_url();?>outil/delete/<?=$file;?>" data-toggle="modal" data-target="#modal_delete" class="delete">
								<i class="fas fa-trash-alt" title="Supprimer"></i>
							</a>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open_multipart('outil/upload');?>
                <div class="modal-header text-white">
                    Ajouter un fichier
                </div>

                <div class="modal-body">
                    <input type="file" name="userfile">
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger btn-ok text-white">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.delete').on('click',function() {
        var link = $(this).attr('href');
        $('#modal_delete .btn-ok').attr('href', link);
    })
</script>
