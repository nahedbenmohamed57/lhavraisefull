<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 pt-3">
			<span style="color: rgb(38, 96, 133)"><b>Séléctionnner un <?php if(isset($_SESSION['config'])) echo $_SESSION['config']->pole_name; else echo "pôle"?> :</b></span>
			<select id="pole" name="forma" class="pole form-control">
				<option><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->pole_name); else echo "Pôle"?></option>
				<?php foreach ($poles as $key => $p) {
					?>
					<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 pt-3">
			<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->etab_name); else echo "Etablissement"?>:</b></span>
			<select class="form-control" id="perimetre" name="forma" class="pole"></select>
		</div>
	</div>
		<div class="row">
			<div class="col-md-4 pt-3">
				<span style="color: rgb(38, 96, 133)"><b><?php if(isset($_SESSION['config'])) echo ucfirst($_SESSION['config']->unit_name); else echo "Unité de travail"?>:</b></span>
				<select class="form-control" id="nombre_unite" name="forma" class="pole"></select>
			</div>
		</div>
	<div class="row">
		<div class="col-md-4 pt-2">
			<button id="fill-indicator-btn" class="btn sousMenu submit mt-3 w-100 btn btn-primary p-2 ml-1" >Démarrer la saisie des indicateurs</button>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
	$('#fill-indicator-btn').on('click', function () {
		var perimetre = $("#perimetre").val();
		var unit =  $("#nombre_unite").val();
		if(perimetre != null) {
			 window.location = '/indicateurs/fill_indicator/'+unit;
		}
	})
</script>
