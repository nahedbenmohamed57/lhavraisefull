<style>
footer{
  position: relative !important;
}
</style>
<div class="container-fluid" style="margin:0px">
<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Définir les indicateurs </strong></h2>

	<a  onclick="showAjaxModal('<?php echo base_url();?>family/crud_family/add','<?php echo($titleAddFamily);?>');">
		<button class="btn sousMenu submit" style="margin-right: 27px;cursor: pointer;">
			<i class="fas fa-plus fa-titre" title="Ajouter"></i>
			<span style="color:#fff;">Ajouter Famille</span>
		</button>
	</a>

    <div class="row col-md-12" style="margin-top: 20px;">
		<?php foreach($families as $family){ ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="family-card" style="position: relative;">
					<button class="nameFamily">
						<span style="color:#fff;"> <b><?=$family->family_name;?></b></span>
					</button>
					
					<div class="family">
						<a onclick="showAjaxModal('<?php echo base_url();?>family/crud_family/update/<?=$family->family_id;?>','<?php echo($titleUpdateFamily);?>');" style="cursor: pointer;" >
							<i class="fas fa-edit fa-titre" style="padding-left:9px;" title="Modifier"></i>
						</a>

						&nbsp;
						
						<a href="#" data-href="<?php echo base_url();?>family/crud_family/delete/<?=$family->family_id;?>" data-toggle="modal" data-target="#modal_delete">
							<i class="fas fa-trash fa-titre" title="Supprimer"></i>
						</a>
					</div>

					<div class="liste">
						<!--<table class='table table-hover display'>-->
						<table class=' table table-bordered'>
							<tbody>
							<?php foreach($determinants as $determinant){ 
								if($determinant->family_id == $family->family_id){ ?>
								<tr id="">
								
									<td>
										<div  onclick="showListing()" style="padding-bottom: 15px;">
											<a href="<?php echo base_url() ?>family/determinant/view/<?=$family->family_id;?>/<?=$determinant->determinant_id;?>" style="color: #000;!important" >
												<p class="determinant" ><?php echo('-'. $determinant->determinant_name) ; ?></p>
											</a>
										</div>
									</td>
									<td style="width: 20%;">
										<a href="#" data-href="<?php echo base_url();?>family/determinant/delete/false/<?=$determinant->determinant_id;?>" data-toggle="modal" data-target="#modal_delete">
											<i class="fas fa-trash fa-deter" title="Supprimer"></i>
										</a>
										<a onclick="showAjaxModal('<?php echo base_url();?>family/determinant/update/false/<?=$determinant->determinant_id;?>','<?php echo($titleUpdatDeterminant);?>');">
											<i class="fas fa-edit fa-deter" title="Modifier"></i>
										</a>
									</td>
									<td>N-3</td>
									<td>N-2</td>
									<td>N-1</td>
								</tr>
							<?php } } ?>	
							</tbody>
						</table>
					</div>
					
					<br>
					<br>

					<a onclick="showAjaxModal('<?php echo base_url();?>family/determinant/add/<?=$family->family_id;?>','<?php echo($titledeter);?>');" style="position: absolute; bottom: 0px; width: 100%;">
						<button class="sousMenu submit" style="cursor: pointer; cursor: pointer; float: none; width: 100%; margin-bottom: 0px;">
							<i class="fas fa-plus fa-titre" title="Ajouter"></i>
							<span style="color:#fff;">Ajouter un indicateur</span>
						</button>
					</a>
				</div>
			</div>
		<?php } ?>
		</div>
    </div>
</div>
<br>
<div>
	<table class=' table table-bordered'>
		<tr id="">
		<td>Indice de tendance(IT)</td>
		<td>Moy</td>
		</tr>
		</table>
</div>
	<a style="position: absolute; bottom: 0px; width: 100%;">
						<button class="sousMenu submit" style="cursor: pointer; cursor: pointer; float: none; width: 100%; margin-bottom: 0px;">
							<span style="color:#fff;">Terminer et sauvegarder</span>
						</button>
	</a>
<script>
function goBack() {
    window.history.back();
}
</script>

