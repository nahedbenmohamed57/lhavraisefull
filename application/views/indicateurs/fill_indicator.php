<style>
	table input {
		width: 50px;
	}
	.indice_1 {
		background: #66aa88;
		text-align: center;
	}
	.indice_2 {
		background: #ffc000;
		text-align: center;
	}
	.indice_3 {
		background: #c0504d;
		text-align: center;
	}
	.indice_4 {
		background: #404040;
		text-align: center;
		color: #fff;
	}
</style>
<div class="container-fluid indicator-page" style="margin:0px">
	<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Définir les indicateurs </strong></h2>

	 <form name="form" role="form"  class="form-horizontal form-groups-bordered"  style="overflow-y: overlay;" method="post" enctype="multipart/form-data">
	 		<?php foreach($families as $family){ ?>
	  		<table class="table table-bordered" style="margin-top: 20px">
	  			<thead>
	  			<tr>
	  				<td width="20%">Familles</td>
	  				<td>Indicateurs</td>
	  				<td>N-3</td>
	  				<td>N-2</td>
	  				<td>N-1</td>
	  				<td>Indice de tendance (IT)</td>
	  			</tr>
	  		</thead>
	  		<tbody>
	  			<tr>
	  				<td rowspan="<?=sizeof($family->list_determinant)+1;?>" width="20%">
	  					<span><?=$family->family_name;?></span>
	  				</td>
	  			</tr>
				<?php  
				foreach($family->list_determinant as $key => $determinant){
				 $i = 0;

					$current_perim = [];
					foreach ($family->list_perimetre as $key => $value) {
						if( $value->determinant_id == $determinant->determinant_id) {
							
							$current_perim = $value;
						}
					}
			       
					$status = ($determinant->family_nature == 'positif') ? "green" : "red";
					?>
				    <tr class="detail-indicator">
						<td width="90%">
							<span><?=$determinant->determinant_name?><small style="color:<?=$status?>"> (<?=$determinant->family_nature?>)</small></span>
						</td>
						<td><input type="text" name="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>_year3" data-status= "<?=$status?>" data-info="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>" class="year" value="<?php if(!empty($current_perim)) echo $current_perim->year3?>"> </td>

						<td ><input type="text" name="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>_year2" data-status= "<?=$status?>" data-info="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>" class="year" value="<?php if(!empty($current_perim)) echo $current_perim->year2?>"></td>

						<td ><input type="text" name="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>_year1" data-status= "<?=$status?>"  data-info="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>" class="year" value="<?php if(!empty($current_perim)) echo $current_perim->year1?>"></td>
						<td><input class="indice_<?=$current_perim->trend_index?>" type="text" name="<?=$family->family_id;?>_<?=$determinant->determinant_id;?>_trend_index" id="indice_tendance_<?=$family->family_id;?>_<?=$determinant->determinant_id;?>" data-family_id="<?=$family->family_id;?>" value="<?php if(!empty($current_perim)) echo $current_perim->trend_index?>"></td>
					</tr>
				<?php 
				$i++;
			} ?>
	  		</tbody>
	  	</table>

	  	<?php }?>
	  	<div class="form-group" style="margin-top: 20px">
            <button type="submit" class="submit btn btn-primary">
                Terminer et Sauvegarder
            </button>
        </div>
	 </form>
</div>
<script type="text/javascript">
	$(".detail-indicator .year").on('keyup',function() {
		var val    = $(this).val();

		var status = $(this).data('status');

		var info = $(this).data('info');
		//test if negative
		var year1 = $('[name='+info+'_year1]').val();
		var year2 = $('[name='+info+'_year2]').val();
		var year3 = $('[name='+info+'_year3]').val();
		var indice = 0;
		if(!isNaN(val)) {
			var res = year1/((year2+year3)/2);
			console.log(res);
            if(status == "red") {
            	if(1.05 > res && res > 1) {
            		indice = 2;
            	} else if (1.15 > res && res > 1.05) {
                    indice = 3;
            	} else if (res > 1.15) {
                   indice = 4;
            	} else if(res < 1) {
                    indice = 1;
            	}
            } else {
				if(1.05 > res && res > 1) {
					indice = 3;
            	} else if (1.15 > res && res > 1.05) {
                    indice = 2;
            	} else if (res > 1.15) {
					indice = 1;
            	} else if(res < 1) {
                    indice = 4;
            	}
            }
             $('#indice_tendance_'+info).removeClass('indice_1 indice_2 indice_3 indice_4')

            if(indice == 1) {
               $('#indice_tendance_'+info).addClass('indice_1')
            } else if(indice == 2) {
            	$('#indice_tendance_'+info).addClass('indice_2')

            } else if(indice == 3 ) {
                $('#indice_tendance_'+info).addClass('indice_3')
            } else if(indice == 4) {
                 $('#indice_tendance_'+info).addClass('indice_4')
            }

			$('#indice_tendance_'+info).val(indice);

			//update AVG value
			var family_id = $('#indice_tendance_'+info).data('family_id');

			/*var moy = 0;
			$.each( $('.indice'), function( key, value ) {
			  if($(this).data('family_id') == family_id) {
			  	if(!isNaN(parseInt($(this).val()))) {
			  		moy = moy+parseInt($(this).val());
			  	}
			  }
			});

			$('#moy_'+family_id).val(moy);*/
		}

	});
</script>
