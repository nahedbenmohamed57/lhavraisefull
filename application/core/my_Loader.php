<?php
class MY_Loader extends CI_Loader 
{
	public function render($template_content, $vars = array(), $return = FALSE)
    {
		if(!isset($_SESSION["role_id"]))
		{
				Redirect(''); 
		}
		$ci =& get_instance();
		$ci->load->helper('functions');
		$ci->load->model('m_modules');
		$ci->load->model('m_access'); 
		$vars["current_module"] = getModule(); 
		$vars["modules"] = $ci->m_access->get_module_role($_SESSION["role_id"]); 
		$vars["template_content"] = $template_content ; 
        $this->view('main', $vars);
    }
}