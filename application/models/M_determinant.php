<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_determinant extends CI_Model{

    public $table = "determinant";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_device');
		$this->load->model('m_listing');
    }

    public function get_determinant($determinant_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("determinant_id",$determinant_id)
            ->get()
            ->row();
    }

    public function get_determinants_ByFamily($family_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("family_id", $family_id)
            ->get()
            ->result();
    }

    public function get_determinants_byPole($id_pole) {
        return $this->db->select('determinant.determinant_id, determinant_name, family_id, valeur as determinant_importance')
            ->from($this->table)
            ->where("importance.pole_id", $id_pole)
            ->join('importance', 'importance.determinant_id = determinant.determinant_id')
            ->get()
            ->result();
    }

    public function get_importance($id, $id_pole = 0, $id_periode = 0) {
        if ($id_pole) {
            $this->db->where('pole_id', $id_pole);
        }

        if ($id_periode) {
            $this->db->where('periode_id', $id_periode);
        }

        return $this->db->select('AVG(valeur) as result')
            ->from('importance')
            ->where("determinant_id", $id)
            ->get()
            ->result()[0]->result;
    }
	
	public function get_determinants(){
        $result = $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
            foreach ($result as $key => $r) {
                $correlation = $this->db->select('*')
                ->from('correlation')
                ->where('correlation.id_determinant='.$r->determinant_id)
                ->get()
                ->result();
                $r->corrolation      =  $correlation;
            }
        return $result;
    }

    public function add_determinant($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

    public function update_determinant($determinant_id, $data)
    {
        $this->db->where('determinant_id',$determinant_id);
		$this->db->set($data);
		$this->db->update('determinant');
    }

    public function delete_determinant($determinant_id){
        $this->db->where('determinant_id', $determinant_id);
        $this->db->delete('determinant');
        $devices = $this->m_device->get_device_ByDeter($determinant_id); 
        foreach($devices as $device){
            $this->m_device->delete_device($device->device_id); 
        }
    }
}
