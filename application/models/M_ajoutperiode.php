<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_ajoutperiode extends CI_Model{

    public $table = "periode";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_ajoutperiode');
       
    }
	
	public function get_periode(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }
    
     public function add_periode($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }
    
    public function delete_periode($periode_id)
    {
        $this->db->where('id_periode',$periode_id);
        $this->db->delete('periode');
      
    }
}