<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class M_deviceHasListing extends CI_Model{

    public $table = "device_has_listing";

    public function __construct(){
        parent::__construct();
    }

	public function get_all(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }

    public function get_ByDeviceId($device_id){
        $this->db->select('*');
		$this->db->from($this->table);
        $this->db->where('device_id',$device_id); 
        $this->db->order_by("id", "asc");    
		$query = $this->db->get();
		return($query->result()); 
		
    }

    public function get_NumberListing($device_id){
        $this->db->select('count(listing_id)');
		$this->db->from($this->table);
        $this->db->where('device_id',$device_id);    
		$query = $this->db->get();
		return($query->result()[0]); 
		
    }
    public function get_all_devices(){
        return $this->db->select('*')
            ->from('device_has_listing')
            ->join('listing', 'listing.listing_id = device_has_listing.listing_id')
            ->get() 
            ->result();
    }
}