<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_facteur extends CI_Model{

    public $table = "facteur";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_device');
		$this->load->model('m_listing');
    }

    public function get_facteur($facteur_id){
        return $this->db->select('*')
            ->from($this->table)
            ->join('family_rps','family_rps.family_id='.$this->table.'.family_rps_id')
            ->where("facteur_id",$facteur_id)
            ->get()
            ->result()[0];
    }

    public function get_facteurs_ByFamily($family_id){
        $query = $this->db->select('*')
            ->from($this->table)
            ->where("family_rps_id", $family_id)
            ->get();

            if($query)  {
                return  $query->result();
            } else {
                return [];
            }
    }

    public function get_facteurs_byPole($id_pole) {
        return $this->db->select('facteur.facteur_id, facteur_name, family_id, valeur as facteur_importance')
            ->from($this->table)
            ->where("importance.pole_id", $id_pole)
            ->join('importance', 'importance.facteur_id = facteur.facteur_id')
            ->get()
            ->result();
    }

    public function get_importance($id, $id_pole = 0, $id_periode = 0) {
        if ($id_pole) {
            $this->db->where('pole_id', $id_pole);
        }

        if ($id_periode) {
            $this->db->where('periode_id', $id_periode);
        }

        return $this->db->select('AVG(valeur) as result')
            ->from('importance')
            ->where("facteur_id", $id)
            ->get()
            ->result()[0]->result;
    }
	
	public function get_facteurs(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }

    public function add_facteur($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

    public function update_facteur($facteur_id, $data)
    {
        $this->db->where('facteur_id',$facteur_id);
		$this->db->set($data);
		$this->db->update('facteur');
    }

    public function delete_facteur($facteur_id){
        $this->db->where('facteur_id', $facteur_id);
        $this->db->delete('facteur');
        $devices = $this->m_device->get_device_ByDeter($facteur_id); 
        foreach($devices as $device){
            $this->m_device->delete_device($device->device_id); 
        }
    }

    public function get_facteurs_correlation(){
        return 
        $this->db->select('*')
            ->from($this->table)
            //->join('correlation','correlation.id_facteur = '.$this->table.'.facteur_id','left')
            ->get()
            ->result();
    }

    public function insert_correlation($data = null)
    {
       $this->db->truncate('correlation');
       if(!empty($data)) {
        $this->db->insert_batch('correlation', $data); 
       }
    }
    public function get_moy_indicator_by_factor($id_factor) {
         return $this->db->select('AVG(trend_index) as moy')
            ->from('correlation')
             ->join('indicators','indicators.determinant_id = correlation.id_determinant')
            ->where('id_facteur', $id_factor)
            ->order_by('date_insert','desc')
            ->get()
            ->result();
    }
     public function get_indicator_by_factor($id_factor) {
        return $this->db->select('*')
            ->from('correlation')
             ->join('indicators','indicators.determinant_id = correlation.id_determinant')
            ->where('id_facteur', $id_factor)
            //->where('trend_index!=',0)
            ->order_by('date_insert','desc')
            ->get()
            ->result();
    }
}