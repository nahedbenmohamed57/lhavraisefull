<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_user extends CI_Model{

	public $table = "qvt_user";


	public function __construct(){
		parent::__construct();
	}

	public function get_user($user, $password){
		return $this->db->select('*')
			->from($this->table)
			->where("user_email",$user)
			->where("user_password",$password)
			->where('active',1)
			->get()
			->result();
	}

	public function add_user($entry){
		if($this->db->insert($this->table,$entry)){
			return true;
		}
		return false;
	}

	public function address_exist($email){
		return $this->db->select('*')
			->from($this->table)
			->where("user_email", $email)
			->where('active', 1)
			->get()
			->result();
	}

	public function update_user($id, $data)
	{
		$this->db->where('user_id',$id);
		$this->db->set($data);
		$this->db->update('qvt_user');
	}

	public function get_users(){
		return $this->db->select('*')
			->from($this->table)
			->where('active',1)
			->get()
			->result();
	}
	public function delete_user($user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->set('active', 0);
		$this->db->update('qvt_user');
	}

	public function get_user_ById($user_id){
		return $this->db->select('*')
			->from($this->table)
			->where("user_id",$user_id)
			->get()
			->result()[0];
	}

	public function get_config() {
		return $this->db->select('*')
			->from('config')
			->limit(1)
			->get()
			->result()[0];
	}
	/**
	 *  update the config msg with the given msg
	 **/
	public function update_msg($msg) {
		$this->db->where('id',1);
		$this->db->set($msg);
		$this->db->update('config');
	}

	public function set_image_config ($image_path) {
		$this->db->where('id',1);
		$this->db->set(array('image_platforme'=> $image_path));
		$this->db->update('config');
	}

	public function set_config ($config) {
		$this->db->where('id',1);
		$this->db->set($config);
		$this->db->update('config');
	}

	public function getAdmins($role) {
		return $this->db->select('user_firstname,user_lastname, user_email')
			->from($this->table)
			->where('active',1)
			->where('user_role',$role)
			->get()
			->result();
	}
}
