<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_importance extends CI_Model{
    public $table = "importance";

    public function __construct(){
        parent::__construct();
    }

    function periode_active() {
        return $this->db->select('*')
            ->from('periode')
            ->where("active", "1")
            ->get()
            ->result()[0]->id;
    }
	
	public function get($pole_id) {
        $periode = $this->periode_active();

        return $this->db->select('*')
            ->from($this->table)
            ->where('pole_id', $pole_id)
            ->where('periode_id', $periode)
            ->get()
            ->result();
    }

    public function create($determinant_id, $pole_id, $value){
        $periode = $this->periode_active();

        if($this->db->insert($this->table, array(
            'determinant_id' => $determinant_id,
            'pole_id' => $pole_id,
            'periode_id' => $periode,
            'valeur' => $value
        ))) {
            return true;
        }

        return false;
    }

    public function exists($determinant_id, $pole_id) {
        $periode = $this->periode_active();
        
        $result = $this->db->select('*')
            ->from($this->table)
            ->where('pole_id', $pole_id)
            ->where('determinant_id', $determinant_id)
            ->where('periode_id', $periode)
            ->get()
            ->result();
        
        return sizeof($result) > 0;
    }

    public function update($determinant_id, $pole_id, $value)
    {
        $periode = $this->periode_active();

        $this->db->where('determinant_id', $determinant_id);
        $this->db->where('pole_id', $pole_id);
        $this->db->where('periode_id', $periode);
		$this->db->update($this->table, array('valeur' => $value));
    }
}