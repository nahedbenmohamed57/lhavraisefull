<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_family extends CI_Model{

    public $table = "family";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_determinant');
    }

    public function get_family($family_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("family_id",$family_id)
            ->get()
            ->result()[0];
    }

    public function add_family($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

	public function update_family($family_id, $data)
    {
        $this->db->where('family_id',$family_id);
		$this->db->set($data);
		$this->db->update('family');
    }
	
	public function get_families(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }

    public function delete_family($family_id){
        $this->db->where('family_id', $family_id);
        $this->db->delete('family');
        $determinants =  $this->m_determinant->get_determinants_ByFamily($family_id); 
        foreach($determinants as $determinant){
            $this->m_determinant->delete_determinant($determinant->determinant_id);
        } 
    }

    public function get_families_ToQCM(){
        $this->db->select('*');
        $this->db->from('family');
        $this->db->join('determinant', 'determinant.family_id = family.family_id');
        $this->db->join('device', 'determinant.determinant_id = device.determinant_id');
        $this->db->join('device_has_listing', 'device_has_listing.device_id = device.device_id');
        $this->db->join('listing', 'listing.listing_id = device_has_listing.listing_id');
        $this->db->order_by("device.device_id", "asc");
         $this->db->order_by("listing.listing_id ", "asc");
        $query = $this->db->get() ->result();
        return ($query);

    }

    public function get_families_ToExport(){
        $this->db->select('*');
        $this->db->from('family');
        $this->db->join('determinant', 'determinant.family_id = family.family_id');
        $this->db->join('device', 'determinant.determinant_id = device.determinant_id');
        $query = $this->db->get() ->result();
        return ($query);
    }


   /* public function get_count_device($family_id){
        $this->db->select('*');
        $this->db->from('determinant');
        $this->db->where('determinant.family_id', $family_id);
        $this->db->join('device', 'determinant.determinant_id = device.determinant_id');
        $query = $this->db->get() ->result()[0];
        return ($query);
    }

    public function get_count_determinant($family_id){
        $this->db->select('*');
        $this->db->from('determinant');
        $this->db->where('determinant.family_id', $family_id);
        $query = $this->db->get() ->result()[0];
        return ($query);
    }*/
}