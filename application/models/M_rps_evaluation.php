<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_rps_evaluation extends CI_Model{

    public $table = "rps_evaluation";

    public function __construct(){
        parent::__construct();
		error_reporting(-1);
		ini_set('display_errors', 1);
    }

    public function add_evaluation_param($data){
        $this->db->insert($this->table, $data);
    }
    public function get_last_evaluation($pole_id, $perimetre_id, $unit_id) {
		return $this->db->select('id_rps_evaluation')
            ->from($this->table)
            ->where('pole_id', $pole_id)
            ->where('establishment_id', $perimetre_id)
            ->where('unit_id', $unit_id)
            ->order_by('evaluation_date','desc')
			->order_by('id_rps_evaluation','desc')
            ->limit(1)
            ->get()
            ->row();
        
    }
    public function insert_iquantitav_eval($data)
    {
    	if(!empty($data)) {
        	$this->db->insert_batch('quantitative_evaluation', $data); 
       	}
    }
    public function get_evaluation_by_ref_eval($id_rps_evaluation)
    {
    	return $this->db->select('*')
            ->from($this->table)
            ->join('quantitative_evaluation','quantitative_evaluation.rps_evaluation_id='. $this->table.'.id_rps_evaluation')
            ->where('id_rps_evaluation', $id_rps_evaluation)
            ->get()
            ->result();
    }
    public function delete_iquantitav_eval($id_rps_evaluation) {
       $this->db->where('rps_evaluation_id', $id_rps_evaluation);
       $this->db->delete('quantitative_evaluation'); 
    }

    public function get_eval_by_pole($pole_id, $active_periode_id) {
        return $this->db->select('id_rps_evaluation')
            ->from($this->table)
            ->where('pole_id', $pole_id)
            ->where('evaluation_periode', $active_periode_id)
            ->order_by('id_rps_evaluation','asc')
            ->limit(1)
            ->get()
            ->row();    
    }

     public function get_details_eval($id_rps_evaluation) {

       $result = $this->db->select('facteur_id,intensity,priority')
            ->from('quantitative_evaluation')
            ->where('rps_evaluation_id', $id_rps_evaluation)
            ->get();
            if(!empty($result)) {
                return $result->result();
            } else {
                return [];
            }
            
    }
    public function add_priority_risks ($name_priority_risks, $rps_evaluation_id) {
        $data = array(
            'name_priority_risks' => $name_priority_risks,
        );
        $this->db->insert('priority_risks', $data);

        //add the priority_risks to the current qualitative evaluation
        $insert_id = $this->db->insert_id();

        $data_eval = array(
            'id_rps_evaluation'   => $rps_evaluation_id,
            'id_priority_risks'   => $insert_id,
        );
        $this->db->insert('qualitative_evaluation', $data_eval);
        echo $this->db->last_query();

    }
    public function get_priority_risks($rps_evaluation_id) {

        return $this->db->select('*')
            ->from('qualitative_evaluation')
            ->join('priority_risks','priority_risks.id_priority_risks=qualitative_evaluation.id_priority_risks')
            ->where('id_rps_evaluation', $rps_evaluation_id)
            ->get()
            ->result();
    }

    public function add_working_situations ($data) {
       
        $this->db->insert('working_situations', $data);
    }
    public function get_working_situations($id_priority_risks) {
        return $this->db->select('*')
            ->from('working_situations')
            ->where('id_priority_risks', $id_priority_risks)
            ->get()
            ->result();
    }

    public function get_working_situations_by_eval($rps_evaluation_id) {
        return $this->db->select('*')
            ->from('working_situations')
             ->join('qualitative_evaluation','qualitative_evaluation.id_priority_risks=working_situations.id_priority_risks')
            ->where('qualitative_evaluation.id_rps_evaluation', $rps_evaluation_id)
            ->get()
            ->result();
    }

    public function add_working_areas ($data) {
       
        $this->db->insert('work_areas', $data);
    }
    public function get_work_areas_by_situation($id_working_situations) {
        return $this->db->select('*')
            ->from(' work_areas')
            ->where('work_areas.id_working_situations', $id_working_situations)
            ->get()
            ->result();
    }

    public function get_priority_risks_by_id($id_priority_risks) {

        return $this->db->select('*')
            ->from('qualitative_evaluation')
            ->join('priority_risks','priority_risks.id_priority_risks=qualitative_evaluation.id_priority_risks')
            ->where('priority_risks.id_priority_risks', $id_priority_risks)
            ->get()
            ->row();
    }
    public function update_priority_risks($data) {

        if(!empty($data['id_priority_risks'])) {
           $this->db->set('name_priority_risks', $data['name_priority_risks']);
            $this->db->where('id_priority_risks', $data['id_priority_risks']);
            $this->db->update('priority_risks');
        }

    }

    public function get_working_situations_by_id($id_working_situations) {

        return $this->db->select('*')
            ->from('working_situations')
            ->where('id_working_situations', $id_working_situations)
            ->get()
            ->row();
    }
    public function update_working_situations($data) {

        if(!empty($data['id_working_situations'])) {
            $data_Update = array('name_working_situations' => $data['name_working_situations'], 'id_priority_risks'=> $data['id_priority_risks']);
            $this->db->where('id_working_situations', $data['id_working_situations']);
            $this->db->update('working_situations', $data_Update);

        }
    }

     public function get_work_areas_by_id($id_work_areas) {

        return $this->db->select('*')
            ->from('work_areas')
            ->where('id_work_areas', $id_work_areas)
            ->get()
            ->row();
    }

    public function update_work_areas($data) {

        if(!empty($data['id_working_situations'])) {
           
            $data_Update = array('work_areas_name' => $data['work_areas_name'], 'id_priority_risks'=> $data['id_priority_risks'], 'id_working_situations'=> $data['id_working_situations']);
            $this->db->where('id_work_areas', $data['id_work_areas']);
            $this->db->update('work_areas', $data_Update);
            echo $this->db->last_query();

        }
    }

    public function get_eval_by_etablissement($etab_id, $active_periode_id) {
        $result = $this->db->select('id_rps_evaluation')
            ->from($this->table)
            ->where('establishment_id', $etab_id)
            ->where('evaluation_periode', $active_periode_id)
            ->get();
            if($result) {
                return $result->result();
            } else {
                return [];
            }
            
    }

    public function get_eval_by_unit($unit_id, $active_periode_id) {
        return $this->db->select('id_rps_evaluation')
            ->from($this->table)
            ->where('unit_id', $unit_id)
            ->where('evaluation_periode', $active_periode_id)
            ->get()
            ->result();
    }

    public function get_evaluation_by_periode($pole_id, $perimetre_id, $unit_id,$active_periode_id) {
        return $this->db->select('id_rps_evaluation')
            ->from($this->table)
            ->where('pole_id', $pole_id)
            ->where('establishment_id', $perimetre_id)
            ->where('unit_id', $unit_id)
            ->where('evaluation_date',$active_periode_id)
            ->get()
            ->row();
    }

	public function get_all_evaluation($active_periode_id) {
		return $this->db->select('count(id_rps_evaluation) as nbEval')
			->from($this->table)
			->where('evaluation_date',$active_periode_id)
			->get()
			->row();
	}

	public function getAllMesure() {
		return $this->db->select('id_quantitative_eval, measures_concerned, measures_recommended,pole.pole_nom,establishment.perimetre_nom, unite.unite_nom, quantitative_evaluation.status,
		 quantitative_evaluation.contributors, quantitative_evaluation.steps,quantitative_evaluation.human_resources,quantitative_evaluation.indicator')
        ->join('rps_evaluation','rps_evaluation.id_rps_evaluation=quantitative_evaluation.rps_evaluation_id')
        ->join('pole','pole.id_pole=rps_evaluation.pole_id')
        ->join('establishment','establishment.id_perimetre=rps_evaluation.establishment_id')
        ->join('unite','unite.id_unite=rps_evaluation.unit_id')
		->from('quantitative_evaluation')
		->where('measures_concerned !=', "")
		->or_where('measures_recommended !=', "")
		->get()
		->result();
	}
    function delete_mesure_text($evalId) {
        $this->db->set('measures_concerned', '');
        $this->db->set('measures_recommended', '');
        $this->db->where('id_quantitative_eval', $evalId);
        $this->db->update('quantitative_evaluation');
    }
    //update mesure sttaus
	public function update_mesure_staus($mesureId, $status) {

    	if($status == 2) {
    		//the current mesure become existnat
			$mesure = $this->db->select('measures_concerned,measures_recommended ')
				->from('quantitative_evaluation')
				->where('id_quantitative_eval', $mesureId)
				->limit(1)
				->get()
				->row();
			if($mesure) {
				$this->db->set('measures_concerned', $mesure->measures_recommended);
				$this->db->set('measures_recommended', '');
			}
		}
		$this->db->set('status', $status);
		$this->db->where('id_quantitative_eval', $mesureId);
		$this->db->update('quantitative_evaluation');
	}

}

