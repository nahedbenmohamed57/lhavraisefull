<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_role extends CI_Model{

    public $table = "qvt_role";

    public function __construct(){
        parent::__construct();
    }

    public function get_role($id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("role_id",$id)
            ->get()
            ->result()[0];
    }
	public function get_all_roles(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }
}