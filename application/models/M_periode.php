<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_periode extends CI_Model{

	public $table = "periode";


	public function __construct(){
		parent::__construct();
	}

	public function get(){
		return $this->db->select('*')
			->from($this->table)
			->order_by('id', 'asc')
			->get()
			->result();
	}

	public function add($entry){
		if($this->db->insert($this->table,$entry)){
			return true;
		}
		return false;
	}

	function get_active() {
		$result = $this->db->select('*')
			->from($this->table)
			->where("active", "1")
			->get();
		if(!empty( $result->result())) {
			return  $result->result()[0];
		} else {
			return null;
		}
	}

	public function clear_active(){
		$this->db->update($this->table, array('active' => '0'));
	}
	public function delete_periode($entry)
	{
		$this->db->where('id',$entry);
		$this->db->delete($this->table);
	}
}
