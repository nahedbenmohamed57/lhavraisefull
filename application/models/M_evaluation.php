<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_evaluation extends CI_Model{

    public $table = "evaluation";

    public function __construct(){
        parent::__construct();
    }

    public function add_evaluation($data){
        $this->db->insert('evaluation',$data);
    }
    public function update_evaluation($evaluation_id, $data){
        $this->db->where('evaluation_id',$evaluation_id);
		$this->db->set($data);
		$this->db->update('evaluation');
    }

    public function get_evaluation($user_id,$device_id){
        $this->db->select('*');
		$this->db->from('evaluation');
        $this->db->where('user_id',$user_id); 
        $this->db->where('device_id',$device_id); 
		$query = $this->db->get();
		return($query->result()); 
    }
    public function get_evaluation2($user_id,$device_id,$id_p){
        $this->db->select('*');
        $this->db->from('evaluation');
        $this->db->where('user_id',$user_id); 
        $this->db->where('id_perimetre  ',$id_p); 
        $this->db->where('device_id',$device_id); 
        $query = $this->db->get();
        return($query->result()); 
    }
    public function get_result_evaluation(){
        $this->db->select('*');
		$this->db->from('evaluation');
		$this->db->where('user_id',$_SESSION['user_id']); 
		$query = $this->db->get();
		return($query->result()); 
    }
     public function get_result_evaluation2($id_per){
        $this->db->select('*');
        $this->db->from('evaluation');
        $this->db->where('user_id',$_SESSION['user_id']); 
        $this->db->where('id_perimetre  ',$id_per); 
        $query = $this->db->get();
        return($query->result()); 
    }

    public function get_evaluation_cotation($user_id,$device_id){

        $find = $this->get_evaluation($user_id,$device_id); 
        if(!empty($find)){
            $this->db->select('evaluation.device_id,listing.listing_id,listing.listing_name,evaluation.avancement, ');
            $this->db->from('evaluation');
            $this->db->where('user_id',$user_id); 
            $this->db->where('device_id',$device_id); 
            $this->db->join('listing','listing.listing_id = evaluation.listing_id' ); 
            $query = $this->db->get();
            return($query->result()[0]); 
        }else {
            return false ; 
        }
    }
    public function get_evaluation_cotation2($user_id,$device_id,$id_per,$id_pole = NULL){

        $find = $this->get_evaluation($user_id,$device_id); 
        if(!empty($find)){
            $this->db->select('evaluation.device_id,listing.listing_id,listing.listing_name,evaluation.avancement, ');
            if ($id_per != 'all') {
                $this->db->from('evaluation');
                $this->db->where('id_perimetre',$id_per);
            } else if ($id_pole && $id_pole != 'all') {
                $this->db->from('perimetre, evaluation');
                $this->db->where('perimetre.pole_id', $id_pole);
                $this->db->where('perimetre.id_perimetre = evaluation.id_perimetre');
            }
            // $this->db->where('user_id',$user_id); 
            $this->db->where('device_id',$device_id); 
            $this->db->join('listing','listing.listing_id = evaluation.listing_id' ); 
            $query = $this->db->get();

            return($query->result()[0]); 
        }else {
            return false ; 
        }
    }
}

