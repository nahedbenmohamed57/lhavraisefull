<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_modules extends CI_Model{

    public $table = "modules";

    public function __construct(){
        parent::__construct();
    }
	
	public function get_all_modules(){
        return $this->db->select('*')
            ->from($this->table)
			->order_by("sort", "asc")
            ->get()
            ->result();
    }
}