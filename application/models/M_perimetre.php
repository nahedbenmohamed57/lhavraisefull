<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_perimetre extends CI_Model{

    public $table = "establishment";

    public function __construct(){
        parent::__construct();
    }
    public function get_perimetre2(){
        return $this->db->select('*')
            ->from($this->table)
            ->order_by("pole_id ", "asc")
            ->get()
            ->result();
    }
	
	public function get_perimetre($id_p){
        return $this->db->select('*')
            ->from($this->table)
            ->order_by("pole_id ", "asc")
            ->where("pole_id",$id_p)
            ->get()
            ->result();
    }
    public function get_perimetre_ById($id_per){
        return $this->db->select('*')
            ->from($this->table)
            ->where("id_perimetre",$id_per)
            ->get()
            ->result()[0];
    }
     public function get_perimetre_ById2($id_per){
        return $this->db->select('*')
            ->from($this->table)
            ->where("id_perimetre",$id_per)
            ->get()
            ->result();
    }
    
     public function add_perimetre($entry,$id_p){
        $entry['pole_id']=$id_p;
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }
    public function update_perimetre($id, $data)
    {
        $this->db->where('id_perimetre',$id);
        $this->db->set($data);
        $this->db->update($this->table);
    }
    public function delete_perimetre($per_id)
    {
        $this->db->where('id_perimetre',$per_id);
        $this->db->delete($this->table);
    }
    //this function get list of pôte and foreach pole get list of establishment and foreach establishment ge list of unit
    public function get_deatil_perimetre() {
        $pole = $this->db->select('*')
            ->from('pole')
            ->get()
            ->result();
       
        if(!empty($pole)) {

            foreach ($pole as $key => $value) {
                $establishment = $this->db->select('*')
                                ->from($this->table)
                                ->where("pole_id",$value->id_pole)
                                ->get()
                                ->result();
               $value->Eteab = new stdclass();
               if(!empty($establishment)) {
                    $value->Eteab = $establishment;
                    foreach ($establishment as $k => $v) {
                        $units = $this->db->select('*')
                                ->from('unite')
                                ->where("perimetre_id",$v->id_perimetre)
                                ->get()
                                ->result();
                        $v->units = new stdclass();
                        if(!empty($units))   {
                             $v->units = $units;
                        }     
                       
                    }
               }
            }
        }
        return $pole;
    }

    public function get_units($id_perimetre){
        return $this->db->select('*')
            ->from('unite')
            ->order_by("id_unite ", "asc")
            ->where("perimetre_id",$id_perimetre)
            ->get()
            ->result();
    }
}