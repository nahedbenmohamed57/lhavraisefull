<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultat extends CI_Controller {

    public function __construct() {
    	error_reporting(-1);
		ini_set('display_errors', 1);
        parent::__construct();
        $this->load->model('m_family');
        $this->load->model('m_family_rps');
        $this->load->model('m_facteur');
		$this->load->model('m_determinant');
		$this->load->model('m_evaluation');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_perimetre');
		$this->load->model('m_pole');
		$this->load->model('m_rps_evaluation');
		$this->load->model('m_periode');
		$this->load->model('m_deviceHasListing');
		$this->load->model('m_unite');
		$this->load->library('MY_Form_validation');
    }

    public function index() {
		$data['active'] = "8";
		$data["families"] = $this->m_family->get_families();
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		//redirect('resultat/show/1');
 		$this->load->render('resultat/index', $data);
	}

   /* public function general() {
		$data["families"] = $this->m_family->get_families();
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		redirect('resultat/show/1');
 		//$this->load->render('resultat/show/1', $data);
	}*/

	public function show($focus) {
		$data['active'] = "8";
		$family_rps = $this->m_family_rps->get_families();

		foreach ($family_rps as $f) {
			$f->facteur = $this->m_facteur->get_facteurs_ByFamily($f->family_id);
		}

		$data["family_rps"] = $family_rps;
        $data["periodes"]   = $this->m_periode->get();

		$data["focus"] = $focus;
		if($focus == 2) {
			$data["poles"] = $this->m_pole->get_pole();
			//get primietre by pôle
		}
		if($focus == 3) { 
			$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		}
		if($focus == 4) { 
			$data['units']      = $this->m_unite->get_unite2();
		}
		$this->load->render('resultat/resultat', $data);
	}

	public function data($focus = '',$pole_id = null, $etab_id = null, $unit = null, $periode = null) {
		$data['active'] = "8";
		$family_rps = $this->m_family_rps->get_families();

		foreach ($family_rps as $f) {
			$f->facteur = $this->m_facteur->get_facteurs_ByFamily($f->family_id);
		}
		$data["family_rps"] = $family_rps;
		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		$data['active_periode_id'] = $active_periode_id;

		if($focus == 1) {
	        $data["poles"]     = $this->m_pole->get_pole();
			foreach ($data["poles"] as $key => $p) {
				//get evaluation by pole for the last periode
				$last_eval = $this->m_rps_evaluation->get_eval_by_pole($p->id_pole, $active_periode_id);
				$p->evals = [];
				if(!empty($last_eval)) {
					foreach ($last_eval as $k => $eval) {
						//get detail evaluation
						if(isset($eval->id_rps_evaluation)) {
							$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
							$p->evals = $last_eval;
							if(!empty($last_eval )) {
								foreach ($last_eval as $i => $value) {
									$value->impact = '';
									$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);

									if( !empty($impact)) {
										$value->impact = floatval($impact[0]->moy)*1.25;
									}
								}
							}
						}
					}
				}

			}
			
			$this->load->view('resultat/data', $data);
		} elseif($focus == 2) {
			if($pole_id > 0) {
				//get list of etab for the selected pole
				$data["perimetres"]     = $this->m_perimetre->get_perimetre($pole_id);
				foreach ($data["perimetres"] as $key => $p) {
					//get evaluation by pole for the last periode
					$last_evals = $this->m_rps_evaluation->get_eval_by_etablissement($p->id_perimetre, $active_periode_id);
					$p->evals = $last_evals;
					foreach ($p->evals as $k => $eval) {
						//get detail evaluation
						$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
						
						$eval->last_eval = $last_eval;
						//var_dump($p->evals);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $value) {
								$value->impact = '';
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								
								if( !empty($impact)) {
									$value->impact = floatval($impact[0]->moy)*1.25;
								} 	
							}
						}	
					}
				}
				$this->load->view('resultat/data_pole', $data);
			}
		} elseif($focus == 3) {
			if($etab_id > 0) {
				//get list of units for the selected pole
				$data["units"]     = $this->m_unite->get_unite($etab_id);
				foreach ($data["units"] as $key => $p) {
					//get evaluation by pole for the last periode
					$last_evals = $this->m_rps_evaluation->get_eval_by_unit($p->id_unite, $active_periode_id);
					$p->evals = $last_evals;
					foreach ($p->evals as $k => $eval) {
						//get detail evaluation
						$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
						
						$eval->last_eval = $last_eval;
						//var_dump($p->evals);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $value) {
								$value->impact = '';
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								
								if( !empty($impact)) {
									$value->impact = floatval($impact[0]->moy)*1.25;
								} 	
							}
						}	
					}
				}
				$this->load->view('resultat/data_perimetres', $data);
			}
		} else if($focus == 4) {
			if($unit > 0) {

				//get list of units for the selected pole
				$data["units"]     = $this->m_unite->get_unite_ById2($unit);
				foreach ($data["units"] as $key => $p) {
					//get evaluation by pole for the last periode
					$last_evals = $this->m_rps_evaluation->get_eval_by_unit($p->id_unite, $active_periode_id);
					$p->evals = $last_evals;
					foreach ($p->evals as $k => $eval) {
						//get detail evaluation
						$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);

						$eval->last_eval = $last_eval;
						//var_dump($p->evals);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $value) {
								$value->impact = '';
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);

								if( !empty($impact)) {
									$value->impact = floatval($impact[0]->moy)*1.25;
								}
							}
						}
					}
				}
				$this->load->view('resultat/data_perimetres', $data);
			}

		}
	}

	public function unit_result($pole_id= null, $perimetre_id = null, $unit_id = null, $periode = null )
	{
		$data['active'] = "8";
		$data["poles"]        = $this->m_pole->get_pole();
		$data["perimetres"]   = $this->m_perimetre->get_perimetre($pole_id);
		$data['units']        = $this->m_perimetre->get_units($perimetre_id);
		//get rps evaluation with the spacific pole , perimetre and unitif we found it we do an edit not a new isert

		$data['pole_id']      = $pole_id;
		$data['perimetre_id'] = $perimetre_id;
		$data['unit_id']      = $unit_id;

		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		

		$data["families_rps"] = $this->m_family_rps->get_families(); 
		$data["facteurs"] = $this->m_facteur->get_facteurs();
		if(!empty($data["facteurs"])) {
			foreach ($data["facteurs"] as $key => $fact) {
				$fact->impact = 0;
				$impact = $this->m_facteur->get_moy_indicator_by_factor($fact->facteur_id);
				if( !empty($impact)) {
					$fact->impact = round(floatval($impact[0]->moy)*1.25,2);
				} 
				$allFacteors = $this->m_facteur->get_indicator_by_factor($fact->facteur_id);
				$fact->fiabilite =  0;
				if(!empty($allFacteors)) {
					$hasTrend = 0;
					foreach ($allFacteors as $key => $fa) {
						if($fa->trend_index > 0) {
							$hasTrend++;
						}
					}
					if($hasTrend>0) {
						$fact->fiabilite = round(sizeof($allFacteors)/$hasTrend,2);
					}
				}
			}
		}

        $evaluation_rps = $this->m_rps_evaluation->get_evaluation_by_periode($pole_id, $perimetre_id, $unit_id, $active_periode_id);
        $data['evaluation_qantitive'] = array();
        if(!empty($evaluation_rps)) {
        	$evaluation_qantitive = $this->m_rps_evaluation->get_evaluation_by_ref_eval($evaluation_rps->id_rps_evaluation);
        	
        	$data['evaluation_qantitive'] = $evaluation_qantitive;
        }
 		$this->load->render('resultat/result_unit', $data);
	}

	/** view resultat global**/
	public  function generalResult($value='')
	{
		$family_rps = $this->m_family_rps->get_families();
        $data["poles"] = $this->m_pole->get_pole();
		$data["family_rps"] = $family_rps;
        $data["periodes"]   = $this->m_periode->get();
        $periode_open       = $this->m_periode->get_active();
        $active_periode_id = 0;
        if(!empty($periode_open)) {
            $active_periode_id = $periode_open->id;
        }
        $data['active_periode_id'] = $active_periode_id;
        $data['closeMenu'] = true;
		$this->load->render('resultat/general_result', $data);
	}

	/** resultat globale **/
	public  function getResultPeriod($periode="", $poleId="", $familyId="")
	{
		$result = array('factor' => [], 'result' => []);
		$periode  = $_POST['period'];
		$poleId   = $_POST['poleId'];
		$familyId = $_POST['familyId'];
		$familyFacteur = $this->m_facteur->get_facteurs_ByFamily($familyId);
		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		if(!empty($familyFacteur)) {
			foreach ($familyFacteur as $key => $value) {
				array_push($result['factor'], $value->facteur_name);
				$impactValue = 0;
				$last_evals = $this->m_rps_evaluation->get_eval_by_pole($poleId, $active_periode_id);

				if(!empty($last_evals)) {
					//get detail evaluation
					if(isset($last_evals->id_rps_evaluation)) {
			
						$last_eval = $this->m_rps_evaluation->get_details_eval($last_evals->id_rps_evaluation);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $v) {
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								if( !empty($impact)) {
									$impactValue = round(floatval($impact[0]->moy)*1.25,2);
								}
							}
						}
					}
				}
				array_push($result['result'], $impactValue);
			}
		}
	   echo json_encode($result);
	}
	/** view resultat par etablisseme**/
	public  function etabResult($value='')
	{
		$family_rps = $this->m_family_rps->get_families();
        $data["poles"] = $this->m_pole->get_pole();
        $data["perimetres"]   =  $this->m_perimetre->get_perimetre2();
		///$data['units']        = $this->m_perimetre->get_units($perimetre_id);
	

		$data["family_rps"] = $family_rps;
        $data["periodes"]   = $this->m_periode->get();
        $periode_open       = $this->m_periode->get_active();
        $active_periode_id = 0;
        if(!empty($periode_open)) {
            $active_periode_id = $periode_open->id;
        }
        $data['active_periode_id'] = $active_periode_id;
        $data['closeMenu'] = true;
		$this->load->render('resultat/etabResult', $data);
	}

	/** resultat globale par etablissement **/
	public  function getResultPeriodEtab($periode="", $poleId="", $familyId="")
	{
		$result = array('factor' => [], 'result' => []);
		$periode  = $_POST['period'];
		$poleId   = $_POST['poleId'];
		$familyId = $_POST['familyId'];
		$id_perimetre = $_POST['id_perimetre'];
		$familyFacteur = $this->m_facteur->get_facteurs_ByFamily($familyId);
		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		if(!empty($familyFacteur)) {
			foreach ($familyFacteur as $key => $value) {
				array_push($result['factor'], $value->facteur_name);
				$impactValue = 0;
				$last_evals = $this->m_rps_evaluation->get_eval_by_etablissement($id_perimetre, $active_periode_id);
				//get detail evaluation
				if(!empty($last_evals)) {
					//get detail evaluation
					if(isset($last_evals[0]->id_rps_evaluation)) {
						$last_eval = $this->m_rps_evaluation->get_details_eval($last_evals[0]->id_rps_evaluation);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $v) {
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								if( !empty($impact)) {
									$impactValue = round(floatval($impact[0]->moy)*1.25,2);
								} 	
							}
						}	
					}
				}
				
				
				array_push($result['result'], $impactValue);
			}
		}
	   echo json_encode($result);
	}

	/** view resultat par unite**/
	public  function unitResult($value='')
	{
		$family_rps = $this->m_family_rps->get_families();
        $data["poles"] = $this->m_pole->get_pole();
		$data["family_rps"] = $family_rps;
        $data["periodes"]   = $this->m_periode->get();
        $periode_open       = $this->m_periode->get_active();
        $active_periode_id = 0;
        if(!empty($periode_open)) {
            $active_periode_id = $periode_open->id;
        }
        $data['active_periode_id'] = $active_periode_id;
        $data['closeMenu'] = true;
		$this->load->render('resultat/unitResult', $data);
	}

	/** resultat globale par etablissement **/
	public  function getResultPeriodUnit($periode="", $poleId="", $familyId="")
	{
		$result = array('factor' => [], 'result' => []);
		$periode  = $_POST['period'];
		$poleId   = $_POST['poleId'];
		$familyId = $_POST['familyId'];
		$id_perimetre = $_POST['id_perimetre'];
		$unit = $_POST['unitId'];
		$familyFacteur = $this->m_facteur->get_facteurs_ByFamily($familyId);
		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		if(!empty($familyFacteur)) {
			foreach ($familyFacteur as $key => $value) {
				array_push($result['factor'], $value->facteur_name);
				$impactValue = 0;
				//get evaluation by pole for the last periode
				$last_evals = $this->m_rps_evaluation->get_eval_by_unit($unit, $active_periode_id);
				//get detail evaluation
				if(!empty($last_evals)) {
					if(isset($last_evals[0]->id_rps_evaluation)) {
						$last_eval = $this->m_rps_evaluation->get_details_eval($last_evals[0]->id_rps_evaluation);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $v) {
						
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);

								if( !empty($impact)) {
									$impactValue = round(floatval($impact[0]->moy)*1.25,2);
								}
							}
						}
					}
				}
				array_push($result['result'], $impactValue);
			}
		}
	   echo json_encode($result);
	}

	public  function globalResultAllPeriod($value='')
	{
		$family_rps = $this->m_family_rps->get_families();
		$data["poles"] = $this->m_pole->get_pole();
		$data["family_rps"] = $family_rps;
		$data["periodes"]   = $this->m_periode->get();
		$periode_open       = $this->m_periode->get_active();
		$active_periode_id = 0;
		if(!empty($periode_open)) {
			$active_periode_id = $periode_open->id;
		}
		$data['active_periode_id'] = $active_periode_id;
		$data['closeMenu'] = true;
		$this->load->render('resultat/general_result_all_period', $data);
	}
	/**
	 * get all period
	 */
	public  function getAllPeriod()
	{
		$periodes  = $this->m_periode->get();
		echo json_encode($periodes);
	}

	public  function getGlobalResultForAllPeriod($per = null)
	{
		$result = array();
		$per  = $_POST['perimetre'];
		$periodes  = $this->m_periode->get();
		$units    = $this->m_unite->get_unite($per);
		foreach ($units as $unit) {
			$result[$unit->unite_nom] = [];
			foreach ($periodes as $period) {
				$impactValue = 0;
				//get evaluation by pole for the last periode
				$last_evals = $this->m_rps_evaluation->get_eval_by_unit($unit->id_unite, $period->id);
				//get detail evaluation
				if(!empty($last_evals)) {
					if(isset($last_evals[0]->id_rps_evaluation)) {
						$last_eval = $this->m_rps_evaluation->get_details_eval($last_evals[0]->id_rps_evaluation);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $v) {

								$impact = $this->m_facteur->get_moy_indicator_by_factor($per);

								if( !empty($impact)) {
									$impactValue = round(floatval($impact[0]->moy)*1.25,2);
								}
							}
						}
					}
				}
				array_push($result[$unit->unite_nom], $impactValue);
			}
		}
		$result['periods'] = $periodes;
		echo json_encode($result);
	}
}
