<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rps_evaluation extends CI_Controller {

    public function __construct()
	{
		error_reporting(-1);
		ini_set('display_errors', 1);
		
        parent::__construct();
        $this->load->model('m_family');
		$this->load->model('m_determinant');
		$this->load->model('m_evaluation');
		$this->load->model('m_periode');
		$this->load->model('m_perimetre');
		$this->load->model('m_pole');
		$this->load->model('m_family_rps');
		$this->load->model('m_rps_evaluation');
		$this->load->library('MY_Form_validation');
    }
    public function index()
	{
		$data['active'] = "7";
		$data['periodes'] =  $this->m_periode->get_active();
		$data["poles"]    = $this->m_pole->get_pole();

		if(!empty($_POST)) {
			//add a new evaluation params
			if(!empty($_POST['pole_id']) && !empty($_POST['establishment_id']) && !empty($_POST['unit_id'])) {
				 $this->m_rps_evaluation->add_evaluation_param($_POST);
				 Redirect("rps_evaluation/quantitative_evaluation/".$_POST['pole_id']."/".$_POST['establishment_id']."/".$_POST['unit_id']."/");
			}
		}
		
 		$this->load->render('evaluation/rps_evaluation', $data);
	}

	public function quantitative_evaluation($pole_id= null, $perimetre_id = null, $unit_id = null )
	{
		$data['active'] = "7";
		$data["poles"]        = $this->m_pole->get_pole();
		$data["perimetres"]   = $this->m_perimetre->get_perimetre($pole_id);
		$data['units']        = $this->m_perimetre->get_units($perimetre_id);
		//get rps evaluation with the spacific pole , perimetre and unitif we found it we do an edit not a new isert

		$data['pole_id']      = $pole_id;
		$data['perimetre_id'] = $perimetre_id;
		$data['unit_id']      = $unit_id;

		$data["families_rps"] = $this->m_family_rps->get_families(); 
		$data["facteurs"] = $this->m_facteur->get_facteurs();
		if(!empty($data["facteurs"])) {
			foreach ($data["facteurs"] as $key => $fact) {
				$fact->impact = 0;
				$impact = $this->m_facteur->get_moy_indicator_by_factor($fact->facteur_id);
				if( !empty($impact)) {
					//old
					//$fact->impact = floatval($impact[0]->moy)*1.25;
					$fact->impact = floatval($impact[0]->moy);
				} 
				$allFacteors = $this->m_facteur->get_indicator_by_factor($fact->facteur_id);
				$fact->fiabilite =  0;
				if(!empty($allFacteors)) {
					$hasTrend = 0;
					foreach ($allFacteors as $key => $fa) {
						if($fa->trend_index > 0) {
							$hasTrend++;
						}
					}
					if($hasTrend>0) {
						$fact->fiabilite = sizeof($allFacteors)/$hasTrend;
					}
				}
			}
		}
        $evaluation_rps = $this->m_rps_evaluation->get_last_evaluation($pole_id, $perimetre_id, $unit_id);
        $data['evaluation_qantitive'] = [];
        if(!empty($evaluation_rps)) {
        	$evaluation_qantitive = $this->m_rps_evaluation->get_evaluation_by_ref_eval($evaluation_rps->id_rps_evaluation);
        	
        	$data['evaluation_qantitive'] = $evaluation_qantitive;
        }
		if(!empty($_POST)) {
			//save data on table
			//get the last evalution rps
			if(!empty($evaluation_rps)) {
				$mydata  = $_POST;
				$newdata =array();
				$i       = 0;
				$details    = [];
				
				foreach ($mydata as $key => $info) {
					$info_detail = explode('_', $key);
				    array_push($details, $info);

	                if ($i == 7) {
	                		array_push($newdata, array('rps_evaluation_id'=> $evaluation_rps->id_rps_evaluation,'facteur_id' =>$info_detail[1], 'description' =>$details[0], 'intensity' => $details[1], 'measures_concerned' =>$details[2],'measures_recommended'=> $details[3],'contributors' =>$details[4],'steps' =>$details[5],'human_resources' =>$details[6],'indicator' =>$details[7]));
	                	$i = -1;
	                	$details = array();
	                }
	                $i++;
			    }
			    if(!empty($evaluation_rps )) {
			    	//delete existing values
			    	 $this->m_rps_evaluation->delete_iquantitav_eval($evaluation_rps->id_rps_evaluation);
			    }

			    $this->m_rps_evaluation->insert_iquantitav_eval($newdata);
			    
				Redirect('rps_evaluation/quantitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id);
			}
		}
 		$this->load->render('family_rps/quantitative', $data);
	}

    public function qualitative_evaluation($pole_id=null, $perimetre_id=null, $unit_id = null, $rps_evaluation_id = null) {
		$data['active'] = "7";
    	//$data["families"] = $this->m_family->get_families();
		$data["poles"]      = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		$data['pole_id']      = $pole_id;
		$data['perimetre_id'] = $perimetre_id;
		$data['unit_id']      = $unit_id;
		$data["perimetres"]   = $this->m_perimetre->get_perimetre($pole_id);
		$data['units']        = $this->m_perimetre->get_units($perimetre_id);

		if($rps_evaluation_id == null) {
			 $evaluation_rps = $this->m_rps_evaluation->get_last_evaluation($pole_id, $perimetre_id, $unit_id);
			 if(!empty($evaluation_rps)) {
			 	$rps_evaluation_id = $evaluation_rps->id_rps_evaluation;
			 }
        	
		}
		if(!$rps_evaluation_id) {
			//si pas d'évaluation on retourne vers la page d'évaluation
			//redirect('/rps_evaluation');
		}
		$data['rps_evaluation_id'] = $rps_evaluation_id;
		//get the list of priority_risks
		$priority_risks = $this->m_rps_evaluation->get_priority_risks($rps_evaluation_id);
		
		if(!empty($priority_risks)) {
			//get list of working_situations foreach risks
			foreach ($priority_risks as $key => $pr) {
				$pr->working_situations = $this->m_rps_evaluation->get_working_situations($pr->id_priority_risks);
				if(!empty($pr->working_situations)) {
					foreach ($pr->working_situations as $k=> $situations) {
						$situations->pistes = $this->m_rps_evaluation->get_work_areas_by_situation($situations->id_working_situations);
					}
				}
			}
		}
		$data['priority_risks']     = $priority_risks;
		
		$this->load->render('evaluation/qualitative_eval',$data);
	}
	public function crud_priority_risks ($pole_id=null, $perimetre_id=null, $unit_id = null, $rps_evaluation_id = null, $action = null, $id_priority_risks = null) {
		$data['active'] = "7";
		$data['rps_evaluation_id'] = $rps_evaluation_id;
		$data['pole_id']      = $pole_id;
		$data['perimetre_id'] = $perimetre_id;
		$data['unit_id']      = $unit_id;

		if($action == "add") {
			//add a new priority_risks to the current evaluation;
			if(!empty($_POST)) {
							$myData = $_POST;
				$name_priority_risks = $myData['name_priority_risks'];
				$this->m_rps_evaluation->add_priority_risks($name_priority_risks,$rps_evaluation_id);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);
			}
		} elseif($action == "update") {
			$data['priority_risks'] = $this->m_rps_evaluation->get_priority_risks_by_id($id_priority_risks);
			if(!empty($_POST)) {
				//update priority_risks
                $this->m_rps_evaluation->update_priority_risks($_POST);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);
			}
		}

		$this->load->view('evaluation/_add_priority_risks',$data);
	}

	public function crud_working_situations ($pole_id=null, $perimetre_id=null, $unit_id = null, $rps_evaluation_id = null, $action = null, $id_working_situations) {
		$data['active'] = "7";
		$data['rps_evaluation_id'] = $rps_evaluation_id;
		$data['pole_id']           = $pole_id;
		$data['perimetre_id']      = $perimetre_id;
		$data['unit_id']           = $unit_id;
		$data['priority_risks']    = $this->m_rps_evaluation->get_priority_risks($rps_evaluation_id);

		if($action == "add") {
			//add a new priority_risks to the current evaluation;
			if(!empty($_POST)) {				
				$myData = $_POST;
				$name_priority_risks = $myData['name_working_situations'];
		
				$this->m_rps_evaluation->add_working_situations($myData);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);
			}
		}elseif($action == "update") {
			$data['working_situations'] = $this->m_rps_evaluation->get_working_situations_by_id($id_working_situations);
			if(!empty($_POST)) {
				//update working_situations
                $this->m_rps_evaluation->update_working_situations($_POST);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);
			}
		}
		
		$this->load->view('evaluation/_working_situations',$data);
	}

	public function crud_work_areas ($pole_id=null, $perimetre_id=null, $unit_id = null, $rps_evaluation_id = null, $action = null, $id_work_areas= null) {
		$data['active'] = "7";
		$data['rps_evaluation_id'] = $rps_evaluation_id;
		$data['pole_id']           = $pole_id;
		$data['perimetre_id']      = $perimetre_id;
		$data['unit_id']           = $unit_id;
		$data['priority_risks']    = $this->m_rps_evaluation->get_priority_risks($rps_evaluation_id);

		$data['working_situations'] = $this->m_rps_evaluation->get_working_situations_by_eval($rps_evaluation_id);
		
		if($action == "add") {
			//add a new work areas to the current evaluation;
			if(!empty($_POST)) {				
				$myData = $_POST;
				$this->m_rps_evaluation->add_working_areas($myData);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);	
			}
		}elseif($action == "update") {
			$data['work_areas'] = $this->m_rps_evaluation->get_work_areas_by_id($id_work_areas);
			if(!empty($_POST)) {
				//update work_areas
                $this->m_rps_evaluation->update_work_areas($_POST);
				redirect('/rps_evaluation/qualitative_evaluation/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id);
			}
		}
		
		$this->load->view('evaluation/_work_areas',$data);
	}
}
?>
