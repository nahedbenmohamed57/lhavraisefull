<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_role');
		$this->load->model('m_periode');
		$this->load->model('m_pole');
		$this->load->model('m_perimetre');
		$this->load->model('m_unite');
		$this->load->model('m_rps_evaluation');
    }

    public function index()
	{
		$data['active'] = "1";
        $data["user"] = $this->m_user->get_user_ById($_SESSION['user_id']); 
        $data["user"]->user_role= $this->m_role->get_role( $data["user"]->user_role);
        $data["title"]=  "Bienvenue " .$data["user"]->user_firstname;
		$data["config"] = $this->m_user->get_config();
		$data["activePeriod"] = $this->m_periode->get_active();
		$data["nbPole"] = count($this->m_pole->get_pole());
		$data["nbPerimetre"] = count($this->m_perimetre->get_perimetre2());
		$data["nbUnit"] = count($this->m_unite->get_unite2());
		$periode_open       = $this->m_periode->get_active();
		$active_periode_id = 0;
		if(!empty($periode_open)) {
			$active_periode_id = $periode_open->id;
		}

		$nbEvaluation = $this->m_rps_evaluation->get_all_evaluation($active_periode_id);
		$data["result"] = 0;
		$data["nbEval"] = $nbEvaluation->nbEval;
		if(!empty($nbEvaluation) && !empty($data["nbUnit"])) {
			$data["result"] = round($nbEvaluation->nbEval/$data["nbUnit"] *100,0);
		}
		$this->load->render('dashboard',$data);
    }

    public function updatePhoto(){
		$target_dir = "template/img/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$fileName = $_FILES["fileToUpload"]["name"];
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists($target_file)) {
			$fileName  = rand(5,2).''.$_FILES["fileToUpload"]["name"];
			//echo "Sorry, file already exists.";
			$uploadOk = 1;
		}

		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 500000) {
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		}

		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}

		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
			$target_file = $target_dir . $fileName;
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
				//save file on database
				$this->m_user->set_image_config($fileName);
				$this->session->set_userdata(array(
					"image_profile" => $fileName
				));
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}
		redirect("/dashboard");
	}

	public function updateConfig() {
    	if(!empty($_POST)){
    		$newConfig = array(
    			'name_platforme' => $_POST['name_platforme'],
				'pole_name' => $_POST['pole_name'],
				'etab_name' => $_POST['etab_name'],
				'unit_name' => $_POST['unit_name'],
				);
			$this->m_user->set_config($newConfig);
		}
		$config = $this->m_user->get_config();
		$this->session->set_userdata(array(
			"config" => $config
		));
		redirect("/dashboard");
	}

	public function sendMail() {
    	if(isset($_POST)) {
			$role = 2;
    		if($_POST['role'] == 2) {
    			$role = 3;
			}
			$listAdmin = $this->m_user->getAdmins($role);
			if(!empty($listAdmin)) {
				foreach ($listAdmin as $admin) {
					$to_email = $admin->user_email;
					$subject  = $_POST['subject'];
					$message  = 'Bonjour, '.$admin->user_firstname.' '.$admin->user_lastname.'
					 '.$_POST['content'];
					$headers  = 'From: nahednenmohamed57@gmail.com';
					mail($to_email,$subject,$message,$headers);
				}
			}
		}
		redirect("/dashboard");
	}
}
