<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_family');
		$this->load->model('m_determinant');
		$this->load->model('m_evaluation');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_perimetre');
		$this->load->model('m_pole');
		$this->load->model('m_listing');
		$this->load->model('m_evaluation');
		$this->load->model('m_deviceHasListing');
		$this->load->library('MY_Form_validation');
    }
    public function index()
	{
		$data["families"] = $this->m_family->get_families(); 
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
 		$this->load->render('evaluation/index', $data);
	}
	public function merci() {
		$this->load->render('evaluation/merci');
	}
	public function evaluate_family($family_id)
	{
		$data["family"] = $this->m_family->get_family($family_id); 
		$data['evaluation'] = $this->m_evaluation->get_result_evaluation();
		$data["families"] = $this->m_family->get_families(); 
		$data["familyQCMs"] = $this->m_family->get_families_ToQCM($family_id); 
 		$this->load->render('evaluation/evaluation', $data);
	}
	public function evaluate_family2($family_id,$id_pole,$id_per)
	{
		$data["family"] = $this->m_family->get_family($family_id); 
		$data['evaluation'] = $this->m_evaluation->get_result_evaluation2($id_per);
		$data["pole"]= $this->m_pole->get_pole_ById($id_pole);
		$data["perimetre"]=$this->m_perimetre->get_perimetre_ById2($id_per);
		$data["nb_devices"]=$this->m_device->get_all_devices();
		$data["families"] = $this->m_family->get_families(); 
		$data["familyQCMs"] = $this->m_family->get_families_ToQCM($family_id); 
		$data["determinants"] = $this->m_determinant->get_determinants_ByFamily($family_id);
		
 		$this->load->render('evaluation/evaluation', $data);
	}
	public function save_evaluation($id_f,$id_p,$id_per)
	{
		$selections = explode(',',$_POST["selections"]);
		$nb=0;
		while ($nb<sizeof($selections))
		{
			$str=$selections[$nb];
			$test=str_split($str, 2);	
			$data["device_id"] =$test[0] ;
			$data["user_id"] = $_SESSION['user_id'];
			$data["listing_id"] = $test[1];
			
			$data["id_perimetre"] = $id_per;
			$number = $this->m_deviceHasListing->get_NumberListing($data["device_id"]);
			$number = (get_object_vars($number)["count(listing_id)"]);
			$x=$this->m_listing->get($data["listing_id"]) ;
			$score = $x[0]->listing_score;
			$data["avancement"]= (100/$number) * $score; 
			$evaluation = $this->m_evaluation->get_evaluation2($data["user_id"] ,$data["device_id"],$id_per);
			if(sizeof($evaluation) == 0){
				$this->m_evaluation->add_evaluation($data);
			}else {
				$this->m_evaluation->update_evaluation($evaluation[0]->evaluation_id,$data);
			}
			$nb++;
		}
		/*
		foreach($selections as $selection){
			$selection = explode('.',$selection);
			$str=$selection;
			$test=str_split($str, 2);
			$data["device_id"] =$test[0] ;
			$data["user_id"] = $_SESSION['user_id'];
			$data["listing_id"] = $test[1];
			$number = $this->m_deviceHasListing->get_NumberListing($data["device_id"]);
			$number = (get_object_vars($number)["count(listing_id)"]);
			$score = $this->m_listing->get($data["listing_id"])->listing_score;
			$data["avancement"]= (100/$number) * $score; 
			$evaluation = $this->m_evaluation->get_evaluation($data["user_id"] ,$data["device_id"]);
			if(sizeof($evaluation) == 0){
				$this->m_evaluation->add_evaluation($data);
			}else {
				$this->m_evaluation->update_evaluation($evaluation[0]->evaluation_id,$data);
			}
		}
		*/
		$data["id_pole"] = $id_p;
		$fam=$data["families"] = $this->m_family->get_families(); 
		$id_fw=$fam[0]->family_id;
		$nb_id=0;
		while ($id_fw!=$id_f) 
		{
			$nb_id++;
			$id_fw=$fam[$nb_id]->family_id;
		}
		$nb_id++;
		$id_fw;
		if($nb_id<sizeof($data["families"]))
		{	
			$id_fw=$fam[$nb_id]->family_id;
			Redirect("evaluation/evaluate_family2/".$id_fw."/".$id_p."/".$id_per);
		}
		else
		{
			// Redirect("evaluation/evaluate_family2/".$id_fw."/".$id_p."/".$id_per);
			Redirect("evaluation/merci");
		}
		//Redirect("evaluation/evaluate_family/".$id_f); 
	}
	public function export_evaluation($id_per)
	{
		$filename = "evaluation.xlsm" ; 
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreads$preadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.$filename);
		header('Cache-Control: max-age=0');
		ob_clean();
		$this->load->library('PHPExcel', NULL, 'excel');
		$fileName = 'exemple.xlsm';
		$fileType =  PHPExcel_IOFactory::identify($fileName);
		// Read the file
		$objReader = PHPExcel_IOFactory::createReader($fileType);
		$objPHPExcel = $objReader->load($fileName);
		$families = $this->m_family->get_families(); 
		$startfamily = 9; 
		$startDeter = 9 ; 
		$startDevice = 9 ; 
		foreach($families as $family){
			$Deters = $this->m_determinant->get_determinants_ByFamily($family->family_id); 
			$DevicesTotal = $this->m_device->get_device_ByFamily($family->family_id); 
			$nbrDeters = sizeof($Deters); 
			$nbrDevices = sizeof($DevicesTotal); 
			$margeFamily = ($nbrDevices +  $nbrDeters - 1)  ;  
			$endFamily = $startfamily + $margeFamily -  1 ; 
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A".($startfamily), $family->family_name); 
			$objPHPExcel->getActiveSheet()->mergeCells("A".($startfamily).":A".($endFamily));
			//insertion determinant
			foreach($Deters as $Deter){
				$DevicesForDeter = $this->m_device->get_device_ByDeter($Deter->determinant_id);	
				$nbrDevicesForDeter = sizeof($DevicesForDeter); 					
				$endDeter = $startDeter + $nbrDevicesForDeter -1;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("C".($startDeter),$Deter->determinant_name); 
				//importance
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("E".($startDeter),$Deter->determinant_importance); 
				//importance %
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("G".($startDeter),(25 *($Deter->determinant_importance)/100)); 

				//insertion device
				$devicesofDeters = $this->m_device->get_device_ByDeter($Deter->determinant_id); 
				$scoreTotal = 0; 
				$nbrDevice = sizeof($devicesofDeters); 
				$startDevice = $startDeter ; 
				foreach($devicesofDeters as $devicesofDeter){
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("I".($startDevice),$devicesofDeter->device_name); 
					//insertion cotation
					$cotation = $this->m_evaluation->get_evaluation2($_SESSION["user_id"],$devicesofDeter->device_id,$id_per);
					if(sizeof($cotation) != 0){
						$cotation[0]->listing_id= $this->m_listing->get_ById($cotation[0]->listing_id);  
						$score = $cotation[0]->avancement; 
						$scoreTotal = $scoreTotal + $score; 
						$score=$score."%";
						//cotation result
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("J".($startDevice),$cotation[0]->listing_id->listing_name); 
						//avancement
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("L".($startDevice),$score); 
					} else {
						//cotation result
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("J".($startDevice),'pas de choix'); 
						//avancement
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("L".($startDevice),0); 
					}
					$startDevice++; 
				}
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("M".($startDeter),round($scoreTotal/$nbrDevice).'%'); 
				$startDeter = $endDeter +2; 
			}		
			$startfamily = $endFamily + 2;
		}
		// Write the file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
		$filePath = '' . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
		$objWriter->save($filePath);
		readfile($filePath);
		unlink($filePath);
		exit;      
	}
}
?>