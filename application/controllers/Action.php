<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
		 $this->load->model('m_action');
		 $this->load->model('m_user');
		 $this->load->model('m_rps_evaluation');
		 $this->load->model('m_pole');
		 $this->load->library('MY_Form_validation');
    }

   /* public function index()
	{ 
        //$data['files'] = array_slice(scandir('./uploads/'), 2);
		$this->load->render('action', null);
    }*/

	public function index()
	{
		//get all actions for admin
		$data['mesures'] = $this->m_rps_evaluation->getAllMesure();

		$data['active']  = "9";
		$data["actions"] = $this->m_action->get_actions();
		$data["poles"] = $this->m_pole->get_pole();
		$this->load->render('action/all_actions', $data);
	}
	
	

    public function delete($file) {
       // unlink('./uploads/' . $file);
        redirect('/outil');
    }

	public function crud_action($task){
		$data['active'] = "9";
       
		//$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		$data['monprofil'] = 0; 
		if (isset($_GET['success'])) {
            $data['updated'] = TRUE;
		} else {
			$data['updated'] = false;
		}

		if(!empty($_SESSION["user_id"])){
			$data["user"] = $this->m_user->get_user_ById($_SESSION["user_id"]);
		}
		if($task == "delete"){
			$this->m_action->delete_action($action_id); 
			Redirect("action"); 
		}else if($task == "show"){
			$this->load->render('action/action', $data);
		}else  if($task == "update"){
			$data['monprofil'] = 1; 
			if(!empty($_POST)){
				if(isset($_POST["user_password"])){
					$_POST["user_password"] = md5($_POST["user_password"]);
					unset($_POST["password_user_confirm"]); 
				}

				if ($user_id == $_SESSION["user_id"]) {
					$redirect="profil"; 
				} else {
					$redirect="user"; 
				}
				$this->m_user->update_user($user_id, $_POST);
				echo json_encode(array("redirect"=>$redirect,"user"=>$user_id));
			} else {
				$this->load->render('users/user', $data);
			}
		}else {
			if(!empty($_POST)){
				$_POST["user_password"] =  md5($_POST["user_password"]);
				if($this->m_user->add_user($_POST)){
					echo json_encode(array("result"=>1));
				}else{
					echo json_encode(array("result"=>0));
				}

			}else {
				$this->load->render('action/action', $data);
			}
		}
	}

	public function deleteMesure($mesureId) {
		if($mesureId) {
			$this->m_rps_evaluation->delete_mesure_text($mesureId);
		}
	
		Redirect("action"); 
	}

	public function updateMesureStatus($mesureId, $status) {
		if($mesureId) {
			$this->m_rps_evaluation->update_mesure_staus($mesureId , intval($status));
		}

		Redirect("action");
	}
}
