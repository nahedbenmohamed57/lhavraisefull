<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Units extends CI_Controller {

    public function __construct()
	{
		error_reporting(-1);
		ini_set('display_errors', 1);
        parent::__construct();
        $this->load->model('m_pole');
        $this->load->model('m_perimetre');
        $this->load->model('m_role');
         $this->load->model('m_unite');
        $this->load->library('MY_Form_validation');
    }

    public function index()
	{
		$data["perimetres"] = $this->m_unite->get_unite2(); 
		$this->load->render('unite/unites', $data);
    }
    // add a new unit
    public function add_unite ($p_id = null) {
    	$data['pole'] = $this->m_pole->get_pole();
    	$data['p_id'] = $p_id;

		if(!empty($_POST)){
			if(!empty($_POST['perimetre_id']) && !empty($_POST['pole_id'])  && !empty($_POST['unite_nom'])) {
				$p_id = $_POST['pole_id'];
			    $result = $this->m_unite->add_unite($_POST, $p_id); 
			}
			Redirect('perimetre');
		}
		$this->load->render('unite/unites', $data);
    }

    public function perimetres_pole($id_p)
	{
		$data["perimetres"] = $this->m_perimetre->get_perimetre($id_p); 
		$data["p_id"]=$id_p;
		$data["pole"] = $this->m_pole->get_pole_ById($id_p);
		$this->load->render('perimetre/perimetre', $data);
    }
	public function crud_units($task, $unit_id =  null, $perimetre_id = false,$p_id = null){
		
		$data["roles"] = $this->m_role->get_all_roles();
		$data['pole'] = $this->m_pole->get_pole();
		$data["task"] = $task;

		if(!empty($unit_id)){
			$data["unites"] = $this->m_unite->get_unite_ById($unit_id); 
		}

		if($task == "delete"){
			$this->m_unite->delete_unite($unit_id); 
			Redirect('perimetre'); 
		}else if($task == "show"){
			$this->load->render('unite/show_unites', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_unite->update_unite($unit_id, $_POST); 
				Redirect('perimetre'); 
			}else {
				$data["perimetres"] = $this->m_perimetre->get_perimetre($data["unites"]->pole_id); 
				
				$this->load->render('unite/show_unites', $data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				$this->m_unite->add_unite($_POST,$p_id); 
				Redirect('unite/perimetres_pole/'.$p_id); 
			}else {
				$this->load->render('unite/show_unites', $data);
			}
		}
	}
}
