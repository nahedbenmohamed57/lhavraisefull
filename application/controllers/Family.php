<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Family extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_family');
        $this->load->model('m_determinant');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_deviceHasListing');
		$this->load->library('MY_Form_validation');
    }

    public function index()
	{
		$data['active'] = "4";
		$data["families"] = $this->m_family->get_families(); 
		$data["determinants"] = $this->m_determinant->get_determinants();
		$data["titledeter"] = "Ajouter indicateur";
		$data["titleAddFamily"] = "Ajouter famille";
		$data["titleUpdateFamily"] = "Modifier famille";
		$data["titleUpdatDeterminant"] = "Modifier indicateur";
		$this->load->render('family/all_families', $data);
    }

    public function crud_family($task, $family_id= false){
		$data['active'] = "4";
		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		if(!empty($family_id)){
			$data["family"] = $this->m_family->get_family($family_id);
		}
		if($task == "delete"){
			$this->m_family->delete_family($family_id); 
			header('Content-Type: application/json');
			echo json_encode('family');
			exit();
		}else if($task == "show"){
			$this->load->render('users/user', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_family->update_family($_POST["family_id"], $_POST); 
				Redirect('family'); 
			}else {
				$data["family"] = $this->m_family->get_family($family_id); 
				$this->load->view('family/_family',$data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				$this->m_family->add_family($_POST); 
				Redirect('family'); 
			}else {
				$this->load->view('family/_family');
			}
		}
	}

    public function determinant($task,$family_id = false,$determinant_id = false)
    {
		$data['active'] = "4";
		$data["titleCotation"] = "Ajouter cotation"; 
		$data["titleDevice"] = "Ajouter dipositif"; 
		$data["titleUpdateDevice"] = "Modifier dipositif";
		$data["families"] = $this->m_family->get_families();

        if($task == "add"){

			if(!empty($_POST)){
				$this->m_determinant->add_determinant($_POST); 
				Redirect('family'); 
			}else {
				$data["family_id"] = $family_id; 
				$this->load->view('family/_determinant',$data);
			}
		}else if($task == "update"){
			if(!empty($_POST)){ 
				$this->m_determinant->update_determinant($_POST['determinant_id'],$_POST); 
				Redirect('family'); 
			}else {
				$data["determinant"] = $this->m_determinant->get_determinant($determinant_id);

				$this->load->view('family/_determinant',$data);
			}
        }else if($task == "delete"){
			$this->m_determinant->delete_determinant($determinant_id); 
			header('Content-Type: application/json');
			echo json_encode('family');
			exit();
		}
		else if($task == "view"){
			$data["family_id"] = $family_id; 
			$data["determinant"] = $this->m_determinant->get_determinant($determinant_id); 
			$data["devices"] = $this->m_device->get_device_ByDeter($determinant_id);
			$data["listings"] = array(); 
			/*$data["deviceHasListing"] = $this->m_deviceHasListing->get_all();
			var_dump($data["deviceHasListing"]); die; 
			foreach($data["deviceHasListing"] as $val){
				$val->listing_id = $this->m_listing->get_ById($val->listing_id ); 
			}*/
			$this->load->render('family/determinant',$data);
			
		}
	}
	
	function getListing($device_id)
	{
		$result = $this->m_deviceHasListing->get_ByDeviceId($device_id); 	
		foreach($result as $val){
			$val->listing_id = $this->m_listing->get_ById($val->listing_id); 
		} 
		$output["device_id"] = $device_id;
		$output["result"] = $result;
		echo json_encode($output);
	}

	public function device($task,$determinant_id= false, $device_id = false)
    {
        if($task == "add"){
			if(!empty($_POST)){
				$this->m_device->add_device($_POST); 
				$family_id = $this->m_determinant->get_determinant($_POST['determinant_id'])->family_id; 
				Redirect('family/determinant/view/'.$family_id.'/'.$_POST['determinant_id']); 
			}else {
				$data["determinant_id"] = $determinant_id; 
				$this->load->view('family/_device',$data);
			}
        }else if ($task == "delete"){
			$this->m_device->delete_device($device_id);
			$family_id = $this->m_determinant->get_determinant($determinant_id)->family_id; 
			$url = 'family/determinant/view/'.$family_id.'/'.$determinant_id ; 
			header('Content-Type: application/json');
			echo json_encode($url);
			exit();
		} else if($task == "update"){
			if(!empty($_POST)){ 
				$data["device_name"] = $_POST['device_name'] ; 
				$this->m_device->update_device($_POST["device_id"], $data);  
				$family_id = $this->m_determinant->get_determinant($_POST['determinant_id'])->family_id; 
				Redirect('family/determinant/view/'.$family_id.'/'.$_POST['determinant_id']); 
			}else { 
				$data['determinant_id'] = $determinant_id ; 
				$data["device"]  = $this->m_device-> get_device_ByID($device_id); 
				$this->load->view('family/_device',$data);
			}
		} else if($task == "addListing"){
			if(!empty($_POST)){
				$data = array('device_id'=>$device_id,
							'listing_id'=>$_POST["listing_id"]); 
				$this->m_device->add_deviceListing($data); 	
				echo json_encode($device_id);
			}else { 
				$data["device_id"] = $device_id; 
				$data["listing"] =  $this->m_listing->get_listing(); 
			/* $existingLists = $this->m_deviceHasListing->get_ByDeviceId($device_id); 
				foreach($data["listing"] as $listing_id){
					foreach($existingLists as $existingList){
						if($existingList->listing_id == $listing_id->listing_id){
							unset($listing_id); 
						}
					}
				}*/
				$this->load->view('family/_deviceList',$data);
			}
		}
	}
	public function listing($task,$determinant_id= false, $device_id=false, $listing_id = false)
    {
        if($task == "add"){
			if(!empty($_POST)){
				$determinant_id = $_POST['determinant_id'];
				unset($_POST['determinant_id']); 
				$this->m_listing->add_listing($_POST);  
				$family_id = $this->m_determinant->get_determinant($determinant_id)->family_id; 
				Redirect('family/determinant/view/'.$family_id.'/'.$determinant_id); 
			}else {
				$data["titre"] = "Ajouter cotation" ; 
				$data["determinant_id"] = $determinant_id; 
				$this->load->view('family/_listing',$data);
			}
        }else if ($task == "delete"){
			$this->m_device->delete_deviceListing($device_id, $listing_id);
			$determinant_id = $this->m_device->get_device_ByID($device_id)->determinant_id;
			$family_id = $this->m_determinant->get_determinant($determinant_id)->family_id; 
			$url = 'family/determinant/view/'.$family_id.'/'.$determinant_id ; 
			header('Content-Type: application/json');
			echo json_encode($url);
			exit();
		}
	}
	public function crud_listing($task, $listing_id = false)
    {
		$data['active'] = "4";
        if($task == "add"){
			if(!empty($_POST)){
				$this->m_listing->add_listing($_POST);  
				Redirect('family/crud_listing/show/'); 
			}else {
				$data["titre"] = "Ajouter cotation" ;
				$this->load->view('family/_listing',$data);
			}
        }if($task == "update"){
			if(!empty($_POST)){
				$this->m_listing->update_listing($_POST["listing_id"],$_POST);  
				Redirect('family/crud_listing/show/'); 
			}else {
				$data["listing"] = $this->m_listing->get($listing_id);
				$this->load->view('family/_listing',$data);
			}
        }else if ($task == "delete"){
			$this->m_listing->delete_listing($listing_id);
			$url = 'family/crud_listing/show/' ; 
			header('Content-Type: application/json');
			echo json_encode($url);
			exit();
		}
		else if($task == "show"){
			$data["titleCotation"] = "Ajouter cotation";
			$data["titleUpdateListing"] = "Modifier cotation"; 
			$data["listings"] = $this->m_listing->get_listing(); 
 			$this->load->render('family/listings', $data);

		}
	}
}
