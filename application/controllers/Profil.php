<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_role');
        $this->load->library('MY_Form_validation');
    }

    public function index()
	{
        $data["user"] = $this->m_user->get_user_ById($_SESSION["user_id"]);
        $data["user"]->user_role= $this->m_role->get_role( $data["user"]->user_role); 
        $data["profil"] = true; 
        $data["task"] ="update";  
        $data["monprofil"] = TRUE;

        if (isset($_GET['success'])) {
            $data['updated'] = TRUE;
        }

		$this->load->render('users/user', $data);
    }
}
