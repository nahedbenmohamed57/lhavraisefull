<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outil extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
    }

    public function index()
	{
		$data['active'] = "10";
        $data['files'] = array_slice(scandir('./uploads/'), 2);
		$this->load->render('outil', $data);
    }

    public function download($file) {
        $file = './uploads/' . $file;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function delete($file) {
        unlink('./uploads/' . $file);
        redirect('/outil');
    }

    public function upload() {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
            // redirect('outil?error');
        } else {
            $data = array('upload_data' => $this->upload->data());
            redirect('outil');
        }
    }
}
