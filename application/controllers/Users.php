<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class users extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_role');
		$this->load->model('m_pole');
		$this->load->model('m_unite');
        $this->load->library('MY_Form_validation');
    }

    public function index()
	{
		$data['active'] = "2";
		$data["users"] = $this->m_user->get_users(); 
		foreach($data["users"] as $user){
			$user->user_role = $this->m_role->get_role($user->user_role);
		}
		$this->load->render('users/all_users', $data);

    }
    public function verifemail()
	{
		$current_mail = $this->m_user->address_exist($_POST["user_email"]);
		
        if ($current_mail) {
            echo json_encode(array("result"=>1));
        } else {
            echo json_encode(array("result"=>0));
        }
    }
	public function crud_user($task, $user_id= false){
		$data['active'] = "2";

		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();

		$data["units"] = $this->m_unite->get_unite2();
		$data['monprofil'] = 0; 
		if (isset($_GET['success'])) {
            $data['updated'] = TRUE;
		} else {
			$data['updated'] = false;
		}
		
		if(!empty($user_id)){
			$data["user"] = $this->m_user->get_user_ById($user_id);
		}

		if($task == "delete"){
			$this->m_user->delete_user($user_id); 
			Redirect("users"); 
		}else if($task == "show"){
			$this->load->render('users/user', $data);
		}else  if($task == "update"){
			$data['monprofil'] = 1;

			if(!empty($_POST)){
				$dataInsert = array(
					'user_firstname' => $_POST["user_firstname"],
					'user_lastname' => $_POST["user_lastname"],
					'user_email' => $_POST["user_email"],
					'user_role' => $_POST["user_role"],
					'user_pole' => NULL,
					'user_perm' => NULL,
					'user_unit' => NULL,
				);
				if(isset($_POST["user_password"])){
					$dataInsert["user_password"] = md5($_POST["user_password"]);
					unset($_POST["password_user_confirm"]); 
				}

				if ($user_id == $_SESSION["user_id"]) {
					$redirect="profil"; 
				} else {
					$redirect="user";
				}
				if(isset($_POST['user_perm'])) {
					// add user perimetre
					$dataInsert['user_perm'] = $_POST['user_perm'] ? json_encode($_POST['user_perm'] ): NULL;
				}
				if(isset($_POST['user_pole'])) {
					// add user pole
					$dataInsert['user_pole'] = $_POST['user_pole']?json_encode($_POST['user_pole'] ): NULL;
				}
				if(isset($_POST['user_unit'])) {
					// add user units
					$dataInsert['user_unit'] = $_POST['user_unit']?json_encode($_POST['user_unit'] ): NULL;
				}
				
				$this->m_user->update_user($user_id, $dataInsert);

				echo json_encode(array("redirect"=>$redirect,"user"=>$user_id));
			} else {
				$this->load->render('users/user', $data);
			}
		}else {
			if(!empty($_POST)){
				$_POST["user_password"] =  md5($_POST["user_password"]);
				if($this->m_user->add_user($_POST)){
					echo json_encode(array("result"=>1));
				}else{
					echo json_encode(array("result"=>0));
				}

			}else {
				$this->load->render('users/user', $data);
			}
		}
	}
}
