<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_password_resets');
        $this->load->library('MY_Form_validation');
		$this->load->helper('sendMail_helper');
    }

    public function index()
	{
        $this->load->view('login');
    }

    public function sign()
	{
        $this->load->library('session');
        $found=$this->m_user->get_user($_POST["user"],md5($_POST["password"]));
        if($found){
            $this->session->set_userdata(array(
                "user_id" => $found[0]->user_id, 
				"role_id" => $found[0]->user_role 
            ));
            $config = $this->m_user->get_config();
			$this->session->set_userdata(array(
				"config" => $config
			));
            echo json_encode(array("result"=>1));
        }else{
            echo json_encode(array("result"=>0));
        }
    }

    public function test()
	{
        $this->template->load('template','login');
    }
	
	public function logout() 
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('role_id');
		redirect('');
	}
	
	public function forgetPwd($email= false)
	{
		if(!empty($email)){
			$token = base64_encode(openssl_random_pseudo_bytes(32));
			$token = str_replace("/", "", $token);
			$exitMail =  $this->m_password_resets->getByMail($email); 
			$data = array(
						'email' => $email,
						'token' => $token
				 );
			if($exitMail == null){
				$this->db->insert('password_resets',$data);	
			}else {
				$this->db->where('id', $exitMail->id);
				$this->db->update('password_resets', $data);
			} 
			$output = send_mail($email,$token); 
			header('Content-Type: application/json');
			echo json_encode($output);

		}else {
			$this->template->load('template','forget_pwd');
		}
	}
	
	function verifyMail($email){
		$result = $this->m_user->address_exist($email);
		if(empty($result)){
			$output = 0 ; 
		}else {
			$output = 1 ; 
		}
		header('Content-Type: application/json');
		echo json_encode($output);
		exit();
	}
	public function resetpwd($token, $pwd=false)
	{ 	 
		if(!empty($pwd)){
			$pwd = urldecode($pwd);
			$email = $this->m_password_resets->getByToken($token)->email;
			$user = $this->m_user->address_exist($email)[0] ; 
			$data = array(
				'user_password' => hash('md5',$pwd)
			); 
			$this->m_user->update_user($user->user_id, $data); 
			
			$this->session->set_userdata(array(
					'id' => $user->user_id
				));
			redirect('users');	
		}else {
			$this->load->view('resetPwd', array('token' => $token));
		}
	}
	
}
