<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facteurs extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_facteur');
        $this->load->model('m_family_rps');
        $this->load->model('m_determinant');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_deviceHasListing');
		$this->load->library('MY_Form_validation');
    }

    public function index()
	{
		$data['active'] = "4";
		$data["families_rps"] = $this->m_family_rps->get_families(); 
		$data["facteurs"] = $this->m_facteur->get_facteurs();
		
		$data["titleindic"] = "Ajouter un indicateur"; 
		$data["titleAddFamilyRps"] = "Ajouter une famille RPS";
		$data["titleUpdateFamily"] = "Modifier famille";
		$data["titleUpdatfacteur"] = "Modifier facteur";
		$data['titre'] = "Ajouter une famille RPS";

		$this->load->render('family_rps/all_facteurs', $data);
    }

    public function crud_family($task, $family_id= false){
		$data['active'] = "4";
		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		if(!empty($family_id)){
			$data["family"] = $this->m_family->get_family($family_id);
		}
		if($task == "delete"){
			$this->m_family->delete_family($family_id); 
			header('Content-Type: application/json');
			echo json_encode('family');
			exit();
		}else if($task == "show"){
			$this->load->render('users/user', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_family->update_family($_POST["family_id"], $_POST); 
				Redirect('family'); 
			}else {
				$data["family"] = $this->m_family->get_family($family_id); 
				$this->load->view('family/_family',$data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				$this->m_family->add_family($_POST); 
				Redirect('family'); 
			}else {
				$this->load->view('family/_family');
			}
		}
	}

    public function facteur($task,$family_id = false,$determinant_id = false)
    {
		$data['active'] = "4";
		$data["titleCotation"] = "Ajouter cotation"; 
		$data["titleDevice"] = "Ajouter dipositif"; 
		$data["titleUpdateDevice"] = "Modifier dipositif";
		$data["families"] = $this->m_family_rps->get_families(); 
        if($task == "add"){
			if(!empty($_POST)){
				$this->m_facteur->add_facteur($_POST); 
				Redirect('facteurs'); 
			}else {
				$data["family_id"] = $family_id; 
				$this->load->view('family_rps/_facteur',$data);
			}
		}else if($task == "update"){
			if(!empty($_POST)){ 
				$this->m_facteur->update_facteur($_POST['facteur_id'],$_POST); 
				Redirect('facteurs'); 
			}else {
				$data["facteur"] = $this->m_facteur->get_facteur($determinant_id);
				$this->load->view('family_rps/_facteur',$data);
			}
        }else if($task == "delete"){
			$this->m_facteur->delete_facteur($determinant_id); 
			header('Content-Type: application/json');
			echo json_encode('facteurs');
			exit();
		}else if($task == "view"){
			$data["family_id"] = $family_id; 
			$data["determinant"] = $this->m_determinant->get_determinant($determinant_id); 
			$data["devices"] = $this->m_device->get_device_ByDeter($determinant_id);
			$data["listings"] = array(); 
			$this->load->render('family/determinant',$data);
			
		}
	}

	public function correlation() {
		$data['active'] = "4";
		$data["facteurs"] = $this->m_facteur->get_facteurs_correlation();

		$data["determinant"] = $this->m_determinant->get_determinants();
		if(isset($_POST)) {
			if(!empty($_POST)) {
				
				$mydata = [];
				$dataPost= $_POST;
				foreach ($dataPost as $key => $d) {
					foreach ($d as $k => $v) {
						array_push($mydata, array('id_determinant'=> $key, 'id_facteur' =>$v, 'value' => 1));
					}
				}

				$this->m_facteur->insert_correlation($mydata);
				Redirect('facteurs/correlation');
			}
			
		} 
		 
		$this->load->render('family_rps/correlation', $data);
	}
	
}
